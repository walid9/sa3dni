<?php

namespace App\Exceptions;

use App\Mail\PaidReservation;
use App\Mail\SupportMail;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Mail;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
//
//        if($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException){
//
//            if($exception->getStatusCode() == 403){
//                return redirect(route('error.authorization.page'));
//            }
//        }
//
//        if((($exception instanceof    \Illuminate\Validation\ValidationException))
//            || ($exception instanceof    \Illuminate\Auth\AuthenticationException   )
//            ||($exception instanceof    \Illuminate\Auth\Access\AuthorizationException)  )
//        {
//            $flag=true;
//        }
//        else if((($exception instanceof    \Symfony\Component\HttpKernel\Exception\HttpException))||
//            (($exception instanceof    \Illuminate\Database\Eloquent\ModelNotFoundException))||
//            (($exception instanceof     \Illuminate\Session\TokenMismatchException))){
//
//
//            if(!$request->ajax()){
//                return redirect(route('error.page'));
//            }else{
//                return response()->json(['message'=>'Error Happened Please, Contact Our Support']);
//            }
//
//        }else{
//
//            $message='<pre>'.$exception->getMessage().'<hr/>'. $exception.'</pre>';
//
//            $this->sendSupportMail($message);
//
//            if(!$request->ajax()){
//                return redirect(route('error.page'));
//            }else{
//                return response()->json(['message'=>'Error Happened Please, Contact Our Support']);
//            }
//        }

        return parent::render($request, $exception);
    }
}
