<?php

use App\Models\DoctorClinicTime;
use App\Models\DoctorPeriod;
use App\Models\Invoice;
use App\Models\Speciality;
use App\Models\Day;
use App\Models\Doctor;
use App\Models\Location;
use App\Models\Notification;
use App\Models\Reservation;
use App\Models\Review;
use App\Models\Setting;
use App\Models\Time;
use App\Models\User;
use App\Models\UserLevel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;


if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
        return $value;

    }
}

function notifications()
{
    $notifications = Notification::orderBy('id', 'DESC')->where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->where("title","!=","registration")->limit(30)->get();
    return $notifications ;
}


function notification_count()
{
    $notification_count = Notification::orderBy('id', 'DESC')->where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->where("read_from_patient",0)->where("title","!=","registration")->limit(30)->get();
    $notification_count = $notification_count->count() ;
    return $notification_count ;
}

function get_in_touch_content()
{
    $get_in_touch_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'get_in_touch_content')->value('value');
    return $get_in_touch_content;
}

function logo()
{
    $logo = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'logo')->value('value');
    return $logo;
}

function banner()
{
    $banner = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'banner')->value('value');
    return $banner;
}

function get_in_touch_image()
{
    $get_in_touch_image = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'get_in_touch_image')->value('value');
    return $get_in_touch_image;
}

function address()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'address')->value('value');
    return $phone;
}


function facebook()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'facebook')->value('value');
    return $phone;
}

function twitter()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'twitter')->value('value');
    return $phone;
}

function google()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'google')->value('value');
    return $phone;
}

function insta()
{
    $phone = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'insta')->value('value');
    return $phone;
}

function linked_in()
{
    $linked_in = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'linked_in')->value('value');
    return $linked_in;
}



function about_us_content()
{
    $lang = 'en';

    if(session()->get('locale')){
        $lang = session()->get('locale');
    }

    $setting_name = 'about_us_content_'.$lang;

    $about_us_content = \Illuminate\Support\Facades\DB::table('settings')->where('name', $setting_name)->value('value');
    return $about_us_content;
}

function get_policy()
{
    $lang = 'en';

    if(session()->get('locale')){
        $lang = session()->get('locale');
    }

    $setting_name = 'policy_'.$lang;

    $policy = \Illuminate\Support\Facades\DB::table('settings')->where('name', $setting_name)->value('value');
    return $policy;
}

function about_us_image()
{

    $about_us_image = \Illuminate\Support\Facades\DB::table('settings')->where('name', 'about_us_image')->value('value');
    return $about_us_image;
}

if (!function_exists('recentNotifications')) {
    function recentNotifications()
    {
        $notifications = DB::table('notifications')
            ->where('user_id', 1)
            ->where('read_from_patient', 0)
            ->orderBy('id', 'desc')
            ->take(2)
            ->get();
        return $notifications;
    }
};

if (!function_exists('countNotifications')) {
    function countNotifications()
    {
        $notifications = DB::table('notifications')
            ->where('user_id', 1)
            ->where('read_from_patient', 0)
            ->get();
        return count($notifications);
    }
};


if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {

            return response()->json(['status' => $status, 'msg' => $msg,], $status);

//            return [
//                'status' => $status, 'msg' => $msg,
//            ];
        } else {

            return
                response()->json(
                    [
                        'status' => $status,
                        'msg' => $msg,
                        'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data

                    ], $status);


//            return [
//                'status' => $status,
//                'msg' => $msg,
//                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
//            ];
        }
    }
};


if (!function_exists('updateNotification')) {
    function updateNotification($notification_id, $status_id)
    {
        if (!$notification_id && !$status_id) {
            $response = [
                'status' => 401,
                'msg' => 'Please Insert Full Fields To Update Notification',
            ];
            return response()->json($response);
        } else {
            Notification::where('id', $notification_id)->update([
                'sending_id' => $status_id,
            ]);
            $response = [
                'status' => 200,
                'msg' => trans('messages.success'),
            ];
            return response()->json($response);
        }
    }
};

if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            ceil($items->count() / $perPage),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}

if (!function_exists('sidebarActive')) {
    function sidebarActive(array $routes)
    {
        if (in_array(Route::currentRouteName(), $routes)) {
            return 'active pcoded-trigger';
        }
        return '';
    }
}
if (!function_exists('set_active')) {
    function set_active($path, $active = 'active pcoded-trigger')
    {
        return call_user_func_array('Request::is', (array)$path) ? $active : '';
    }
}


if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
        return $value;

    }
}

if (!function_exists('malePlaceHolder')) {
    function malePlaceHolder()
    {
        return str_random(60);
    }
}

if (!function_exists('generateRandomCode')) {
    function generateRandomCode($digits, $id = null)
    {
        $i = 0; //counter
        $value = $id; //our default pin is the order id .
        while ($i < $digits - intval(strlen($id))) {
            //generate a random number between 0 and 9.
            $value .= mt_rand(0, 9);
            $i++;
        }
        return $value;
        //return 1111; //TODO Disable In Production
    }


}


if (!function_exists('adminNotification')) {

    function adminNotification($message)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        /* $notificationHead= DB::table('notifications')
             ->where('notifications.id', $notification->id)
             ->where('notifications.is_read' , 0)
             ->join('notificationLocales', 'notifications.notification_key', '=', 'notificationLocales.notification_key')
             ->where('notificationLocales.language_id', 2)
             ->select( 'notificationLocales.heading', 'notificationLocales.content')
             ->first();*/
        $notification = [
            'title' => $message->title,
            'body' => $message->body,
            //'sound' => true,
        ];
        $tokenId[] = User::where('id', 1)->first()->firebase_token;
        // array_push($tokenId, $user->firebase_token);
        $fcmNotification = [
            'registration_ids' => $tokenId,
            'notification' => $notification,
        ];

        $headers = [
            'Authorization:key=AAAAfswMCPU:APA91bHSsDDIG8h71soZqKpSUVCOp5YR690hKrOqAug1a7FgnzmYCv7aBrV0jHQ_BETfaUgvpcfCjaWP3RO4M9qOBVwt8G2DqfqnWFTguxag5IZLsTEjfSSNZ1dpR2FFKvIL1vpdqInh',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $res_json = curl_exec($ch);
        /* $result     = get_object_vars(json_decode($res_json));
         $code       = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
         $merge1     = array_merge($code,$fcmNotification);
         $merge      = array_merge($result,$merge1);
         $reposeDet  = array('response' => $merge);*/
        curl_close($ch);
        return $res_json;
    }
}

if (!function_exists('doctorNotification')) {

    function doctorNotification($action_type , $message , $action_id)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $data = [
            'title'        => $message->title,
            'body'         => $message->body,
            'action_id'    => $action_id,
            'action_type'  => $action_type
        ];

        $notification = [
            'title' => $message->title,
            'body' => $message->body,
        ];

        $tokenId[] = Doctor::where('id', $message->doctor_id)->first()->firebase_token;
        // array_push($tokenId, $user->firebase_token);
        $fcmNotification = [
            'registration_ids' => $tokenId,
            'notification' => $notification,
            'data' => $data
        ];

        $headers = [
            'Authorization:key=AAAAfswMCPU:APA91bHSsDDIG8h71soZqKpSUVCOp5YR690hKrOqAug1a7FgnzmYCv7aBrV0jHQ_BETfaUgvpcfCjaWP3RO4M9qOBVwt8G2DqfqnWFTguxag5IZLsTEjfSSNZ1dpR2FFKvIL1vpdqInh',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $res_json = curl_exec($ch);
        /* $result     = get_object_vars(json_decode($res_json));
         $code       = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
         $merge1     = array_merge($code,$fcmNotification);
         $merge      = array_merge($result,$merge1);
         $reposeDet  = array('response' => $merge);*/
        curl_close($ch);
        return $res_json;
    }
}

if (!function_exists('userNotification')) {

    function userNotification($message)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $data = [
            'title' => $message->title,
            'body'         => $message->title,
            'action_type'  => $message->action_type
        ];

        $notification = [
            'title' => $message->title,
            'body' => $message->title,
            'sound' => true,
        ];

        if(User::where('id', $message->user_id)->first()) {
            $tokenId[] = User::where('id', $message->user_id)->first()->firebase_token;
        }

        $fcmNotification = [
            'registration_ids' => $tokenId??null,
            'notification' => $notification,
            'data' => $data,
        ];


        $headers = [
            'Authorization:key=AAAA86uH8to:APA91bHsBKcmzd7DvV_cJKCE1YAxX7IOhdOlCo5YQ4bKC5KBuBoWnYTo20d2ehVbsLVHZPt9W9eF-dQY-N2pYQuAdcm-Mx0HGRPdMs82CUS-vYGgaw2Bigsph7mdBQ3RDYlPmYoP8UYN',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $res_json = curl_exec($ch);
        /* $result     = get_object_vars(json_decode($res_json));
         $code       = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
         $merge1     = array_merge($code,$fcmNotification);
         $merge      = array_merge($result,$merge1);
         $reposeDet  = array('response' => $merge);*/
        curl_close($ch);
        return $res_json;
    }
}

if (!function_exists('levelNotification')) {

    function levelNotification($level , $message)
    {

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $data = [
            'title' => $message->title,
        ];

        $notification = [
            'title' => $message->title,
            'body' => $message->body,
            'sound' => true,
        ];

        foreach (UserLevel::where('level_id' , $level)->pluck('user_id') as $user_id)
            $tokenId[] = User::where('id', $user_id)->first()->firebase_token;

            $fcmNotification = [
                'registration_ids' => $tokenId??null,
                'notification' => $notification,
                'data' => $data,
            ];


            $headers = [
                'Authorization:key=AAAA86uH8to:APA91bHsBKcmzd7DvV_cJKCE1YAxX7IOhdOlCo5YQ4bKC5KBuBoWnYTo20d2ehVbsLVHZPt9W9eF-dQY-N2pYQuAdcm-Mx0HGRPdMs82CUS-vYGgaw2Bigsph7mdBQ3RDYlPmYoP8UYN',
                'Content-Type: application/json'
            ];
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $fcmUrl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
            $res_json = curl_exec($ch);
            /* $result     = get_object_vars(json_decode($res_json));
             $code       = array('info' => curl_getinfo($ch, CURLINFO_HTTP_CODE));
             $merge1     = array_merge($code,$fcmNotification);
             $merge      = array_merge($result,$merge1);
             $reposeDet  = array('response' => $merge);*/
            curl_close($ch);
            return $res_json;
           }
}

if (!function_exists('getAuth')) {
    function getAuth()
    {
        $auth = '';

        if (auth()->check())
            return $auth = auth()->user();

        if (auth('doctor')->check())
            return $auth = auth('doctor')->user();

    }
}

if (!function_exists('calculateDoctorReviews')) {
    function calculateDoctorReviews($doctor)
    {
        $result = 0;

        if($doctor->reviews->count()){

            $result = round($doctor->reviews()->sum('rate') / $doctor->reviews->count());
        }

        return $result;
    }
}

////////////////////// start reservations time  ///////////////

if (!function_exists('doctorTimes')) {
    function doctorTimes($doctor)
    {
        $session = $doctor->session_time;   // in minutes

        $days = [];

        for ($i = 0; $i < 7; $i++) {

            $date = Carbon::now()->addDays($i);

            $periods = $doctor->periods()->whereHas('day', function ($day) use ($i, $date) {

                $day->where('name', 'like', '%' .$date->format('l') . '%');

            })->get();


            if (!$periods)
                continue;

            $days[$i] = timesInPeriod($periods, $session, $date);

            if($i == 0)
                $days[$i] = remainTimeToday($days[$i]);
        }

      //  $days = availableTimes($days, $doctor);

        return $days;
    }
}

if (!function_exists('clinicTimes')) {
    function clinicTimes($clinic , $doctor)
    {
        $session = $doctor->clinic_session;   // in minutes

        $days = [];

        for ($i = 0; $i < 7; $i++) {

            $date = Carbon::now()->addDays($i);

            $periods = $clinic->times()->whereHas('day', function ($day) use ($i, $date) {

                $day->where('name', 'like', '%' .$date->format('l') . '%');

            })->get();


            if (!$periods)
                continue;

            $days[$i] = timesInPeriod($periods, $session, $date);

            if($i == 0)
                $days[$i] = remainTimeToday($days[$i]);
        }

        //  $days = availableTimes($days, $doctor);

        return $days;
    }
}

if (!function_exists('availableTimes')) {
    function availableTimes($days, $doctor)
    {
        $data = [];

        foreach ($days as $index => $day) {

            $times = [];

            foreach ($day as $value) {

                $time = timeDetailsData($value);

                $date = Carbon::now()->addDays($index)->format('Y-m-d');

                $date = onlyCreateDateFull($date)->addHours($time['hour'])->addMinutes($time['minute']);

                $reservations = Reservation::where('doctor_id', $doctor->id)->where('date', $date)->count();

                if(!$reservations){
                    $key_real_time = $date->format('H').'.'.$date->format('i');
                    $times[$key_real_time] = $date->format('h:i A');
                }
            }
            $data[$index] = $times;
        }

        return $data;
    }
}

if (!function_exists('timeDetailsData')) {
    function timeDetailsData($value){

        $timeDetails = [];

        $data = explode(' ', $value);

        $time = $data[0];

        $time = explode(':', $time);

        $hour = $time[0];

        $minute = $time[1];

        $type = $data[1];

        if ($type == 'AM') {

            if ($hour == 12)
                $hour = 0.00;

        } else {

            if ($hour != 12)
                $hour += 12;
        }

        $timeDetails['hour'] = $hour;
        $timeDetails['minute'] = $minute;
        $timeDetails['type'] = $type;

        return $timeDetails;
    }
}

if (!function_exists('onlyCreateDateFull')) {

    function onlyCreateDateFull($date)
    {
        $selected_date = explode('-', $date);

        $month = $selected_date[1];
        $day = $selected_date[2];
        $year = $selected_date[0];

        $creation_date = Carbon::create($year, $month, $day, 0, 0, 0);

        return $creation_date;
    }
}


if (!function_exists('timesInPeriod')) {

    function timesInPeriod($periods, $session, $date)
    {
        //dd($session);
        $y = $date->format('Y');
        $m = $date->format('m');
        $d = $date->format('d');

        $times = [];

        foreach($periods as $period){

            $time_from = Time::findOrFail($period->from_id);

            $time_to = Time::findOrFail($period->to_id);

            $hours_number = $time_to->system_twenty_four - $time_from->system_twenty_four ;

            $loop = intval($hours_number * 60 / $session);

            $start_date = Carbon::create($y, $m, $d, intval($time_from->system_twenty_four ),0, 0);

            $key_start_time = $start_date->format('H').'.'.$start_date->format('i');

            $times[$key_start_time] = $start_date->format('h:i A');

            for($k=1; $k<=$loop; $k++){

                $real_date = $start_date->addMinutes($session);

                $key_real_time = $real_date->format('H').'.'.$real_date->format('i');

                $times[$key_real_time] = $real_date->format('h:i A');

                if($real_date->format('H') >= $time_to->system_twenty_four)
                    break;
            }
        }

        ksort($times);

        return $times;
    }
}

if (!function_exists('getHours')) {

    function getHours($time)
    {
        $time = explode(' ',$time)[0];

        $type = $time[1];

        $hour = explode(':', $time)[0];

        if ($type == 'AM'){

            if ($hour == 12)
                $hour = 0.00;

        } else {

            if ($hour != 12)
                $hour += 12;
        }

        return $hour;
    }
}

if (!function_exists('getMinutes')) {

    function getMinutes($time)
    {
        $time = explode(' ',$time)[0];

        $minutes = explode(':', $time)[1];

        return $minutes;
    }
}

if (!function_exists('remainTimeToday')) {

    function remainTimeToday($times)
    {
        $hour_now = Carbon::now()->format('H');
        $minute_now = Carbon::now()->format('i');

        $period_now = floatval($hour_now.'.'.$minute_now);

        $data = [];

        foreach ($times as $key => $time) {

            if ($key > $period_now ){
                $data[$key] = $time;
            }
        }

        return $data;
    }
}

////////////////////// end reservations time  ///////////////

if (!function_exists('getDoctorBySlug')) {

    function getDoctorBySlug($slug)
    {
        $doctor = Doctor::where('slug',$slug)->first();
        return $doctor;
    }
}

if (!function_exists('getExaminationResult')) {

    function getExaminationResult($value)
    {
        if($value == 0)
            return 'No';
        elseif ($value == 1)
            return 'little';
        elseif ($value == 2)
            return 'sometimes';
        elseif ($value == 3)
            return 'Continuously';
        else
            return  'not found';
    }
}

function create_random_name() {

    $time       = time();
    $random     = rand( 1, 100000 );
    $divide     = $time / $random;
    $encryption = md5( $divide );
    $name_enc   = substr( $encryption, 0, 20 );

    return $name_enc;
}




