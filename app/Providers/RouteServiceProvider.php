<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $adminNamespace = 'App\Http\Controllers\Admin';
    protected $webNamespace = 'App\Http\Controllers\Web';

    protected $middleware =  [ 'web','localeSessionRedirect', 'localizationRedirect', 'localeViewPath'];



    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

//        $this->adminUsersRoutes();
//        $this->adminSettingsRoutes();
//        $this->adminServicesRoutes();
//        $this->adminCategoriesRoutes();
//        $this->adminReportsRoutes();
//        $this->adminFawryOrdersRoutes();
//        $this->adminNotificationsRoutes();
//        $this->adminMessagesRoutes();
//        $this->adminRolesRoutes();
//        $this->adminDoctorsRoutes();
//        $this->adminReviewsRoutes();
//        $this->adminLocationsRoutes();
//        $this->adminReservationsRoutes();
//        $this->adminInvoicesRoutes();
//        $this->adminOrdersRoutes();
//        $this->adminTransactionsRoutes();
//        $this->adminAdsRoutes();
//
//        $this->adminExaminationResultsRoutes();
//
//        $this->adminTagsRoutes();
//        $this->adminCouponsRoutes();
//
//
//        //////////////// Web Maps /////////////////////
//
//        $this->webDoctorsRoutes();
//        $this->webReservationRoutes();
//        $this->webPaymentRoutes();
//        $this->webMarketingRoutes();
//        $this->webMeetingRoutes();
//        $this->webCovidProgramRoutes();
//        $this->webPlansRoutes();
//        $this->webCouponsRoutes();
//        $this->webDoctorProfileRoutes();
//        $this->webPatientProfileRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }


//    protected function adminUsersRoutes()
//    {
//        Route::middleware('web','CheckAdmin')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->as('admin:')
//            ->group(base_path('routes/admin/users.php'));
//    }
//
//    protected function adminCategoriesRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/categories.php'));
//    }
//
//    protected function adminServicesRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/services.php'));
//    }
//
//    protected function adminFawryOrdersRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/fawry_orders.php'));
//    }
//
//    protected function adminReportsRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/reports.php'));
//    }
//
//    protected function adminNotificationsRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/notifications.php'));
//    }
//
//    protected function adminSettingsRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/settings.php'));
//    }
//
//    protected function adminMessagesRoutes()
//    {
//        Route::middleware('web')
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/messages.php'));
//    }
//
//    protected function adminRolesRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->as('admin:')
//            ->group(base_path('routes/admin/roles.php'));
//    }
//
//    protected function adminDoctorsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//        ->namespace($this->adminNamespace)
//            ->prefix('admin/doctor')
//            ->name('doctors.')
//            ->group(base_path('routes/admin/doctors.php'));
//    }
//
//    protected function adminAdsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin/ads')
//            ->name('ads.')
//            ->group(base_path('routes/admin/ads.php'));
//    }
//
//    protected function adminReviewsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin/review')
//            ->name('reviews.')
//            ->group(base_path('routes/admin/reviews.php'));
//    }
//
//    protected function adminLocationsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin/location')
//            ->name('locations.')
//            ->group(base_path('routes/admin/locations.php'));
//    }
//
//    protected function adminTagsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin/tag')
//            ->name('tags.')
//            ->group(base_path('routes/admin/tags.php'));
//    }
//
//    protected function adminReservationsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/reservations.php'));
//    }
//
//    protected function adminInvoicesRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/invoices.php'));
//    }
//
//    protected function adminOrdersRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/orders.php'));
//    }
//
//    protected function adminTransactionsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/transactions.php'));
//    }
//
//    protected function adminExaminationResultsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/examination_results.php'));
//    }
//
//    protected function adminCouponsRoutes()
//    {
//        Route::middleware(['web','CheckAdmin'])
//            ->namespace($this->adminNamespace)
//            ->prefix('admin')
//            ->name('admin:')
//            ->group(base_path('routes/admin/coupons.php'));
//    }

    /////////////////////////////// Web Maps //////////////////////////

//    protected function webDoctorsRoutes()
//    {
//
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix.'/doctors')
//            ->name('doctors.')
//            ->group(base_path('routes/web/doctors.php'));
//    }
//
//    protected function webReservationRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix.'/reservations')
//            ->name('web:reservations.')
//            ->group(base_path('routes/web/reservations.php'));
//    }
//
//    protected function webPaymentRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix.'/payment')
//            ->name('web:payment.')
//            ->group(base_path('routes/web/payment.php'));
//    }
//
//    protected function webMarketingRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix.'/marketing')
//            ->name('web:marketing.')
//            ->group(base_path('routes/web/marketing.php'));
//    }
//
//    protected function webMeetingRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/meeting.php'));
//    }
//
//    protected function webCovidProgramRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/covid_program.php'));
//    }
//
//    protected function webPlansRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/plans.php'));
//    }
//
//    protected function webCouponsRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/coupons.php'));
//    }
//
//    protected function webDoctorProfileRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/doctor_profile.php'));
//    }
//
//    protected function webPatientProfileRoutes()
//    {
//        $langObj = new \Mcamara\LaravelLocalization\LaravelLocalization();
//        $prefix =  $langObj->setLocale();
//
//        Route::middleware($this->middleware)
//            ->namespace($this->webNamespace)
//            ->prefix($prefix)
//            ->name('web:')
//            ->group(base_path('routes/web/patient_profile.php'));
//    }






    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
