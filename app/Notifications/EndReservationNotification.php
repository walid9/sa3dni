<?php

namespace App\Notifications;

use App\Models\Reservation;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EndReservationNotification extends Notification
{
    use Queueable;

    protected $reservation;
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //   Send Notifications To Patient
        $notification_patient = new \App\Models\Notification();
        $notification_patient->title = "Reminder End Reservation";
        $notification_patient->body = "سوف ينتهي الحجز بعد 10 دقائق مع الدكتور ". $this->reservation->doctor->name;
        $notification_patient->user_id = $this->reservation->user_id;
        $notification_patient->save();

        userNotification($notification_patient);

        return (new MailMessage)
            ->line('Hi.'.optional($this->reservation->user)->name)
            ->action('your reservation will End within 10 minutes', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
