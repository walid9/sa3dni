<?php

namespace App\Notifications;

use App\Models\Reservation;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReminderDoctorReservationNotification extends Notification
{
    use Queueable;

    protected $reservation;
    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //   Send Notifications To Doctor
        $notification_patient = new \App\Models\Notification();
        $notification_patient->title = "Reminder Start Reservation";
        $notification_patient->body = "سوف يبدا الحجز بعد 15 دقيقة مع المريض ". $this->reservation->user->name;
        $notification_patient->doctor_id = $this->reservation->doctor_id;
        $notification_patient->save();

        doctorNotification($notification_patient);

        return (new MailMessage)
            ->line('Hi.'.optional($this->reservation->doctor)->name)
            ->action('your reservation will start in '. $this->reservation->date, url('/'))
            ->line('Thank you for using our application!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
