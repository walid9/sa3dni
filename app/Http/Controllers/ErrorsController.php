<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ErrorsController extends Controller
{
    public function errorPage(){
        return view('error');
    }

    public function errorAuthorizationPage(){
        return view('authorization_error_page');
    }
}
