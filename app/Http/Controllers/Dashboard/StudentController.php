<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Department;
use App\Models\LevelDepartment;
use App\Models\Notification;
use App\Models\Subject;
use App\Models\SubjectFile;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function index(){
        return redirect('student/subjects/video');
        //return view('Student.index');
    }
    public function video(){
        $files=SubjectFile::where('user_id',Auth::id())->where('file_type',0)->get();
        $user_departments=UserLevel::where('user_id',Auth::id())->with('level.department')->get();
        return view('Student.Subjects.video',['files'=>$files,'departments'=>$user_departments]);
    }
    public function video_add(Request $request){
        $level_department=LevelDepartment::where('id',$request->department)->first();
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 0,
            'department_id' => $level_department->department_id,
            'level' => $level_department->level,
            'user_id'=>Auth::id(),
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',Auth::id())->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=',$user->id)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Video Added Successful');

    }
    public function sheet(){
        $files=SubjectFile::where('user_id',Auth::id())->where('file_type',1)->get();
        $user_departments=UserLevel::where('user_id',Auth::id())->with('level.department')->get();
        return view('Student.Subjects.sheet',['files'=>$files,'departments'=>$user_departments]);
    }
    public function sheet_add(Request $request){
        $level_department=LevelDepartment::where('id',$request->department)->first();
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 1,
            'department_id' => $level_department->department_id,
            'level' => $level_department->level,
            'user_id'=>Auth::id(),
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',Auth::id())->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $user->id)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Sheet Added Successful');

    }
    public function summary(){
        $files=SubjectFile::where('user_id',Auth::id())->where('file_type',2)->get();
        $user_departments=UserLevel::where('user_id',Auth::id())->with('level.department')->get();
        return view('Student.Subjects.summary',['files'=>$files,'departments'=>$user_departments]);
    }
    public function summary_add(Request $request){
        $level_department=LevelDepartment::where('id',$request->department)->first();
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 2,
            'department_id' => $level_department->department_id,
            'level' => $level_department->level,
            'user_id'=>Auth::id(),
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',Auth::id())->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $user->id)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Summary Added Successful');

    }

    public function get_subject(Request $request){
        $levels['data']=Subject::where('department_id',$request->get('id'))->get();
        return response()->json($levels);
    }
}
