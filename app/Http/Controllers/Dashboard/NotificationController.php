<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Faculty;
use App\Models\LevelDepartment;
use App\Models\Notification;
use App\Models\University;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(){
        $universities=University::all();
        return view('Pages.Management.notifications',['universities'=>$universities]);
    }

    public function send(Request $request){
        if($request->faculty=='all'){
            $faculties=Faculty::where('university_id',$request->university)->get();
            foreach ($faculties as $faculty){
                $departments=Department::where('faculty_id',$faculty->id)->get();
                foreach ($departments as $department){
                    $level_departments=LevelDepartment::where('department_id',$department->id)->get();
                    foreach ($level_departments as $level_department){
                        $notification=new Notification();
                        $notification->title=$request->title;
                        $notification->action_type="admin";
                        $notification->level_id=$level_department->id;
                        $notification->save();
                        levelNotification($level_department->id , $notification);
                    }
                }
            }
            return back()->with('message','Notification Send Successful');
        }
        else{
            if($request->department=='all'){
                $departments=Department::where('faculty_id',$request->faculty)->get();
                foreach ($departments as $department){
                    $level_departments=LevelDepartment::where('department_id',$department->id)->get();
                    foreach ($level_departments as $level_department){
                        $notification=new Notification();
                        $notification->title=$request->title;
                        $notification->action_type="admin";
                        $notification->level_id=$level_department->id;
                        $notification->save();
                        levelNotification($level_department->id , $notification);
                    }
                }
                return back()->with('message','Notification Send Successful');
            }
            else{
                if($request->level=='all'){
                    $level_departments=LevelDepartment::where('department_id',$request->department)->get();
                    foreach ($level_departments as $level_department){
                        $notification=new Notification();
                        $notification->title=$request->title;
                        $notification->action_type="admin";
                        $notification->level_id=$level_department->id;
                        $notification->save();
                        levelNotification($level_department->id , $notification);
                    }
                    return back()->with('message','Notification Send Successful');
                }
                else{
                    $level_department=LevelDepartment::where('department_id',$request->department)->where('level',$request->level)->first();
                    $notification=new Notification();
                    $notification->title=$request->title;
                    $notification->action_type="admin";
                    $notification->level_id=$level_department->id;
                    $notification->save();
                    levelNotification($level_department->id , $notification);
                }
                return back()->with('message','Notification Send Successful');
            }
        }
    }
}
