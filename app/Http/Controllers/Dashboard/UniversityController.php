<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\University;

class UniversityController extends Controller
{
    public function index(){
        $university=University::all();
        return view('Pages.university')->with('universities',$university);
    }

    public function Add(Request $request){
        $university=new University();
        $university->name=$request->name;
        $university->save();
        return back()->with('message','University Added Successful');
    }

    public function Edit(Request $request,$id){
        $university=University::findOrFail($id);
        $university->name=$request->name;
        $university->save();
        return back()->with('message','University Edited Successful');
    }

    public function Delete($id){
        $unversity=University::findOrFail($id);
        $unversity->delete();
        return back()->with('message','University Deleted Successful');

    }
}
