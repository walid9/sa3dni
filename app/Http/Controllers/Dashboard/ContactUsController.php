<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function index(){
        $contacts=ContactUs::orderBy('id','desc')->get();
        return view('Pages.contact_us')->with('contacts',$contacts);
    }

    public function delete($id){
        $contact=ContactUs::findOrFail($id);
        $contact->delete();
        return back()->with('message','Contact Deleted Successful');
    }
}
