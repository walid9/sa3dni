<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Comment;
use App\Models\Department;
use App\Models\Faculty;
use App\Models\LevelDepartment;
use App\Models\Notification;
use App\Models\University;
use App\Models\User;
use App\Models\UserLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    public function approved(){
        $posts=Post::where('approved','1')->where('user_id','<>',null)->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Posts.approved')->with('posts',$posts);
    }
    public function waiting(){
        $posts=Post::where('approved','0')->where('user_id','<>',null)->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Posts.wait')->with('posts',$posts);
    }
    public function rejected(){
        $posts=Post::where('approved','-1')->where('user_id','<>',null)->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Posts.reject')->with('posts',$posts);
    }
    public function reports(){
        $posts=Post::where('report','>','0')->where('user_id','<>',null)->where('approved','1')->orderBy('report','desc')->with('user','comments')->get();
        return view('Pages.Posts.report')->with('posts',$posts);
    }
    public function admin(){
        $universities=University::all();
        $posts=Post::where('user_id',null)->with('user','comments')->orderBy('id','desc')->get();
        return view('Pages.Posts.admin',['universities'=>$universities,'posts'=>$posts]);
    }
    public function add_post(Request $request){
        if($request->university==null){
            $post=new Post();
            $post->text=$request->text;
            $post->user_id=null;
            $post->approved=1;
            $post->level_department_id=0;
            $post->save();
            if ($request->file('image'))
            {
                $file = $request->file('image');
                $post->addMedia($file)->toMediaCollection('post');
            }
            return back()->with('message','Post Added Successful');
        }
        if($request->faculty=='all'){
            $faculties=Faculty::where('university_id',$request->university)->get();
            foreach ($faculties as $faculty){
                $departments=Department::where('faculty_id',$faculty->id)->get();
                foreach ($departments as $department){
                    $level_departments=LevelDepartment::where('department_id',$department->id)->get();
                    foreach ($level_departments as $level_department){
                        $post=new Post();
                        $post->text=$request->text;
                        $post->user_id=null;
                        $post->approved=1;
                        $post->level_department_id=$level_department->id;
                        $post->save();
                        if ($request->file('image'))
                        {
                            $file = $request->file('image');
                            $post->addMedia($file)->toMediaCollection('post');
                        }
                    }
                }
            }
            return back()->with('message','Post Added Successful');
        }
        else{
            if($request->department=='all'){
                $departments=Department::where('faculty_id',$request->faculty)->get();
                foreach ($departments as $department){
                    $level_departments=LevelDepartment::where('department_id',$department->id)->get();
                    foreach ($level_departments as $level_department){
                        $post=new Post();
                        $post->text=$request->text;
                        $post->user_id=null;
                        $post->approved=1;
                        $post->level_department_id=$level_department->id;
                        $post->save();
                        if ($request->file('image'))
                        {
                            $file = $request->file('image');
                            $post->addMedia($file)->toMediaCollection('post');
                        }
                    }
                }
                return back()->with('message','Post Added Successful');
            }
            else{
                if($request->level=='all'){
                    $level_departments=LevelDepartment::where('department_id',$request->department)->get();
                    foreach ($level_departments as $level_department){
                        $post=new Post();
                        $post->text=$request->text;
                        $post->user_id=null;
                        $post->approved=1;
                        $post->level_department_id=$level_department->id;
                        $post->save();
                        if ($request->file('image'))
                        {
                            $file = $request->file('image');
                            $post->addMedia($file)->toMediaCollection('post');
                        }
                    }
                    return back()->with('message','Post Added Successful');
                }
                else if($request->level!=null){
                    $level_department=LevelDepartment::where('department_id',$request->department)->where('level',$request->level)->first();
                    $post=new Post();
                    $post->text=$request->text;
                    $post->user_id=null;
                    $post->approved=1;
                    $post->level_department_id=$level_department->id;
                    $post->save();
                    if ($request->file('image'))
                    {
                        $file = $request->file('image');
                        $post->addMedia($file)->toMediaCollection('post');
                    }
                }
                return back()->with('message','Post Added Successful');
            }
        }

        return back()->with('message','Error !!! ');
    }
    public function edit_post(Request $request){
        $post=Post::findOrFail($request->id);
        $post->text=$request->text;
        $post->save();
        if ($request->file('image'))
        {
            $file = $request->file('image');
            $post->addMedia($file)->toMediaCollection('post');
        }
        return back()->with('message','Post Edited Successful');
    }
    public function del_post($id){
        $post=Post::findOrFail($id);
        $comments=Comment::where('post_id',$id)->get();
        foreach ($comments as $key=>$comment){
            $comments[$key]->delete();
        }

        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = " تم حذف منشورك بواسطة فريق ساعدني";
        $notification->user_id = $post->user_id;
        $notification->action_type = 'delete';
        $notification->save();
        userNotification($notification);

        $post->delete();

        return back()->with('message','Post Deleted Successful');
    }
    public function to_approve($id){
        $post=Post::findOrFail($id);
        $post->approved=1;
        $post->save();

        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم قبول منشورك";
        $notification->user_id = $post->user_id;
        $notification->action_type = 'approve';
        $notification->save();
        userNotification($notification);

        $levels = UserLevel::where('user_id' , $post->user->id)->pluck('level_id');

        $userss = UserLevel::whereIn('level_id' , $levels)->where('user_id' , '!=' , $post->user->id)->pluck('user_id');
        $users = User::whereIn('id' , $userss)->where('university_id' , $post->user->university_id)->where('faculty_id' , $post->user->faculty_id)->
        where('department_id' , $post->user->department_id)->get();

        foreach ($users as $usr)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع منشور جديد بواسطة ".$post->user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $post->user->id ;
            $notification_patient->action_type = 'post';
            $notification_patient->save();
            userNotification($notification_patient);

        }

//        foreach ($levels as $level)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع منشور جديد بواسطة ".$post->user->full_name;
//            $notification_patient->sender_id = $post->user->id ;
//            $notification_patient->action_type = 'post' ;
//            $notification_patient->level_id = $level ;
//            $notification_patient->save();
//            levelNotification($level , $notification);
//        }

        return back()->with('message','Post Approved Successful');
    }
    public function to_reject($id){
        $post=Post::findOrFail($id);
        $post->approved=-1;
        $post->save();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم رفض منشورك";
        $notification->user_id = $post->user_id;
        $notification->action_type = 'reject';
        $notification->save();
        userNotification($notification);
        return back()->with('message','Post Reject Successful');
    }

    public function comments(){
        $posts=Post::whereNotNull('user_id')->with('user','comments.reply')->has('comments')->orderBy('id','desc')->get();
        /*foreach ($posts as $key1=>$post){
            foreach ($post->comments as $key2=>$comment){
                if($comment->report==0){
                    unset($post->comments[$key2]);
                }
            }
            if($post->comments->count()==0){
                unset($posts[$key1]);
            }
        }*/
        return view('Pages.Posts.comments')->with('posts',$posts);
    }


    public function add_comment(Request $request){
        $comment=new Comment();
        $comment->text=$request->text;
        $comment->approved=1;
        $comment->post_id=$request->post_id;
        $comment->user_id=null;
        $comment->save();
        return back()->with('message','Comment Added Successful');
    }
    public function edit_comment(Request $request){
        $comment=Comment::findOrFail($request->id);
        $comment->text=$request->text;
        $comment->save();
        return back()->with('message','Comment Edited Successful');
    }
    public function del_comment($id){
        $comment=Comment::findOrFail($id);
        if ($comment->user_id != null)
        {
            $notification = new Notification();
            $notification->title = "تم حذف تعليقك بواسطة فريق ساعدني";
            $notification->user_id = $comment->post->user_id ;
            $notification->action_type = 'delete' ;
            $notification->save();
            userNotification($notification);
        }
        $comment->delete();
        return back()->with('message','Comment Deleted Successful');
    }
}
