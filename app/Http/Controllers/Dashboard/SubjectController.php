<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\LevelDepartment;
use App\Models\Notification;
use App\Models\Subject;
use App\Models\SubjectFile;
use App\Models\University;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faculty;

class SubjectController extends Controller
{
    public function index(){
        $faculties=Faculty::with('university')->with('departments')->get();
        $levels=LevelDepartment::with('department')->get();
        $subjects=Subject::with('department')->orderBy('department_id')->with('videos')->with('files')->get();
        return view('Pages.subject',['faculties'=>$faculties,'levels'=>$levels,'subjects'=>$subjects]);
    }
    public function get_subject(Request $request){
        $level_department=LevelDepartment::where('department_id',$request->get('department'))->where('level',$request->get('level'))->first();
        $subjects['data']=Subject::where('department_id',$level_department->id)->get();
        return response()->json($subjects);
    }
    public function Add(Request $request){
        $subject=new Subject();
        $subject->name_ar=$request->name_ar;
        $subject->name_en=$request->name_en;
        $subject->department_id=$request->level_id;
        $subject->save();
        return back()->with('message','Subject Added Successful');
    }

    public function Edit(Request $request,$id){
        $subject=Subject::findOrFail($id);
        $subject->name_ar=$request->name_ar;
        $subject->name_en=$request->name_en;
        $subject->save();
        return back()->with('message','Subject Edited Successful');
    }

    public function Delete($id){
        $subject=Subject::findOrFail($id);
        $subject->delete();
        return back()->with('message','Subject Deleted Successful');
    }

    public function videos(){
        $universities=University::all();
        $files=SubjectFile::where('file_type',0)->with('subject','department')->orderBy('id','desc')->get();
        return view('Pages.Subjects.video',['files'=>$files,'universities'=>$universities]);
    }
    public function video_add(Request $request){
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 0,
            'department_id' => $request->department,
            'level' => $request->level,
            'user_id'=>$request->user,
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',$request->user)->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $request->user)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Video Added Successful');

    }
    public function video_del(Request $request){
        $file=SubjectFile::findOrFail($request->id);
        $file->delete();
        return back()->with('message','Video Deleted Successful');
    }
    public function sheets(){
        $universities=University::all();
        $files=SubjectFile::where('file_type',1)->with('subject','department')->orderBy('id','desc')->get();
        return view('Pages.Subjects.sheet',['files'=>$files,'universities'=>$universities]);
    }
    public function sheet_add(Request $request){
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 1,
            'department_id' => $request->department,
            'level' => $request->level,
            'user_id'=>$request->user,
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',$request->user)->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $request->user)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Sheet Added Successful');

    }

    public function sheet_del(Request $request){
        $file=SubjectFile::findOrFail($request->id);
        $file->delete();
        return back()->with('message','Sheet Deleted Successful');
    }
    public function summary(){
        $universities=University::all();
        $files=SubjectFile::where('file_type',2)->with('subject','department')->orderBy('id','desc')->get();
        return view('Pages.Subjects.summary',['files'=>$files,'universities'=>$universities]);
    }
    public function summary_add(Request $request){
        $subject = SubjectFile::create([
            'name' => $request->name,
            'subject_id' => $request->subject,
            'file_type' => 2,
            'department_id' => $request->department,
            'level' => $request->level,
            'user_id'=>$request->user,
        ]);

        $file = $request->file('file');
        $subject->addMedia($file)->toMediaCollection('subject');
        $subject->save();

        //   Send Notifications To Students
        $user=User::where('id',$request->user)->first();
        $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $request->user)->pluck('user_id');
        $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
        where('department_id', $request->department)->get();
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject)->pluck('user_id'))->get();

        foreach ($users as $use) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($users_subjects as $usr) {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $subject->user_id;
            $notification_patient->action_type = 'file';
            $notification_patient->level_id = $request->level;
            $notification_patient->save();
            userNotification($notification_patient);
        }
        return back()->with('message','Summary Added Successful');

    }

    public function summary_del(Request $request){
        $file=SubjectFile::findOrFail($request->id);
        $file->delete();
        return back()->with('message','Summary Deleted Successful');
    }
}
