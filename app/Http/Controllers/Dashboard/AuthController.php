<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|unique:admins',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ]);

        Admin::create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'role'=>$request->role,
        ]);

        return redirect('/dashboard');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('username', 'password');

        if (Auth::guard('admin')->attempt($credentials)) {

            return redirect()->intended('/dashboard');
        }

        return redirect('dashboard/login')->with('error', 'Please Check Username and Password');
    }

    public function logout() {
        Auth::guard('admin')->logout();

        return redirect('dashboard/login');
    }

    public function login_student()
    {
        return view('Student.login');
    }

    public function authenticate_student(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = $request->only('email', 'password');
        $user=User::where('email',$request->only('email'))->first();
        if($user->type!=0) {
            if (Auth::guard('web')->attempt($credentials)) {
                return redirect('student/subjects/video');
            }
            else{
                return redirect('student/login')->with('error', 'Please Check Email and Password');
            }
        }
        else{
            return redirect('student/login')->with('error', 'Sorry!! .. Excellent Student Only');
        }
    }

    public function logout_student() {
        Auth::guard('web')->logout();

        return redirect('student/login');
    }
}
