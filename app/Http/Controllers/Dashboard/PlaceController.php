<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\PaymentPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaceController extends Controller
{
    public function index(){
        $places=PaymentPlace::all();
        return view('Pages.Management.paymentPlace')->with('places',$places);
    }
    public function add(Request $request){
        $place=new PaymentPlace();
        $place->name=$request->name;
        $place->status=0;
        $place->save();
        return back()->with('message','Place Added Successful');
    }
    public function edit(Request $request){
        $place=PaymentPlace::findOrFail($request->id);
        $place->name=$request->name;
        $place->save();
        return back()->with('message','Place Edited Successful');
    }
    public function delete($id){
        $place=PaymentPlace::findOrFail($id);
        $place->delete();
        return back()->with('message','Place Deleted Successful');
    }
    public function active($id){
        $place=PaymentPlace::findOrFail($id);
        if($place->status==1){
            $place->status=0;
            $place->save();
            return back()->with('message','Place Un Active Successful');
        }
        else{
            $place->status=1;
            $place->save();
            return back()->with('message','Place Active Successful');
        }
    }
}
