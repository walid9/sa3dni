<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Notification;
use App\Models\UserLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends Controller
{
    public function approved(){
        $news=News::where('approved','1')->where('user_id','<>',null)->orderBy('id','desc')->with('user')->get();
        return view('Pages.News.approved')->with('news',$news);
    }
    public function waiting(){
        $news=News::where('approved','0')->where('user_id','<>',null)->orderBy('id','desc')->with('user')->get();
        return view('Pages.News.wait')->with('news',$news);
    }
    public function rejected(){
        $news=News::where('approved','-1')->where('user_id','<>',null)->orderBy('id','desc')->with('user')->get();
        return view('Pages.News.reject')->with('news',$news);
    }
    public function reports(){
        $news=News::where('report','>','0')->where('user_id','<>',null)->where('approved','1')->orderBy('report','desc')->with('user')->get();
        return view('Pages.News.report')->with('news',$news);
    }
    public function admin(){
        $news=News::where('user_id',null)->with('user')->orderBy('id','desc')->get();
        return view('Pages.News.admin')->with('news',$news);
    }
    public function add_news(Request $request){
        $news=new News();
        $news->newsText=$request->text;
        $news->approved=1;
        $news->user_id=null;
        $news->save();
        return back()->with('message','News Added Successful');
    }
    public function edit_news(Request $request){
        $news=News::findOrFail($request->id);
        $news->newsText=$request->text;
        $news->save();
        return back()->with('message','News Edited Successful');
    }
    public function del_news($id){
        $news=News::findOrFail($id);
        $news->delete();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم حذف الخبر";
        $notification->user_id = $news->user_id;
        $notification->action_type = 'delete';
        $notification->save();
        userNotification($notification);
        return back()->with('message','News Deleted Successful');
    }
    public function to_approve($id){
        $news=News::findOrFail($id);
        $news->approved=1;
        $news->save();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم قبول الخبر";
        $notification->user_id = $news->user_id;
        $notification->action_type = 'approve';
        $notification->save();
        userNotification($notification);

        $levels = UserLevel::where('user_id' , $news->user->id)->pluck('level_id');

        foreach ($levels as $level)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع خبر جديد بواسطة ".$news->user->full_name;
            $notification_patient->sender_id = $news->user->id ;
            $notification_patient->action_type = 'news' ;
            $notification_patient->level_id = $level ;
            $notification_patient->save();
            levelNotification($level , $notification);
        }

        return back()->with('message','News Approved Successful');
    }
    public function to_reject($id){
        $news=News::findOrFail($id);
        $news->approved=-1;
        $news->save();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم رفض الخبر";
        $notification->user_id = $news->user_id;
        $notification->action_type = 'reject';
        $notification->save();
        userNotification($notification);
        return back()->with('message','News Reject Successful');
    }
}
