<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Comment;
use App\Models\Faculty;
use App\Models\LevelDepartment;
use App\Models\News;
use App\Models\Notification;
use App\Models\PaymentRequest;
use App\Models\Post;
use App\Models\QComment;
use App\Models\Question;
use App\Models\SubjectFile;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserPromoCode;
use App\Models\UserSubject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function all(){
        $faculties=Faculty::all();
        return view('Pages.Users.all',['users'=>$this->users_data("0",0),'faculties'=>$faculties]);
    }
    public function normal(){
        $faculties=Faculty::all();
        return view('Pages.Users.normal',['users'=>$this->users_data("0",1),'faculties'=>$faculties]);
    }
    public function excellent(){
        return view('Pages.Users.excellent',['users'=>$this->users_data("1",1)]);
    }
    public function balance(){
        return view('Pages.Users.balance',['users'=> $this->users_data('balance',1)]);
    }
    public function add_level(Request $request){
        $level_department=LevelDepartment::where('level',$request->level)->where('department_id',$request->department)->first();
        $check_exist=UserLevel::where('user_id',$request->user_id)->where('level_id',$level_department->id)->first();
        if($check_exist){
            return back()->with('failed','User Level Existed');
        }
        $level_user=new UserLevel();
        $level_user->user_id=$request->user_id;
        $level_user->level_id=$level_department->id;
        $level_user->save();
        return back()->with('success','User Level Added Successful');
    }
    public function del_level($id){
        $level_user=UserLevel::findOrFail($id);
        $level_user->delete();
        return back()->with('success','User Level Deleted Successful');
    }
    public function change_department(Request $request){
        $user=User::findOrFail($request->user_id);
        $user->department_id=$request->department_id;
        $user->save();
        $level_department=LevelDepartment::where('department_id',$request->department_id)->where('level',$request->level)->first();
        $user_level=UserLevel::where('user_id',$request->user_id)->first();
        $user_level->level_id=$level_department->id;
        $user_level->save();
        return back()->with('success','User Edited Successful');
    }
    public function del_user($id){
        $user=User::findOrFail($id);
        $user_id=$user->id;
        $levels=UserLevel::where('user_id',$user_id)->get();
        foreach ($levels as $key=>$level){
            $levels[$key]->delete();
        }
        $subjects=UserSubject::where('user_id',$user_id)->get();
        foreach ($subjects as $key=>$subject){
            $subjects[$key]->delete();
        }
        $notifications=Notification::where('user_id',$user_id)->orWhere('sender_id',$user_id)->get();
        foreach ($notifications as $key=>$notification){
            $notifications[$key]->delete();
        }
        //DB::delete('delete from media where model_type = "App\Models\User" where model_id = ?',[$user_id]);
        //Delete Media
        DB::table('media')->where('model_type','App\Models\User')->where('model_id',$user_id)->delete();

        $user->delete();
        return back()->with('success','User Deleted Successful');
    }
    public function to_normal($id){
        $user=User::findOrFail($id);
        $user->type='0';
        $user->verified=0;
        $user->status=1;
        $user->deadline=Carbon::now()->addDay(3)->format('Y-m-d');
        $user->save();
        $levels=UserLevel::where('user_id',$id)->get();
        foreach ($levels as $key=>$level){
            if($key>0){
                $levels[$key]->delete();
            }
        }
        return back()->with('success','User Begin Normal Successful');
    }
    public function to_excellent($id){
        $user=User::findOrFail($id);
        $user->type='1';
        $user->verified=1;
        $user->status=1;
        //$user->deadline=Carbon::now()->addDay(360)->format('Y-m-d');
        $user->deadline="2022-11-01";
        $user->save();
        return back()->with('success','User Begin Excellent Successful');
    }
    public function verified(Request $request){
        $user=User::findOrFail($request->id);
        $user->verified=!$user->verified;
        $user->save();
        if($user->verified){
            $message="تم توثيق حسابك";
        }
        else{
            $message="تم الغاء توثيق حسابك";
        }

        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = $message;
        $notification->user_id = $user->id;
        $notification->action_type = 'verified';
        $notification->save();
        userNotification($notification);
        return back()->with('success','User Edited Successful');
    }

    public function rank(Request $request){
        $rank=$request->rank?$request->rank:0;

        $user=User::findOrFail($request->id);
        if($rank!=$user->rank) {
            if ($rank > $user->rank) {
                $message="تم زيادة نقاطك ";
            }
            else{
                $message="تم نقص نقاطك";
            }
            $user->rank = $rank;
            $user->save();

            //   Send Notifications To Students
            $notification = new Notification();
            $notification->title = $message;
            $notification->user_id = $user->id;
            $notification->action_type = 'rank';
            $notification->save();
            userNotification($notification);

            return back()->with('success', 'User Edited Successful');
        }
    }

    public function Request_Payment(){
        $transactions=PaymentRequest::with('user','user.university','user.faculty','user.department')->orderBy('id','desc')->where('status',0)->get();
        return view('Pages.Users.request_paid')->with('transactions',$transactions);
    }
    public function Approve_Request(Request $request){
        $transaction=PaymentRequest::findOrFail($request->id);
        $transaction->status=1;
        $transaction->save();

        $user=User::findOrFail($transaction->user_id);
        $user->status=1;
        $deadline=Carbon::now()->addDays($request->count*30);
        $user->deadline=$deadline->toDateString();
        $user->save();

        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم قبول طلب دفعك بنجاح وتم تفعيل حسابك";
        $notification->user_id = $user->id;
        $notification->action_type = 'payment';
        $notification->save();
        userNotification($notification);

        return back()->with('success','User Payment Successful');
    }
    public function del_Request($id){
        $transaction=PaymentRequest::findOrFail($id);
        $transaction->delete();
        return back()->with('success','User Request Deleted');
    }
    public function deadline(){
        $i=0;
        $yesterday=date('Y-m-d',strtotime("-1 days"));
        //Delete Notifications
        $expire_date=date('Y-m-d',strtotime("-4 days"));
        Notification::where('created_at','<=',$expire_date)->delete();

        $users=User::where('status','1')->get();
        foreach ($users as $key=>$user){
            if($user->deadline<=$yesterday){
                $i++;
                $users[$key]->status=0;
                $users[$key]->save();
            }
        }
        return back()->with('success','Check Deadline Successful Count : '.$i.' Users');

    }

    public function status(Request $request){
        $user=User::findOrFail($request->id);
        if($request->status=="on"){
            $user->status=1;
        }
        else{
            $user->status=0;
        }
        $user->deadline=$request->deadline;
        $user->save();
        return back()->with('success','Updated User Successful');
    }

    public function statics(){
        $faculties=Faculty::with('university')->with('departments')->get();
        $levels=LevelDepartment::with('department')->orderBy('level')->get();
        foreach ($levels as $key=>$level) {
            $count=UserLevel::where('level_id',$level->id)->count();
            Arr::add($levels[$key],'count',$count);
        }
        return view('Pages.Users.statics',['faculties'=>$faculties,'levels'=>$levels]);
    }

    public function reset(Request $request){
        $user=User::findOrFail($request->id);
        $user->device_id=null;
        $user->save();
        return back()->with('success','User Reset Successful');
    }
    public function promo(Request $request){
        $promo=UserPromoCode::where('user_id',$request->id)->first();
        $promo->status=!$promo->status;
        $promo->save();
        return back()->with('success','User Promo Edited Successful');
    }
    public function users_data($type,$email_verified){
        if($type=='balance'){
            $users = User::with('faculty', 'university', 'department', 'userlevel.level.department','promo_code','balance')->whereHas('balance',function ($q){
                $q->where('cache','>',0)->orWhere('service','>',0);
            })->get();
        }
        else {
            if ($email_verified) {
                $users = User::with('faculty', 'university', 'department', 'userlevel.level.department', 'promo_code', 'balance')->where('type', $type)->whereNotNull('email_verified_at')->get();
            } else {
                $users = User::with('faculty', 'university', 'department', 'userlevel.level.department', 'promo_code', 'balance')->where('type', $type)->get();
            }
        }
        foreach ($users as $key =>$user){
            $levels  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->with('department')->get();
            $file_video=SubjectFile::where('user_id',$user->id)->where('file_type',0)->count();
            $file_pdf=SubjectFile::where('user_id',$user->id)->where('file_type','<>',0)->count();
            $news= News::where('user_id',$user->id)->count();
            $post= Post::where('user_id',$user->id)->count();
            $question=Question::where('user_id',$user->id)->count();
            $comment_post=Comment::where('user_id',$user->id)->count();
            $comment_question=QComment::where('user_id',$user->id)->count();
            Arr::add($users[$key],'videos',$file_video);
            Arr::add($users[$key],'pdf',$file_pdf);
            Arr::add($users[$key],'levels',$levels);
            Arr::add($users[$key],'news',$news);
            Arr::add($users[$key],'post',$post);
            Arr::add($users[$key],'question',$question);
            Arr::add($users[$key],'comment',$comment_post+$comment_question);
        }
        return $users;
    }

    public function user_data(Request $request){
        $user = User::with('faculty', 'university', 'department', 'userlevel.level.department','promo_code','balance')->where('id', $request->get('id'))->first();
        $levels  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->with('department')->get();
        $file_video=SubjectFile::where('user_id',$user->id)->where('file_type',0)->count();
        $file_pdf=SubjectFile::where('user_id',$user->id)->where('file_type','<>',0)->count();
        $news= News::where('user_id',$user->id)->count();
        $post= Post::where('user_id',$user->id)->count();
        $question=Question::where('user_id',$user->id)->count();
        $comment_post=Comment::where('user_id',$user->id)->count();
        $comment_question=QComment::where('user_id',$user->id)->count();
        Arr::add($user,'videos',$file_video);
        Arr::add($user,'pdf',$file_pdf);
        Arr::add($user,'levels',$levels);
        Arr::add($user,'news',$news);
        Arr::add($user,'post',$post);
        Arr::add($user,'question',$question);
        Arr::add($user,'comment',$comment_post+$comment_question);
        return response()->json($user);
    }

    public function get_user(Request $request){
        $level_department=LevelDepartment::where('department_id',$request->get('department'))->where('level',$request->get('level'))->first();
        $user_level=UserLevel::where('level_id',$level_department->id)->get(['user_id']);
        $excellent=User::where('type',1)->whereIn('id',$user_level)->get();
        $users['data']=$excellent;
        return response()->json($users);
    }
}
