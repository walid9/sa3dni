<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Ad;
use App\Models\AdLevel;
use App\Models\Department;
use App\Models\LevelDepartment;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AdsController extends Controller
{
    public function index(){
        $universities=University::all();
        $ads=Ad::with('faculty')->orderBy('active','desc')->get();
        foreach ($ads as $key=>$ad){
            $levels=AdLevel::where('ad_id',$ad->id)->with('level_department.department')->get();
            Arr::add($ads[$key], 'level', $levels);
        }
        return view('Pages.Management.ads',['universities'=>$universities,'ads'=>$ads]);
    }
    public function create(Request $request){
        $ad=new Ad();
        $file = $request->file('image');
        $ad->addMedia($file)->toMediaCollection('ad');
        $ad->link=$request->link?$request->link:NULL;
        $ad->start_date=$request->start_date;
        $ad->end_date=$request->end_date;
        $ad->faculty_id=$request->faculty;
        $ad->type=$request->type;
        $ad->active=0;
        $ad->save();
        $departments=Department::where('faculty_id',$request->faculty)->get('id');
        $levels=LevelDepartment::whereIn('department_id',$departments)->get();
        foreach ($levels as $level){
            $ad_level=new AdLevel();
            $ad_level->ad_id=$ad->id;
            $ad_level->level_department_id=$level->id;
            $ad_level->save();
        }
        return back()->with('message','Ad Added Successful');
    }
    public function active($id){
        $ad=Ad::findOrFail($id);
        if($ad->active){
            $ad->active=0;
            $ad->save();
            return back()->with('message','Ad Un Active Successful');
        }
        else{
            $ad->active=1;
            $ad->save();
            return back()->with('message','Ad Active Successful');
        }
    }

    public function edit(Request $request){
        $ad=Ad::findOrFail($request->id);
        if($request->file('image')) {
            $file = $request->file('image');
            $ad->addMedia($file)->toMediaCollection('ad');
        }
        $ad->link=$request->link?$request->link:NULL;
        $ad->start_date=$request->start_date;
        $ad->end_date=$request->end_date;
        $ad->type=$request->type;
        $ad->save();
        return back()->with('message','Ad Edited Successful');
    }
    public function delete($id){
        $ad=Ad::findOrFail($id);
        $ad->delete();
        return back()->with('message','Ad Deleted Successful');
    }

    public function add_level(Request $request){
        $old=AdLevel::where('ad_id',$request->ad_id)->where('level_department_id',$request->level_department)->first();
        if($old){
            return back()->with('message','Level Already Exist');
        }
        $level=new AdLevel();
        $level->ad_id=$request->ad_id;
        $level->level_department_id=$request->level_department;
        $level->save();
        return back()->with('message','Level Added Successful');

    }

    public function del_level($id){
        $level=AdLevel::findOrFail($id);
        $level->delete();
        return back()->with('message','Level Deleted Successful');
    }
}
