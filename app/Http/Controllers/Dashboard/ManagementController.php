<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AppImage;
use App\Models\Managament;
use Illuminate\Http\Request;

class ManagementController extends Controller
{
    public function android(){
        $tools=Managament::where('os','android')->get();
        return view('Pages.Management.android')->with('tools',$tools);
    }

    public function android_tool($id){
        $management=Managament::findOrFail($id);
        if($management->status){
            $management->status=0;
            $management->save();
            return back()->with('message','Tool Un Active Successful');
        }
        else{
            $management->status=1;
            $management->save();
            return back()->with('message','Tool Active Successful');
        }

    }

    public function img_app(){
        $images=AppImage::all();
        return view('Pages.Management.app_images')->with('images',$images);
    }

    public function add_img_app(Request $request){
        $image=new AppImage();
        $image->type=$request->type;
        if ($request->file('image'))
        {
            $file = $request->file('image');
            $image->addMedia($file)->toMediaCollection('app_images');
        }
        $image->save();
        return back()->with('message','Image Added Successful');
    }

    public function del_img_app($id){
        $image=AppImage::findOrFail($id);
        $image->delete();
        return back()->with('message','Image Deleted Successful');
    }
}
