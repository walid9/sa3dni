<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\FacultyPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faculty;
use App\Models\University;

class FacultyController extends Controller
{
    public function index(){
        $universities=University::with('facultys')->get();
        return view('Pages.faculty')->with('universities',$universities);
    }

    public function Add(Request $request){
        $faculty=new Faculty();
        $faculty->name=$request->name;
        $faculty->university_id=$request->university_id;
        $faculty->save();
        return back()->with('message','Faculty Added Successful');
    }

    public function Edit(Request $request,$id){
        $faculty=Faculty::findOrFail($id);
        $faculty->name=$request->name;
        $faculty->save();
        return back()->with('message','Faculty Edited Successful');
    }

    public function Delete($id){
        $faculty=Faculty::findOrFail($id);
        $faculty->delete();
        return back()->with('message','Faculty Deleted Successful');

    }

    public function get_faculty(Request $request){
        $faculties['data']=Faculty::where('university_id',$request->get('id'))->get();
        return response()->json($faculties);
    }

    public function Price(){
        $universities=University::all();
        $objects=FacultyPrice::all();
        return view('Pages.Management.facultyPrice',['universities'=>$universities,'objects'=>$objects]);
    }
    public function add_price(Request $request){
        $object=new FacultyPrice();
        $object->university_id=$request->university;
        $object->faculty_id=$request->faculty;
        $object->price=$request->price;
        $object->period=$request->period;
        $object->save();
        return back()->with('message','Faculty Price Added Successful');
    }
    public function edit_price(Request $request){
        $object=FacultyPrice::findOrFail($request->id);
        $object->price=$request->price;
        $object->period=$request->period;
        $object->save();
        return back()->with('message','Faculty Price Edited Successful');
    }
    public function delete_price($id){
        $object=FacultyPrice::findOrFail($id);
        $object->delete();
        return back()->with('message','Faculty Price Deleted Successful');
    }
}
