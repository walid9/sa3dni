<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\QrCode;
use Illuminate\Http\Request;

class QrCodeController extends Controller
{
    public function index(){
        $codes=QrCode::all();
        return view('Pages.Management.qr')->with('codes',$codes);
    }

    public function add(Request $request){
        $code=new QrCode();
        $code->code=$request->code;
        $code->description=$request->description?$request->description:"";
        $code->period=$request->period;
        $code->price=$request->price;
        $code->save();
        return back()->with('message','QrCode Added Successful');
    }
    public function edit(Request $request){
        $code=QrCode::findOrFail($request->id);
        $code->code=$request->code;
        $code->description=$request->description?$request->description:"";
        $code->period=$request->period;
        $code->price=$request->price;
        $code->save();
        return back()->with('message','QrCode Edited Successful');
    }
    public function delete($id){
        $code=QrCode::findOrFail($id);
        $code->delete();
        return back()->with('message','QrCode Deleted Successful');
    }
}
