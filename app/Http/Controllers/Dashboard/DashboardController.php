<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\Question;
use App\Models\SubjectFile;
use App\Models\User;
use App\Models\University;
use App\Models\Faculty;
use App\Models\Department;
use App\Models\Subject;
use App\Models\Admin;
use App\Models\Post;
use App\Models\News;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users_all=User::all()->count();
        $users_verified=User::whereNotNull('email_verified_at')->count();
        $users_unverified=User::whereNull('email_verified_at')->count();
        $users_excellent=User::where('type','1')->count();
        $users_normal=User::where('type','0')->whereNotNull('email_verified_at')->count();
        $users_paid=User::where('type','0')->whereNotNull('email_verified_at')->where('status',1)->count();
        $university=University::all()->count();
        $faculties=Faculty::all()->count();
        $departments=Department::all()->count();
        $subjects=Subject::all()->count();
        $admins=Admin::all()->count();
        $videos=SubjectFile::where('file_type',0)->count();
        $sheets=SubjectFile::where('file_type',1)->count();
        $summary=SubjectFile::where('file_type',2)->count();
        $contact=ContactUs::all()->count();
        $posts=Post::all()->count();
        $posts_approve=Post::where('approved','1')->count();
        $posts_wait=Post::where('approved','0')->count();
        $posts_reject=Post::where('approved','-1')->count();
        $news=News::all()->count();
        $news_approve=News::where('approved','1')->count();
        $news_wait=News::where('approved','0')->count();
        $news_reject=News::where('approved','-1')->count();
        $questions=Question::all()->count();
        $questions_approve=Question::where('approved','1')->count();
        $questions_wait=Question::where('approved','0')->count();
        $questions_reject=Question::where('approved','-1')->count();
        return view('Pages.dashboard',
            ['users_all'=>$users_all,'users_excellent'=>$users_excellent,'users_normal'=>$users_normal,
                'users_paid'=>$users_paid,'users_verified'=>$users_verified,'users_unverified'=>$users_unverified,
                'admins'=>$admins,'universities'=>$university,'faculties'=>$faculties,'department'=>$departments,
                'subjects'=>$subjects,'videos'=>$videos,'sheets'=>$sheets,'summary'=>$summary,'contact'=>$contact,
                'posts'=>$posts,'posts_approve'=>$posts_approve,'posts_wait'=>$posts_wait,'posts_reject'=>$posts_reject,
                'news'=>$news,'news_approve'=>$news_approve,'news_wait'=>$news_wait,'news_reject'=>$news_reject,
                'questions'=>$questions,'questions_approve'=>$questions_approve,'questions_wait'=>$questions_wait,'questions_reject'=>$questions_reject
            ]
        );
    }
}
