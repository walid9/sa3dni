<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Department;
use App\Models\LevelDepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faculty;

class DepartmentController extends Controller
{
    public function index(){
        $faculties=Faculty::with('university')->with('departments')->get();
        $levels=LevelDepartment::with('department')->orderBy('level')->get();
        //dd($faculties);
        return view('Pages.department',['faculties'=>$faculties,'levels'=>$levels]);
    }

    public function get_department(Request $request){
        $departments['data']=Department::where('faculty_id',$request->get('id'))->get();
        return response()->json($departments);
    }
    public function get_level(Request $request){
        $levels['data']=LevelDepartment::where('department_id',$request->get('id'))->get();
        return response()->json($levels);
    }
    public function Add(Request $request){
        $department=new Department();
        $department->name=$request->name;
        $department->faculty_id=$request->faculty_id;
        $department->save();
        return back()->with('message','Department Added Successful');
    }

    public function Edit(Request $request,$id){
        $department=Department::findOrFail($id);
        $department->name=$request->name;
        $department->faculty_id=$request->faculty_id;
        $department->save();
        return back()->with('message','Department Edited Successful');
    }

    public function Delete($id){
        $department=Department::findOrFail($id);
        $department->delete();
        return back()->with('message','Department Deleted Successful');
    }

    public function Add_level(Request $request){
        $level=new LevelDepartment();
        $level->level=$request->level;
        $level->department_id=$request->department_id;
        $level->save();
        return back()->with('message','Level Added Successful');
    }

    public function Delete_level($id){
        $level=LevelDepartment::findOrFail($id);
        $level->delete();
        return back()->with('message','Level Deleted Successful');
    }
}
