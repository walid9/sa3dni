<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Trial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrialController extends Controller
{
    public function index(){
        $trials=Trial::all();
        return view('Pages.Management.trial')->with('trials',$trials);
    }

    public function edit(Request $request){
        $trial=Trial::findOrFail($request->id);
        $trial->days=$request->days;
        $trial->save();
        return back()->with('message','Trial Edited Successful');
    }
}
