<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Notification;
use App\Models\QComment;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserSubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Question;

class QuestionController extends Controller
{
    public function approved(){
        $questions=Question::where('approved','1')->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Questions.approved')->with('questions',$questions);
    }
    public function waiting(){
        $questions=Question::where('approved','0')->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Questions.wait')->with('questions',$questions);
    }
    public function rejected(){
        $questions=Question::where('approved','-1')->orderBy('id','desc')->with('user','comments')->get();
        return view('Pages.Questions.reject')->with('questions',$questions);
    }
    public function reports(){
        $questions=Question::where('report','>','0')->where('approved','1')->orderBy('report','desc')->with('user','comments')->get();
        return view('Pages.Questions.report')->with('questions',$questions);
    }
    public function to_approve($id){
        $question=Question::findOrFail($id);
        $question->approved=1;
        $question->save();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم قبول سؤالك";
        $notification->user_id = $question->user_id;
        $notification->action_type = 'approve';
        $notification->save();
        userNotification($notification);

        $levels = UserLevel::where('user_id' , $question->user->id)->pluck('level_id');
        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $question->subject_id)->pluck('user_id'))->get();

        foreach ($users_subjects as $use)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع سؤال جديد بواسطة ".$question->user->full_name;
            $notification_patient->user_id = $use->id;
            $notification_patient->sender_id = $question->user->id ;
            $notification_patient->action_type = 'question' ;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        foreach ($levels as $level)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع سؤال جديد بواسطة ".$question->user->full_name;
            $notification_patient->sender_id = $question->user->id ;
            $notification_patient->action_type = 'question' ;
            $notification_patient->level_id = $level ;
            $notification_patient->save();
            levelNotification($level , $notification);
        }

        return back()->with('message','Question Approved Successful');
    }
    public function to_reject($id){
        $question=Question::findOrFail($id);
        $question->approved=-1;
        $question->save();
        //   Send Notifications To Students
        $notification = new Notification();
        $notification->title = "تم رفض سؤالك";
        $notification->user_id = $question->user_id;
        $notification->action_type = 'reject';
        $notification->save();
        userNotification($notification);
        return back()->with('message','Question Reject Successful');
    }
    public function delete($id){
        $question=Question::findOrFail($id);
        $comments=QComment::where('question_id',$id)->get();
        foreach ($comments as $key=>$comment){
            $comments[$key]->delete();
        }
        $question->delete();
        return back()->with('message','Question Deleted Successful');
    }
    public function comments(){
        $questions=Question::orderBy('id','desc')->with('user')->has('comments')->get();
        /*
        foreach ($questions as $key1=>$question){
            foreach ($question->comments as $key2=>$comment){
                if($comment->report==0){
                    unset($question->comments[$key2]);
                }
            }
            if($question->comments->count()==0){
                unset($questions[$key1]);
            }
        }*/
        return view('Pages.Questions.comments')->with('questions',$questions);
    }
    public function del_comment($id){
        $comment=QComment::findOrFail($id);
        if ($comment->user_id !== null)
        {
            $notification = new Notification();
            $notification->title = "تم حذف تعليقك بواسطة فريق ساعدني";
            $notification->user_id = $comment->post->user_id ;
            $notification->action_type = 'delete' ;
            $notification->save();
            userNotification($notification);
        }
        $comment->delete();
        return back()->with('message','Comment Deleted Successful');
    }

}
