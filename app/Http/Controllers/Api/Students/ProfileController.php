<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\profile\ProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\LevelDepartment;
use App\Models\User;
use App\Models\UserLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class ProfileController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/profile/",
     *      operationId="get User Information",
     *      tags={"profile"},
     *      summary="Get Profile information for user",
     *      description="Get Profile information for user",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Profile", "read:Profile"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        try {
            if (request()->header('authorization')&&request()->header('authorization')!="") {
                try {
                    $user = \JWTAuth::parseToken()->authenticate();
                    return apiResponse(200, trans('messages.success', [] , $request->header('lang')), new UserResource($user));

                } catch (JWTException $e) {
                    return apiResponse(501, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
                }
            }
            else{
                return apiResponse(415, trans('messages.login', [] , $request->header('lang')));
            }
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');
        }
    }

    /**
     * @SWG\Post(
     *      path="/profile/update",
     *      operationId="update profile for user",
     *      tags={"profile"},
     *      summary="update user profile",
     *      description="update user profile",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *     @SWG\Parameter(
     *          name="full_name",
     *          description="Full name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="user_name",
     *          description="User name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *        @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="phone",
     *          description="User phone",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="university_id",
     *          description="university id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="faculty_id",
     *          description="faculty id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="department_id",
     *          description="department id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="level",
     *          description="level",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="image",
     *          description="user image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="staff_img",
     *          description="staff image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:category", "read:category"}
     *         }
     *     },
     * )
     *
     */

    public function update(ProfileRequest $request)
    {
        try {
            if (request()->header('authorization')) {
                try {
                    $user = \JWTAuth::parseToken()->authenticate();

                } catch (JWTException $e) {

                    return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
                }
            }

            if ($request->phone) {
                if (User::where('id', '!=', $user->id)->where('phone', $request->phone)->first())
                    return apiResponse(401, trans('messages.phone_exist', [] , $request->header('lang')));
            }

            if ($request->email) {
                if (User::where('id', '!=', $user->id)->where('email', $request->email)->first())
                    return apiResponse(401, trans('messages.email_exist', [] , $request->header('lang')));
            }

            $user->update($request->all());

            if ($request['password'])
            {
                if (Hash::check($request->password , $user->password)) {
                    return apiResponse(400, trans('messages.change_password' , [] , $request->header('lang')));
                }

                $user->password = Hash::make($request['password']);
                $user->save();
            }

            if (($user->type == 1 || $user->type == 2) && $request->file('staff_img'))
            {
                $file = $request->file('staff_img');
                $user->addMedia($file)->toMediaCollection('staffImg');
            }

            if ($request->file('image'))
            {
                $file = $request->file('image');
                $user->addMedia($file)->toMediaCollection('user');
            }

            if (strlen($request->level) == 1)
            {
                UserLevel::where('user_id' , $user->id)->delete();
                $level = LevelDepartment::where('department_id' , $request->department_id)->where('level' , $request->level)->first();
                UserLevel::create([
                    'user_id' => $user->id,
                    'level_id' => $level->id,
                ]);
            }

            if (strlen($request->level) > 1)
            {
                UserLevel::where('user_id' , $user->id)->delete();
                $levels = array_map('intval', explode(',', $request['level']));
                foreach ($levels as $levl)
                {
                    $level = LevelDepartment::where('department_id' , $request->department_id)->where('level' , $levl)->first();
                    UserLevel::create([
                        'user_id' => $user->id,
                        'level_id' => $level->id,
                    ]);
                }
            }


            return apiResponse(200, trans('messages.success', [] , $request->header('lang')), new UserResource($user));
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }
}
