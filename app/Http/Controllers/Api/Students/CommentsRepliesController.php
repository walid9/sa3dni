<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentsResource;
use App\Http\Resources\RepliesResource;
use App\Models\Comment;
use App\Models\CommentReply;
use App\Models\Notification;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class CommentsRepliesController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/replies/create",
     *      operationId="Create reply",
     *      tags={"Replies"},
     *      summary="Create reply",
     *      description="Create reply",
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function create(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = new CommentReply();
        $comment->comment_id = $request['comment_id'];
        $comment->text = $request['text'];
        $comment->user_id = $user->id;
        $comment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $comment->addMedia($file)->toMediaCollection('reply');
        }

        //   Send Notifications To Student

        if ($comment->comment->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الرد على التعليق التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $comment->comment->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'reply' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }

        /*
        if ($comment->comment->user_id == $user->id)
        {
            foreach (Comment::where('user_id' , '!=' , $comment->comment->user_id)->get() as $us)
            {
                $notification_patient = new Notification();
                $notification_patient->title = "تم الرد على التعليق المشارك به بالتعليقات بواسطة ".$user->full_name;
                $notification_patient->user_id = $us->user_id ;
                $notification_patient->sender_id = $user->id ;
                $notification_patient->action_type = 'comment' ;
                $notification_patient->save();
                userNotification($notification_patient);
            }
        }
        */
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/replies/edit",
     *      operationId="edit reply",
     *      tags={"Replies"},
     *      summary="edit reply",
     *      description="edit reply",
     *      @SWG\Parameter(
     *          name="reply_id",
     *          description="reply id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function update(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = CommentReply::find($request->reply_id);
        $comment->text = $request['text'];
        $comment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $comment->addMedia($file)->toMediaCollection('reply');
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/replies/delete",
     *      operationId="delete replies",
     *      tags={"Replies"},
     *      summary="delete replies",
     *      description="delete replies",
     *      @SWG\Parameter(
     *          name="reply_id",
     *          description="reply id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        CommentReply::find($request->reply_id)->delete();
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }


    /**
     * @SWG\Post(
     *      path="/replies/index",
     *      operationId="Show replies",
     *      tags={"Replies"},
     *      summary="Show replies",
     *      description="get replies",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *       @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comments = CommentReply::where('comment_id' , $request->comment_id)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), RepliesResource::collection($comments));

    }

    /**
     * @SWG\Post(
     *      path="/replies/like",
     *      operationId="like replies",
     *      tags={"Replies"},
     *      summary="like replies",
     *      description="like replies",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reply_id",
     *          description="reply id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function like(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = CommentReply::find($request->reply_id);
        if ($comment->liked_by !== null && in_array(json_encode($user->id) , json_decode($comment->liked_by)) == 1)
        {
            return apiResponse(400, trans('like_exist' , [] , $request->header('lang')));

        }
        $comment->likes +=1;
        $liked_by = [];

        if ($comment->liked_by == null)
            $liked_by[] = json_encode($user->id);

        if ($comment->liked_by !== null)
        {
            $liked_by = json_decode($comment->liked_by);

            array_push($liked_by , json_encode($user->id));
        }

        $comment->liked_by =$liked_by ;
        $comment->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/replies/dislike",
     *      operationId="dislike replies",
     *      tags={"Replies"},
     *      summary="dislike replies",
     *      description="dislike replies",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reply_id",
     *          description="reply id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function dislike(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = CommentReply::find($request->reply_id);
        if (in_array(json_encode($user->id) , json_decode($comment->liked_by)) == 1)
        {
            $liked_by = [];
            for($i = 0 ; $i < count(json_decode($comment->liked_by)) ; $i++)
            {
                if (json_decode($comment->liked_by)[$i] == $user->id)
                    continue;

                if (json_decode($comment->liked_by)[$i] !== $user->id)
                {
                    $liked_by []=json_decode($comment->liked_by)[$i];
                }
            }

            $comment->liked_by = $liked_by;
            $comment->likes -=1;
            $comment->save();
            return apiResponse(200, trans('Success' , [] , $request->header('lang')));

        }

    }

    /**
     * @SWG\Post(
     *      path="/replies/report",
     *      operationId="report replies",
     *      tags={"Replies"},
     *      summary="report replies",
     *      description="report replies",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reply_id",
     *          description="reply id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $comment = CommentReply::find($request->reply_id);
        $comment->report +=1;
        $comment->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }
}
