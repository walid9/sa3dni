<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\PaymentRequest;
use App\Models\QrCode;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentRequestController extends Controller
{
    /**
     * @SWG\Post(
     *      path="/payment-request/send",
     *      operationId="send payment-request",
     *      tags={"payment request"},
     *      summary="send payment-request",
     *      description="send payment-request",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="transaction_number",
     *          description="transaction_number",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function send(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $req = new PaymentRequest();
        $req->user_id =$user->id;
        $req->transaction_number =$request->transaction_number;
        $req->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/qr-code/send",
     *      operationId="send qr-code",
     *      tags={"payment request"},
     *      summary="send qr-code",
     *      description="send qr-code",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="code",
     *          description="code",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function qrcode(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();

        if (QrCode::where('code' , $request->code)->first())
        {
            //Todo update status of user to paid with deadline (as in admin dashboard)
            $code=QrCode::where('code' , $request->code)->first();
            $user->status=1;
            $deadline=Carbon::now()->addDays(30*$code->period); //One Month
            $user->deadline=$deadline->toDateString();
            $user->save();
            //Send Notifications To Students
            $notification = new Notification();
            $notification->title = "تم قبول طلب دفعك بنجاح وتم تفعيل حسابك";
            $notification->user_id = $user->id;
            $notification->action_type = 'payment';
            $notification->save();
            userNotification($notification);
            return apiResponse(200, trans('Success' , [] , $request->header('lang')));
        }

        return apiResponse(400, trans('Error' , [] , $request->header('lang')));

    }
}
