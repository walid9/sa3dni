<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentsResource;
use App\Http\Resources\PostsResource;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Post;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/comments/create",
     *      operationId="Create Comment",
     *      tags={"Comments"},
     *      summary="Create Comment",
     *      description="Create Comment",
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function create(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = new Comment();
        $comment->post_id = $request['post_id'];
        $comment->text = $request['text'];
        $comment->user_id = $user->id;
        $comment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $comment->addMedia($file)->toMediaCollection('comment');
        }

        //   Send Notifications To Student

        if ($comment->post->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الرد على المنشور التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $comment->post->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'comment' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }

        /*
        if ($comment->post->user_id == $user->id)
        {
            foreach (Comment::where('user_id' , '!=' , $comment->post->user_id)->get() as $us)
            {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الرد على المنشور المشارك به بالتعليقات بواسطة ".$user->full_name;
            $notification_patient->user_id = $us->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'comment' ;
            $notification_patient->save();
            userNotification($notification_patient);
            }
        }
        */

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/comments/edit",
     *      operationId="edit Comment",
     *      tags={"Comments"},
     *      summary="edit Comment",
     *      description="edit Comment",
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function update(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = Comment::find($request->comment_id);
        $comment->text = $request['text'];
        $comment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $comment->addMedia($file)->toMediaCollection('comment');
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/comments/delete",
     *      operationId="delete Comment",
     *      tags={"Comments"},
     *      summary="delete Comment",
     *      description="delete Comment",
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
         Comment::find($request->comment_id)->delete();
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }


    /**
     * @SWG\Post(
     *      path="/comments/index",
     *      operationId="Show comments",
     *      tags={"Comments"},
     *      summary="Show comments",
     *      description="get comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *       @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comments = Comment::where('post_id' , $request->post_id)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), CommentsResource::collection($comments));

    }

    /**
     * @SWG\Post(
     *      path="/comments/like",
     *      operationId="like comments",
     *      tags={"Comments"},
     *      summary="like comments",
     *      description="like comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function like(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = Comment::find($request->comment_id);
        if ($comment->liked_by !== null && in_array(json_encode($user->id) , json_decode($comment->liked_by)) == 1)
        {
            return apiResponse(400, trans('like_exist' , [] , $request->header('lang')));

        }
        $comment->likes +=1;
        $liked_by = [];

        if ($comment->liked_by == null)
            $liked_by[] = json_encode($user->id);

        if ($comment->liked_by !== null)
        {
            $liked_by = json_decode($comment->liked_by);

            array_push($liked_by , json_encode($user->id));
        }

        $comment->liked_by =$liked_by ;
        $comment->save();

        /*
        //   Send Notifications To Student

        if ($comment->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الاعجاب بالتعليق التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $comment->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'comment' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }
        */
        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/comments/dislike",
     *      operationId="dislike comments",
     *      tags={"Comments"},
     *      summary="dislike comments",
     *      description="dislike comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function dislike(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $comment = Comment::find($request->comment_id);
        if (in_array(json_encode($user->id) , json_decode($comment->liked_by)) == 1)
        {
            $liked_by = [];
            for($i = 0 ; $i < count(json_decode($comment->liked_by)) ; $i++)
            {
                if (json_decode($comment->liked_by)[$i] == $user->id)
                    continue;

                if (json_decode($comment->liked_by)[$i] !== $user->id)
                {
                    $liked_by []=json_decode($comment->liked_by)[$i];
                }
            }

            $comment->liked_by = $liked_by;
            $comment->likes -=1;
            $comment->save();
            return apiResponse(200, trans('Success' , [] , $request->header('lang')));

        }

    }

    /**
     * @SWG\Post(
     *      path="/comments/report",
     *      operationId="report comments",
     *      tags={"Comments"},
     *      summary="report comments",
     *      description="report comments",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $comment = Comment::find($request->comment_id);
        $comment->report +=1;
        $comment->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }
}
