<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class ContactUsController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/create-contact_us",
     *      operationId="Create new contact us to adminstration",
     *      tags={"contact_us"},
     *      summary="Create new contact us to adminstration",
     *      description="Create new contact us from user to adminstration",
     *
     *          @SWG\Parameter(
     *          name="name",
     *          description="user name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="phone",
     *          description="user phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="message",
     *          description="user message",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:supports", "read:supports"}
     *         }
     *     },
     * )
     *
     */

    public function create(Request $request)
    {
        try {

            ContactUs::create($request->all());

        } catch (\Exception $e) {

            return apiResponse(400, trans('Error'));
        }

        return apiResponse(200, trans('Success'));
    }

}
