<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\FilesResource;
use App\Http\Resources\HomeResource;
use App\Http\Resources\LockupTablesResource;
use App\Http\Resources\NewsResource;
use App\Http\Resources\PostsResource;
use App\Http\Resources\QuestionsResource;
use App\Models\LevelDepartment;
use App\Models\News;
use App\Models\Post;
use App\Models\Question;
use App\Models\Subject;
use App\Models\SubjectFile;
use App\Models\UserLevel;
use App\Models\UserSubject;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class SubjectsController extends Controller
{
    /**
     * @SWG\Post (
     *      path="/subjects/files",
     *      operationId="get all subject files ",
     *      tags={"Subjects"},
     *      summary="get all subject files",
     *      description="get subject files ",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="filter",
     *          description="filter with videos , files , summary , questions",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="subject_id",
     *          description="subject id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function files(Request $request)
    {
        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
        else
            return apiResponse(401, trans('messages.login', [] , $request->header('lang')));

        $duplicate_subjects=$this->Duplicate_subject($request->subject_id);

        if ($request->filter == 'videos')
        {
            $files = FilesResource::collection(SubjectFile::whereIn('subject_id', $duplicate_subjects)->where('file_type', 0)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $files);
        }

        if ($request->filter == 'files')
        {
            $files  = FilesResource::collection(SubjectFile::whereIn('subject_id', $duplicate_subjects)->where('file_type' , 1)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $files);
        }

        if ($request->filter == 'summary')
        {
            $files  = FilesResource::collection(SubjectFile::whereIn('subject_id', $duplicate_subjects)->where('file_type' , 2)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $files);
        }

        if ($request->filter == 'questions')
        {
            $questions  = QuestionsResource::collection(Question::whereIn('subject_id', $duplicate_subjects)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $questions);
        }
    }


    /**
     * @SWG\Post(
     *      path="/subjects/add",
     *      operationId="store subject",
     *      tags={"Subjects"},
     *      summary="store subject",
     *      description="store subject",
     *      @SWG\Parameter(
     *          name="subject_id",
     *          description="subject_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function store(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $subs = array_map('intval', explode(',', $request['subject_id']));
        foreach ($subs as $sub)
        {
            if(!UserSubject::where('user_id',$user->id)->where('subject_id',$sub)->first()) {
                $user_subject = new UserSubject();
                $user_subject->user_id = $user->id;
                $user_subject->subject_id = $sub;
                $user_subject->save();
            }
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    public function Duplicate_subject($subject_id)
    {
        $subject_search=Subject::findOrFail($subject_id);
        //$departments = LevelDepartment::where('id','<>',$subject_search->department_id)->get();
        $departments = LevelDepartment::all();
        $subjectss= [];
        foreach ($departments as $dep)
        {
            $subjects=Subject::where('department_id' , $dep->id)->get();
            foreach ($subjects as $subject){
                if($subject_search->name_en==$subject->name_en){
                   array_push($subjectss,$subject->id);
                }
            }
        }
        return $subjectss;
    }
}
