<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Requests\Api\Auth\ChangePasswordRequest;
use App\Http\Requests\Api\Auth\CheckEmailRequest;
use App\Http\Requests\Api\Auth\CreateUserRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginUserRequest;
use App\Http\Resources\UserResource;
use App\Mail\ResetPassword;
use App\Models\LevelDepartment;
use App\Models\Trial;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\UserLevel;
use App\Models\UserPromoCode;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('api.authenticatable', ['only' => ['logout']]);
    }

    /**
     * @SWG\Post(
     *      path="/auth/update-firebase-token",
     *      operationId="update firebase_token for user",
     *      tags={"Authenticate"},
     *      summary="update firebase_token",
     *      description="update firebase_token",
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *          ),
     *          @SWG\Parameter(
     *          name="firebase_token",
     *          description="firebase_token",
     *          required=true,
     *          type="string",
     *          in="formData"
     *          ),
     *          @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *          ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function updateFirebase(Request $request)
    {
        $user = \JWTAuth::parseToken()->authenticate();

        $user->update(['firebase_token' => $request->firebase_token]);

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));
    }

    /**
     * @SWG\Post(
     *      path="/auth/register",
     *      operationId="user regestration",
     *      tags={"Authenticate"},
     *      summary="user regestration",
     *      description="Returns user Data",
     *     @SWG\Parameter(
     *          name="device_id",
     *          description="device_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="verified",
     *          description="1 if google or facebook , 0 otherwise",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="full_name",
     *          description="Full name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="user_name",
     *          description="User name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *        @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="phone",
     *          description="User phone",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="repeat_password",
     *          description="Repeat password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="type",
     *          description="0=>normal student , 1=>excellent student , 2=>teaching assistant , 3=>doctor",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="university_id",
     *          description="university id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="faculty_id",
     *          description="faculty id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="department_id",
     *          description="department id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="level",
     *          description="level",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="gender",
     *          description="male or female",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="image",
     *          description="user image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="staff_img",
     *          description="staff image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=411, description="Email Exist"),
     *      @SWG\Response(response=412, description="Phone Exist"),
     *      @SWG\Response(response=413, description="Confirm Password"),
     *      @SWG\Response(response=414, description="Username Exist"),

     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function register(CreateUserRequest $request)
    {
            $data = $request->only('user_name','full_name', 'email', 'phone','type' , 'university_id' , 'department_id' , 'faculty_id' , 'gender');

            $data['password'] = Hash::make($request['password']);

            if ($request['email'] && User::where('email', $request['email'])->first())
                return apiResponse(411, trans('messages.email_exist' , [] , $request->header('lang')));

            if ($request['phone'] && User::where('phone', $request['phone'])->orWhere('phone','+'.$request['phone'])->first())
                return apiResponse(412, trans('messages.phone_exist' , [] , $request->header('lang')));

            if ($request->password !== $request->repeat_password)
                return apiResponse(413, trans('confirm_password' , [] , $request->header('lang')));

            if($request['user_name']&&User::where('user_name', $request['user_name'])->first())
                return apiResponse(414, trans('messages.username_exist' , [] , $request->header('lang')));

            $user = User::create($data);
            $user->save();

            $promo_code=new UserPromoCode();
            $promo_code->user_id=$user->id;
            do{
                $code=generateRandomCode(10);
            }
            while(UserPromoCode::where('promo_code',$code)->first());
            $promo_code->promo_code=$code;
            $promo_code->status=0;
            $promo_code->save();

            $balance=new UserBalance();
            $balance->user_id=$user->id;
            $balance->save();

            if($user->gender=='ذكر'){
                $user->gender='Male';
                $user->save();
            }
            if($user->gender=='انثى'){
                $user->gender='Female';
                $user->save();
            }

            if($request['verified']){
                $user->email_verified_at =  \Carbon\Carbon::now() ;
                $user->save();
            }

            if (($request->type == 1 || $request->type == 2) && $request->file('staff_img'))
            {
                $file = $request->file('staff_img');
                $user->addMedia($file)->toMediaCollection('staffImg');
            }

            if ($request->file('image'))
            {
                $file = $request->file('image');
                $user->addMedia($file)->toMediaCollection('user');
            }

            if (strlen($request->level) == 1)
            {
                $level = LevelDepartment::where('department_id' , $request->department_id)->where('level' , $request->level)->first();
                UserLevel::create([
                    'user_id' => $user->id,
                    'level_id' => $level->id,
                ]);
            }

            if (strlen($request->level) > 1)
            {
                $levels = array_map('intval', explode(',', $request['level']));
                foreach ($levels as $levl)
                {
                    $level = LevelDepartment::where('department_id' , $request->department_id)->where('level' , $levl)->first();
                    UserLevel::create([
                        'user_id' => $user->id,
                        'level_id' => $level->id,
                    ]);
                }
            }

        return apiResponse(200, trans('Success' , [] , $request->header('lang')), new UserResource($user));

    }

    /**
     * @SWG\Post(
     *      path="/auth/login",
     *      operationId="user Login",
     *      tags={"Authenticate"},
     *      summary="user Login",
     *      description="Returns user Data",
     *      @SWG\Parameter(
     *          name="data",
     *          description=" Mobile Or Email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="device_id",
     *          description="Device id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function login(LoginUserRequest $request)
    {
        $user = User::where('phone', $request->data)->orWhere('email', $request->data)->first();
        if($user) {
            if (!$user->email_verified_at) {
                return apiResponse(401, trans('messages.activation_required', [], $request->header('lang')));
            }
            if($user->deadline==null) {
                if (!User::where('device_id', $request['device_id'])->where('id', '<>', $user->id)->first()) {
                    $deadline = "2022-01-31";
                    $user->deadline = $deadline;
                    $user->status = 1;
                    $user->save();
                    /*
                    $days = Trial::find(1)->days;
                    if ($days != 0) {
                        $user->deadline = Carbon::now()->addDay($days - 1)->format('Y-m-d');
                        $user->status = 1;
                        $user->save();
                    }
                    */
                } else {
                    $user->deadline = Carbon::now()->addDay(-1)->format('Y-m-d');
                    $user->status = 0;
                    $user->save();
                }
            }

            if ($user->device_id == null && $request['device_id'] !== null) {
                $user->device_id = $request['device_id'];
                $user->save();
            }

            if ($user->device_id != $request['device_id']) {
                return apiResponse(402, trans('messages.device_error', [], $request->header('lang')));
            }

            if (Hash::check($request->password, $user->password)) {
                $token = \JWTAuth::fromUser($user);
                return apiResponse(200, trans('messages.success', [], $request->header('lang')), ['token' => $token, 'user' => new UserResource($user)]);
            } else {

                return apiResponse(400, trans('messages.invalid_credentials', [], $request->header('lang')));
            }
        }
        else{
            return apiResponse(400, trans('Phone or Email not found', [], $request->header('lang')));
        }
    }

    /**
     * @SWG\Post(
     *      path="/auth/skip-login",
     *      operationId="skip login , create new user",
     *      tags={"Authenticate"},
     *      summary="skip login , create new user",
     *      description="skip login , create new user",
     *          @SWG\Parameter(
     *          name="device_id",
     *          description="device_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function skipLogin(Request $request)
    {
        $this->validate($request, [
            'device_id' => 'required|string|max:150',
        ]);

        try {
            DB::beginTransaction();

            $user = User::where('device_id', $request['device_id'])->where('phone', null)->first();

            if ($user)
                $token = \JWTAuth::fromUser($user);

            else {
                $user = User::create([
                    'device_id' => $request['device_id'],
                ]);


                $token = \JWTAuth::fromUser($user);
            }

            DB::commit();
        } catch (JWTException $e) {

            DB::rollBack();
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')), [ 'token' => $token]);
    }

    /**
     * @SWG\Post(
     *      path="/auth/logout",
     *      operationId="user logout",
     *      tags={"Authenticate"},
     *      summary="user logout",
     *      description="Returns user logout",
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function logout(Request $request)
    {
        if (\JWTAuth::parseToken()->authenticate()) {

            auth('api')->logout();

            return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));
        }

        return apiResponse(400, trans('messages.error' , [] , $request->header('lang')));
    }

    /**
     * @SWG\Post(
     *      path="/auth/forget-password",
     *      operationId="user forget-password",
     *      tags={"Authenticate"},
     *      summary="user forget-password",
     *      description="Returns Code",
     *      @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('phone', $request->data)->orWhere('email', $request->data)->first();

        $admin_email = "info@sa3deni.com";

        if ($user && $admin_email) {

            $code = generateRandomCode(6);
            $email = $user->email;

            $user->code = $code;

            $user->save();
            $data=[
                'name'=>$user->full_name,
                'code'=>$code,
            ];
            @Mail::to($email)->send(new ResetPassword($data));
            /*
            @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                $message->to($email)
                    ->subject('Sa3dni Reset Password')
                    ->setBody("Reset Password Code : " . $code."\n مع تحيات فريق عمل ساعدني ");
                $message->from($admin_email);
            });
            */
            return apiResponse(200, trans('messages.success' , [] , $request->header('lang')), $code);

        } else {
            return apiResponse(400, trans('messages.error_data' , [] , $request->header('lang')));
        }


    }

    /**
     * @SWG\Post(
     *      path="/auth/change-password",
     *      operationId="user change-password",
     *      tags={"Authenticate"},
     *      summary="user change-password",
     *      description="Returns New Password",
     *      @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="code",
     *          description="Verification code",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="new_password",
     *          description="New password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="confirm_password",
     *          description="Confirm password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function changePassword(ChangePasswordRequest $request)
    {

        $user = User::where('phone', $request->data)->orWhere('email', $request->data)->first();

        $token = \JWTAuth::fromUser($user);

        if ($user) {
            if ($request->code !== $user->code) {
                return apiResponse(400, trans('error_code' , [] , $request->header('lang')));
            }

            if ($request->new_password !== $request->confirm_password) {
                return apiResponse(400, trans('confirm_password' , [] , $request->header('lang')));
            }


            $new_password = $request->new_password;

            $admin_email = "info@sa3deni.com";

            $user->password = Hash::make($request['new_password']);

            $user->save();

            if ($admin_email) {

                $email = $user->email;
                    /*
                    @Mail::send([], [], function ($message) use ($email, $admin_email, $new_password) {
                        $message->to($email)
                            ->subject('Sa3deni')
                            ->setBody("New Password Is : " . $new_password);
                        $message->from($admin_email);
                    });
                    */

                return apiResponse(200, trans('messages.success' , [] , $request->header('lang')), ['token' => $token]);

            } else {

                return apiResponse(400, trans('messages.error_data' , [] , $request->header('lang')));
            }

        } else {

            return apiResponse(400, trans('messages.error_data' , [] , $request->header('lang')));
        }
    }

    /**
     * @SWG\Post(
     *      path="/auth/check-login",
     *      operationId="check Login",
     *      tags={"Authenticate"},
     *      summary="check email or phone",
     *      description="Returns success,token and user Data if the enterd email was found in the database otherwise  return not found ",
     *      @SWG\Parameter(
     *          name="data",
     *          description="Email Or Phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="device_id",
     *          description="Device ID",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function checklogin(CheckEmailRequest $request)
    {

        if($user = User::where('phone', '+'.$request->data)->orWhere('phone',$request->data)->orWhere('email', $request->data)->first())
        {
            if (!$user->email_verified_at) {
                return apiResponse(401, trans('messages.activation_required' , [] , $request->header('lang')));
            }

            if ($user->device_id == null && $request['device_id'] !== null)
            {
                $user->device_id = $request['device_id'];
                $user->save();
            }
            if($user &&$user->device_id!=$request['device_id']){
                return apiResponse(402, trans('messages.device_error' , [] , $request->header('lang')));
            }
            $token = \JWTAuth::fromUser($user);
            return apiResponse(200, trans('messages.success', [], $request->header('lang')), ['token' => $token, 'user' => new UserResource($user)]);
        }
        else {

            return apiResponse(400, trans('Phone or Email not found', [], $request->header('lang')));
        }


    }

    /**
     * @SWG\Post(
     *      path="/auth/check-phone",
     *      operationId="check Phone",
     *      tags={"Authenticate"},
     *      summary="check phone",
     *      description="Returns success if the phone was not found in the database otherwise  return 412 ",
     *      @SWG\Parameter(
     *          name="phone",
     *          description="Phone",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=412, description="Phone Exist"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function checkphone(Request $request)
    {
        if ($request['phone'] && User::where('phone', '+'.$request['phone'])->orWhere('phone',$request['phone'])->first())
            return apiResponse(412, trans('messages.phone_exist' , [] , $request->header('lang')));

        return apiResponse(200, trans('messages.success', [], $request->header('lang')));
    }

    /**
     * @SWG\Post(
     *      path="/auth/check-username",
     *      operationId="check Username",
     *      tags={"Authenticate"},
     *      summary="check username",
     *      description="Returns success if the phone was not found in the database otherwise  return 414 ",
     *      @SWG\Parameter(
     *          name="username",
     *          description="Username",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=414, description="Username Exist"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function checkusername(Request $request)
    {
        if ($request['username'] && User::where('user_name', $request['username'])->first())
            return apiResponse(414, trans('messages.username_exist' , [] , $request->header('lang')));

        return apiResponse(200, trans('messages.success', [], $request->header('lang')));
    }
    /**
     * @SWG\Post(
     *      path="/auth/get-names",
     *      operationId="get user names",
     *      tags={"Authenticate"},
     *      summary="get user names",
     *      description="Returns Names",
     *      @SWG\Parameter(
     *          name="name",
     *          description="user name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function getNames(Request $request)
    {
            $data =
                [
                  'name_one' => $request['name'].generateRandomCode(4),
                  'name_two' => $request['name'].generateRandomCode(4),
                  'name_three' => $request['name'].generateRandomCode(4),
                ];
            return apiResponse(200, trans('messages.success' , [] , $request->header('lang')), $data);
    }
    /**
     * @SWG\Post(
     *      path="/auth/promo-code",
     *      operationId="Verify Promo Code",
     *      tags={"Authenticate"},
     *      summary="Verify Promo Code",
     *      description="Verify Promo Code",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="promo_code",
     *          description="Promo Code",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=402, description="Promo Code Error"),
     *
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function verify_promocode(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $user_promo=UserPromoCode::where('promo_code',$request->promo_code)->first();
        if(!$user_promo){
            return apiResponse(402, trans('messages.invalid_code' , [] , $request->header('lang')));
        }
        if(!$user_promo->status){
            return apiResponse(402, trans('messages.coupon_invalid' , [] , $request->header('lang')));
        }
        $user_promo->count++;
        $user_promo->save();
        $balance=UserBalance::where('user_id',$user_promo->user_id)->first();
        $balance->cache+=2.5;
        $balance->service+=2.5;
        $balance->save();
        $my_promo=UserPromoCode::where('user_id',$user->id)->first();
        $my_promo->status=1;
        $my_promo->save();
        return apiResponse(200, trans('messages.success', [], $request->header('lang')));
    }

}
