<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Requests\Api\Auth\ChangePasswordRequest;
use App\Http\Requests\Api\Auth\checkEmailRequest;
use App\Http\Requests\Api\Auth\CreateUserRequest;
use App\Http\Requests\Api\Auth\ForgetPasswordRequest;
use App\Http\Requests\Api\Auth\LoginUserRequest;
use App\Http\Resources\LockupTablesResource;
use App\Http\Resources\NewsResource;
use App\Http\Resources\RankResource;
use App\Http\Resources\UserResource;
use App\Models\Comment;
use App\Models\LevelDepartment;
use App\Models\News;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserLevel;
use App\Traits\ApiResponser;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class NewsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/AddNews",
     *      operationId="Add News",
     *      tags={"News"},
     *      summary="Add New News",
     *      description="The user add news and admin approve that this news is correct so the news is added ",
     *      @SWG\Parameter(
     *          name="NewsText",
     *          description="NewsText",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function AddNews(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $news = new News();
        $news->newsText = $request['NewsText'];
        $news->user_id = $user->id;
        $news->approved = $user->type == 1 ? 1 : 0;
        $news->save();

        //   Send Notifications To Students

        if ($user->type == 1) {
            $levels = UserLevel::where('user_id', $user->id)->pluck('level_id');
            $userss = UserLevel::whereIn('level_id', $levels)->where('user_id', '!=', $user->id)->pluck('user_id');
            $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
            where('department_id', $user->department_id)->get();

        foreach ($users as $usr)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع خبر جديد بواسطة ".$user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'news' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }
        }
//        foreach ($levels as $level)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع خبر جديد بواسطة ".$user->full_name;
//            $notification_patient->sender_id = $user->id ;
//            $notification_patient->action_type = 'news' ;
//            $notification_patient->level_id = $level ;
//            $notification_patient->save();
//
//        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/AddNews-multi",
     *      operationId="Add News to Level_Department",
     *      tags={"News"},
     *      summary="Add News to Level_Department",
     *      description="Add News to Level_Department ",
     *      @SWG\Parameter(
     *          name="NewsText",
     *          description="NewsText",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="department",
     *          description="Departmnet ID",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="level",
     *          description="Level",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function AddNews_multi(Request $request)
    {
        $level_department=LevelDepartment::where('department_id',$request['department'])->where('level',$request['level'])->first();
        $user =\JWTAuth::parseToken()->authenticate();
        $news = new News();
        $news->newsText = $request['NewsText'];
        $news->user_id = $user->id;
        $news->approved = $user->type == 1 ? 1 : 0;
        $news->level_department_id=$level_department->id;
        $news->save();

        //   Send Notifications To Students

        if ($user->type == 1) {
            //$levels = UserLevel::where('user_id', $user->id)->pluck('level_id');
            $userss = UserLevel::where('level_id', $level_department->id)->where('user_id', '!=', $user->id)->pluck('user_id');
            $users = User::whereIn('id', $userss)->get();

            foreach ($users as $usr)
            {
                $notification_patient = new Notification();
                $notification_patient->title = "تم رفع خبر جديد بواسطة ".$user->full_name;
                $notification_patient->user_id = $usr->id;
                $notification_patient->sender_id = $user->id ;
                $notification_patient->action_type = 'news' ;
                $notification_patient->save();
                userNotification($notification_patient);

            }
        }
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }
    /**
     * @SWG\Post(
     *      path="/ShowNews",
     *      operationId="Show News",
     *      tags={"News"},
     *      summary="Show Saved News",
     *      description="The user See the saved news",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function ShowNews(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $level=LevelDepartment::findOrFail(UserLevel::where('user_id',$user->id)->first()->level_id)->level;
        $level_department=LevelDepartment::where(['level'=>$level,'department_id'=>$user->department_id])->first();
        $users_same_level=UserLevel::where('level_id',$level_department->id)->pluck('user_id');
        $news  = News::whereIn('user_id',$users_same_level)->where('approved' , 1)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), NewsResource::collection($news));

    }

    /**
     * @SWG\Post(
     *      path="/news/admin-news",
     *      operationId="Show admin News",
     *      tags={"News"},
     *      summary="Show admin News",
     *      description="The user See the admin news",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function adminNews(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $news  = News::whereNull('user_id')->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), NewsResource::collection($news));

    }

    /**
     * @SWG\Post(
     *      path="/news/report",
     *      operationId="report news",
     *      tags={"News"},
     *      summary="report news",
     *      description="report news",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="new_id",
     *          description="new id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $new = News::find($request->new_id);
        $new->report +=1;
        $new->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/ShowRanks",
     *      operationId="Show Ranks",
     *      tags={"Rank"},
     *      summary="Show Students Ranks",
     *      description="The user See the Students Ranks",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function ShowRanks(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $user_levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');

        $users = UserLevel::whereIn('level_id' , $user_levels)->pluck('user_id');

        $ranked_users = User::whereIn('id' , $users)->orderBy('rank' , 'desc')->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), RankResource::collection($ranked_users));
    }
}
