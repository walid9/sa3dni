<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentsResource;
use App\Http\Resources\PostsResource;
use App\Http\Resources\QCommentsResource;
use App\Models\Notification;
use App\Models\QComment;
use App\Models\Question;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class QCommentsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/create",
     *      operationId="Create Question Comment",
     *      tags={"QuestionComments"},
     *      summary="Create Question Comment",
     *      description="Create Question Comment",
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="question id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Comment Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function create(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $Qcomment = new QComment();
        $Qcomment->question_id = $request['question_id'];
        $Qcomment->text = $request['text'];
        $Qcomment->user_id = $user->id;
        $Qcomment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $Qcomment->addMedia($file)->toMediaCollection('answer');
        }

        //   Send Notifications To Student

        if ($Qcomment->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الرد على السؤال التابع لك ";
            $notification_patient->user_id = $Qcomment->question->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'answer' ;
            $notification_patient->save();
            userNotification($notification_patient);
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/edit",
     *      operationId="edit Question Comment",
     *      tags={"QuestionComments"},
     *      summary="edit Question Comment",
     *      description="edit Question Comment",
     *      @SWG\Parameter(
     *          name="qcomment_id",
     *          description="qcomment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Comment Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function update(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $Qcomment = QComment::find($request->qcomment_id);
        $Qcomment->text = $request['text'];
        $Qcomment->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $Qcomment->addMedia($file)->toMediaCollection('answer');
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/delete",
     *      operationId="delete Question Comment",
     *      tags={"QuestionComments"},
     *      summary="delete Question Comment",
     *      description="delete Question Comment",
     *      @SWG\Parameter(
     *          name="qcomment_id",
     *          description="qcomment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        QComment::find($request->qcomment_id)->delete();

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }


    /**
     * @SWG\Post(
     *      path="/Qcomments/index",
     *      operationId="Show Question comments",
     *      tags={"QuestionComments"},
     *      summary="Show Question comments",
     *      description="get questions comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *       @SWG\Parameter(
     *          name="question_id",
     *          description="question id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $Qcomments = QComment::where('question_id' , $request->question_id)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), QCommentsResource::collection($Qcomments));

    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/like",
     *      operationId="like Question comments",
     *      tags={"QuestionComments"},
     *      summary="like question comments",
     *      description="like question comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function like(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $Qcomment = QComment::find($request->comment_id);
        if ($Qcomment->liked_by !== null && in_array(json_encode($user->id) , json_decode($Qcomment->liked_by)) == 1)
        {
            return apiResponse(400, trans('like_exist' , [] , $request->header('lang')));

        }
        $Qcomment->likes +=1;
        $liked_by = [];

        if ($Qcomment->liked_by == null)
            $liked_by[] = json_encode($user->id);

        if ($Qcomment->liked_by !== null)
        {
            $liked_by = json_decode($Qcomment->liked_by);

            array_push($liked_by , json_encode($user->id));
        }

        $Qcomment->liked_by =$liked_by ;
        $Qcomment->save();

        /*
        //   Send Notifications To Student

        if ($Qcomment->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الاعجاب بالاجابة التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $Qcomment->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'answer' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }
        */
        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/dislike",
     *      operationId="dislike Question comments",
     *      tags={"QuestionComments"},
     *      summary="dislike question comments",
     *      description="dislike question comments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function dislike(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $Qcomment = QComment::find($request->comment_id);
        if (in_array(json_encode($user->id), json_decode($Qcomment->liked_by)) == 1) {
            $liked_by = [];
            for ($i = 0; $i < count(json_decode($Qcomment->liked_by)); $i++) {
                if (json_decode($Qcomment->liked_by)[$i] == $user->id)
                    continue;

                if (json_decode($Qcomment->liked_by)[$i] !== $user->id) {
                    $liked_by [] = json_decode($Qcomment->liked_by)[$i];
                }
            }

            $Qcomment->liked_by = $liked_by;
            $Qcomment->likes -= 1;
            $Qcomment->save();
            return apiResponse(200, trans('Success', [], $request->header('lang')));

        }
    }

    /**
     * @SWG\Post(
     *      path="/Qcomments/report",
     *      operationId="report Question comments",
     *      tags={"Comments"},
     *      summary="report question comments",
     *      description="report question comments",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          description="comment id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $Qcomment = QComment::find($request->comment_id);
        $Qcomment->report +=1;
        $Qcomment->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }
}
