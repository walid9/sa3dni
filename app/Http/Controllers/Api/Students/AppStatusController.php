<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ManagementController;
use App\Http\Resources\FacultyPricesResource;
use App\Http\Resources\LockupTablesResource;
use App\Http\Resources\SubjectResource;
use App\Models\AppImage;
use App\Models\Department;
use App\Models\Faculty;
use App\Models\LevelDepartment;
use App\Models\Managament;
use App\Models\PaymentPlace;
use App\Models\Subject;
use App\Models\University;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\FacultyPrice;
use App\Models\UserPromoCode;
use Illuminate\Http\Request;

class AppStatusController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/app-status/payment-places",
     *      operationId="get payment places",
     *      tags={"Look-ups"},
     *      summary="get payment places",
     *      description="get payment places",
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function places()
    {
        $places = PaymentPlace::get(['id' , 'name' , 'status']);
        return apiResponse(200, 'Success', $places);
    }

    /**
     * @SWG\Post (
     *      path="/app-status/faculty-cost",
     *      operationId="get faculty cost",
     *      tags={"Look-ups"},
     *      summary="get faculty cost",
     *      description="get faculty cost",
     *    @SWG\Parameter(
     *          name="faculty_id",
     *          description="faculty id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function facultyCost(Request $request)
    {
        $cost = FacultyPrice::where('faculty_id' , $request->faculty_id)->get();
        return apiResponse(200, 'Success', FacultyPricesResource::collection($cost));
    }

    /**
     * @SWG\Get(
     *      path="/app-status/universities",
     *      operationId="get universities",
     *      tags={"Look-ups"},
     *      summary="get universities",
     *      description="get universities",
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function universities()
    {
        $services = University::all();
        return apiResponse(200, 'Success', LockupTablesResource::collection($services)->prepend(['id'=>0,'name'=>'Choose University ..']));
    }

    /**
     * @SWG\Post(
     *      path="/app-status/faculties",
     *      operationId="get faculties",
     *      tags={"Look-ups"},
     *      summary="get faculties",
     *      description="get faculties",
     *     @SWG\Parameter(
     *          name="university_id",
     *          description="university id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function faculties(Request $request)
    {
        $services = Faculty::where('university_id' , $request->university_id)->get();
        return apiResponse(200, 'Success', LockupTablesResource::collection($services)->prepend(['id'=>0,'name'=>'Choose Faculty ..']));
    }

    /**
     * @SWG\Post(
     *      path="/app-status/departments",
     *      operationId="get departments",
     *      tags={"Look-ups"},
     *      summary="get departments",
     *      description="get departments",
     *     @SWG\Parameter(
     *          name="faculty_id",
     *          description="faculty id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function departments(Request $request)
    {
        //$deps = LevelDepartment::whereIn('department_id' , Department::where('faculty_id' , $request->faculty_id)->pluck('id'))->where('level' , $request->level)->pluck('department_id');
        $services = Department::where('faculty_id' , $request->faculty_id)->get();
        return apiResponse(200, 'Success', LockupTablesResource::collection($services)->prepend(['id'=>0,'name'=>'Choose Department ..']));
    }

    /**
     * @SWG\Post(
     *      path="/app-status/levels",
     *      operationId="get levels",
     *      tags={"Look-ups"},
     *      summary="get levels",
     *      description="get levels",
     *     @SWG\Parameter(
     *          name="department_id",
     *          description="department id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function levels(Request $request)
    {
        $levels=LevelDepartment::where('department_id',$request->department_id)->get(['id' , 'level']);
        return apiResponse(200, 'Success', $levels->prepend(['id'=>0,'level'=>'Choose Level ..']));
    }
    /**
     * @SWG\Post(
     *      path="/app-status/subjects",
     *      operationId="get subjects",
     *      tags={"Look-ups"},
     *      summary="get subjects",
     *      description="get subjects",
     *     @SWG\Parameter(
     *          name="department_id",
     *          description="department id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="level",
     *          description="level",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function subjects(Request $request)
    {
        $level = LevelDepartment::where('department_id' , $request->department_id)->where('level' , $request->level)->first();
        $subjects = Subject::where('department_id' , $level->id)->get();
        return apiResponse(200, 'Success', LockupTablesResource::collection($subjects));
    }
    /**
 * @SWG\Post(
 *      path="/app-status/promo-code",
 *      operationId="Check Promo Code Management",
 *      tags={"Look-ups"},
 *      summary="Check Promo Code Management",
 *      description="Check Promo Code Management",
 *       @SWG\Parameter(
 *          name="authorization",
 *          description="authorization token",
 *          required=true,
 *          type="string",
 *          in="header"
 *      ),
 *    @SWG\Parameter(
 *          name="lang",
 *          description="lang",
 *          required=true,
 *          type="string",
 *          in="header"
 *      ),
 *      @SWG\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @SWG\Response(response=400, description="Bad request"),
 *      @SWG\Response(response=404, description="Resource Not Found"),
 *      security={
 *         {
 *             "oauth2_security_example": {"write:offer", "read:offer"}
 *         }
 *     },
 * )
 *
 */

    public function check_promocode(Request $request)
    {
        $check_promo=Managament::where('title','promo_code')->first();
        $user =\JWTAuth::parseToken()->authenticate();
        $check_device=User::where('id','<>',$user->id)->where('device_id' , $user->device_id)->first();
        $user_promo=UserPromoCode::where('user_id',$user->id)->first();
        if($check_promo->status && $user->status==1 && !$check_device && !$user_promo->status){
            return apiResponse(200, 'Success');
        }
    }
    /**
     * @SWG\Post(
     *      path="/app-status/faculty-subjects",
     *      operationId="get my faculty subjects",
     *      tags={"Look-ups"},
     *      summary="get my faculty subjects",
     *      description="get my faculty subjects",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function mySubjects(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $deps = Department::where('faculty_id' , $user->faculty_id)->pluck('id');
        $my_deps = UserLevel::where('user_id' , $user->id)->pluck('level_id');
        //dd($my_deps);
        $levels = LevelDepartment::whereNotIn('id' , $my_deps)->whereIn('department_id' , $deps)->pluck('id');
        //dd($levels);
        $subjects = Subject::whereIn('department_id' , $levels)->with('department.department')->get();
        //dd($subjects);
        return apiResponse(200, 'Success', SubjectResource::collection($subjects));
    }

    /**
     * @SWG\Post(
     *      path="/app-status/shared-subjects",
     *      operationId="get shared subjects in departments",
     *      tags={"Look-ups"},
     *      summary="get shared subjects in departments",
     *      description="get shared subjects in departments",
     *     @SWG\Parameter(
     *          name="department_id",
     *          description="department id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function sharedSubjects(Request $request)
    {
        $deps = array_map('intval', explode(',', $request['department_id']));
        $departments = LevelDepartment::whereIn('department_id' , $deps)->get();

        $subjects_one = [];

        foreach ($departments as $dep)
        {
            /*
            if (Subject::where('department_id' , $dep->id)->first())
            {
                $subjects_one[] = Subject::where('department_id' , $dep->id)->first()->name_en;

            }*/
            $subjects=Subject::where('department_id' , $dep->id)->get();
            foreach ($subjects as $subject){
                $subjects_one[] = $subject->name_en;
            }
        }

        $dupes = $this->findDuplicates($subjects_one);

        $subjects = Subject::whereIn('name_en' , $dupes)->pluck('name_en')->toArray();
        $result = [];

        $new_subjects =  array_unique($subjects);

        foreach ($new_subjects as $sub)
        {
            $subject=Subject::where('name_en' , $sub)->first();
            if ($subject&&(!(in_array($subject,$result)))) {
                $result[] = $subject;
            }
        }

        return apiResponse(200, 'Success', LockupTablesResource::collection($result));

    }

    function findDuplicates($array1)
    {
        //$combined = array_merge($array1,$array2);
        $counted = array_count_values($array1);
        $dupes = [];
        $keys = array_keys($counted);
        foreach ($keys as $key)
        {
            if ($counted[$key] > 1)
            {$dupes[] = $key;}
        }
        sort($dupes);
        return $dupes;
    }

    /**
     * @SWG\Get(
     *      path="/app-status/subjects-images",
     *      operationId="get subjects Images",
     *      tags={"Look-ups"},
     *      summary="get subjects Images",
     *      description="get subjects Images",
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function subjects_images()
    {
        $images = AppImage::where('type','subject')->get();
        return apiResponse(200, 'Success', $images);
    }
}
