<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\AddResources;
use App\Models\Ad;
use App\Models\AdLevel;
use App\Models\UserLevel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/allAds",
     *      operationId="get All Advertisments",
     *      tags={"Ads"},
     *      summary="get All Advertisments",
     *      description="Returns  All Advertisments",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:Ads", "read:Ads"}
     *         }
     *     },
     * )
     *
     */

    public function allAds()
    {
        $user =\JWTAuth::parseToken()->authenticate();

        $user_faculty = $user->faculty_id;
        $user_levels = UserLevel::where('user_id' , $user->id)->pluck('level_id')->toArray();

        $ads=[];
        foreach (Ad::all() as $ad)
        {
            $add=$ad->end_date;
            if($ad->faculty_id == $user_faculty && array_intersect($user_levels , AdLevel::where('ad_id' , $ad->id)->pluck('level_department_id')->toArray()) && $add >= carbon::now()->format('Y-m-d') && $ad->active == 1) {
                $ads = $ad;
            }
        }

        if (!$ads)
            return apiResponse(200, trans('Success'),  $ads , true);

        $ads=[];
        foreach (Ad::all() as $ad)
        {
            $add=$ad->end_date;
            if($ad->faculty_id == $user_faculty && array_intersect($user_levels , AdLevel::where('ad_id' , $ad->id)->pluck('level_department_id')->toArray()) && $add >= carbon::now()->format('Y-m-d') && $ad->active == 1)
                $ads[] = $ad;
        }

        $adss=[];
        for ($i=0 ; $i < count($ads) ; $i++) {
                $adss[$i] = [
                    'id' => $ads[$i]['id'],
                    'start_date' => $ads[$i]['start_date'],
                    'end_date' => $ads[$i]['end_date'],
                    'type' => $ads[$i]['type'],
                    'link' => $ads[$i]['link'],
                    'created_at' => $ads[$i]['created_at']->format('Y-m-d'),
                    'image' => $ads[$i]['img']['localUrl'] ?? 'Null',
                ];
            }

        return apiResponse(200, trans('Success'), $adss , true , 10);


    }
}
