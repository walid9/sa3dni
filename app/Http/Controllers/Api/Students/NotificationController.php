<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserLevel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;

class NotificationController extends Controller
{

    /**
     * @SWG\Get(
     *      path="/Notifications",
     *      operationId="get notifications for user ",
     *      tags={"notifications"},
     *      summary="get notifications for user",
     *      description="get notifications for user ",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {

        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
        else
            return apiResponse(401, 'تسجيل الدخول مطلوب من اجل هذه العمليه');

        $yesterday=date('Y-m-d',strtotime("-1 days"));
        $notifications = Notification::where('user_id' , $user->id)->orWhereIn('level_id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->orderBy('id' , 'desc')->where('created_at','>=',$yesterday.' 00:00:00')->get();

//        foreach ($notifications as $notification)
//        {
//            $notification->update(['read' => 1]);
//        }

        return apiResponse(200, trans('messages.success'), NotificationResource::collection($notifications) , true,100);
    }

    /**
     * @SWG\Post(
     *      path="/updateNotification",
     *      operationId="mark notification as read ",
     *      tags={"notifications"},
     *      summary="mark notification of user as read",
     *      description="mark notification of user as read ",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=false,
     *          type="string",
     *          in="header"
     *      ),
     *          @SWG\Parameter(
     *          name="notification_id",
     *          description="notification Id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function updateNotification(Request $request)
    {

        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
        else
            return apiResponse(401, trans('messages.login', [] , $request->header('lang')));

        $notification = Notification::where('id', $request->notification_id)->first();

        $notification->update(['read' => 1]);
        return apiResponse(200, trans('messages.success', [] , $request->header('lang')));

    }
}
