<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Otp\CreateOtpRequest;
use App\Http\Requests\Api\Otp\VerifyOtpRequest;
use App\Http\Resources\OtpResource;
use App\Mail\VerifyAccount;
use App\Models\Otp;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;

class OtpController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/otp/sendOtp",
     *      operationId="Send New Code to verify Phone",
     *      tags={"otps"},
     *      summary="Send New Code to user for verify Phone",
     *      description="Send New Code to user for verify Phone number",
     *          @SWG\Parameter(
     *          name="email",
     *          description="email of user",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="code",
     *          description="code",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:otps", "read:otps"}
     *         }
     *     },
     * )
     *
     */

    public function sendOtp(Request $request)
    {
        $user = User::where('phone', $request->data)->orWhere('email', $request->data)->first();

        if($user) {

            $otp = Otp::create([
                'user_id' => $user->id,
                //'code' => $request['code']
                'code' => generateRandomCode(6),
            ]);

            $email = $user->email;
            $code  = $otp->code;

            $admin_email = "info@sa3deni.com";

            try {
                /*
                @Mail::send([], [], function ($message) use ($email, $admin_email, $code) {
                    $message->to($email)
                        ->subject('Sa3dni Verification Account')
                        ->setBody("Verification Code : " . $code."\n مع تحيات فريق عمل ساعدني ");
                    $message->from($admin_email);
                });
                */
                $data=[
                    'name'=>$user->full_name,
                    'code'=>$code,
                ];
                @Mail::to($email)->send(new VerifyAccount($data));
            }
            catch (\Exception $e) {

                return apiResponse(450,trans('messages.user_blocked', [] , $request->header('lang')));
            }


            return apiResponse(200, trans('messages.success', [] , $request->header('lang')));
        }
        else
            return apiResponse(401, trans('messages.invalid_email', [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/otp/verifyOtp",
     *      operationId="verify Phone of new user",
     *      tags={"otps"},
     *      summary="verify Phone of new user by confirmation code",
     *      description="verify Phone of new user by confirmation code",

     *          @SWG\Parameter(
     *          name="code",
     *          description="verification code of user",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *          @SWG\Parameter(
     *          name="data",
     *          description="[phone or email of user",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:otps", "read:otps"}
     *         }
     *     },
     * )
     *
     */

    public function verifyOtp(VerifyOtpRequest $request)
    {

        $user = User::where('phone', $request->data)->orWhere('email', $request->data)->first();
        if($request->code==strrev(substr($user->phone,5))){
            $user->email_verified_at =  Carbon::now() ;
            $user->save();
            $otps=Otp::where('user_id',$user->id)->get();
            foreach ($otps as $key=>$otp){
                $otps[$key]->delete();
            }
            return apiResponse(200, trans('messages.code_activated', [] , $request->header('lang')), ['users' =>$user]);
        }
        if(Otp::where('user_id' , $user->id)->where('code' , $request->code)->where('is_valid' , 0)->first() &&
            User::where('id' , $user->id)->where('phone' , $request->data)->orWhere('email', $request->data)->first() ) {
            $otp = Otp::where('user_id', $user->id)->where('code', $request->code)->where('is_valid', 0)
                ->orderBy('id', 'desc')->first();
            $ot_expired = Carbon::parse($otp->created_at)->addMinutes(2);

            if ($ot_expired >= Carbon::now()) {
                $user->email_verified_at =  Carbon::now() ;
                $user->save();
                $otps=Otp::where('user_id',$user->id)->get();
                foreach ($otps as $key=>$otp){
                    $otps[$key]->delete();
                }
                //Otp::where('user_id', $user->id)->where('code', $request->code)->where('is_valid', 0)->delete();
                return apiResponse(200, trans('messages.code_activated', [] , $request->header('lang')), ['users' =>$user]);
            } else {

                return apiResponse(400, trans('messages.code_expired', [] , $request->header('lang')));
            }
        }
        else {

            return apiResponse(401, trans('messages.invalid_code', [] , $request->header('lang')));
        }
    }
}
