<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Http\Resources\PostsResource;
use App\Models\Comment;
use App\Models\LevelDepartment;
use App\Models\News;
use App\Models\Notification;
use App\Models\Post;
use App\Models\User;
use App\Models\UserLevel;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/posts/create",
     *      operationId="Create Post",
     *      tags={"Posts"},
     *      summary="Create Post",
     *      description="Create Post",
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function create(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $post = new Post();
        $post->text = $request['text'];
        $post->user_id = $user->id;
        $post->approved = $user->type == 1 ? 1 : 0;
        $post->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $post->addMedia($file)->toMediaCollection('post');
        }

        if ($user->type == 1)
        {
        //   Send Notifications To Students
        $levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');

        $userss = UserLevel::whereIn('level_id' , $levels)->where('user_id' , '!=' , $user->id)->pluck('user_id');
        $users = User::whereIn('id' , $userss)->where('university_id' , $user->university_id)->where('faculty_id' , $user->faculty_id)->
        where('department_id' , $user->department_id)->get();

        foreach ($users as $usr)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم رفع منشور جديد بواسطة ".$post->user->full_name;
            $notification_patient->user_id = $usr->id;
            $notification_patient->sender_id = $post->user->id ;
            $notification_patient->action_type = 'post' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }
     }

//        foreach ($levels as $level)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع منشور جديد بواسطة ".$user->full_name;
//            $notification_patient->sender_id = $user->id ;
//            $notification_patient->action_type = 'post' ;
//            $notification_patient->level_id = $level ;
//            $notification_patient->save();
//
//        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }
    /**
     * @SWG\Post(
     *      path="/posts/create-multi",
     *      operationId="Create Post to Level_Department",
     *      tags={"Posts"},
     *      summary="Create Post to Level_Department",
     *      description="Create Post to Level_Department",
     *      @SWG\Parameter(
     *          name="text",
     *          description="Post Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="department",
     *          description="Departmnet ID",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="level",
     *          description="Level",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function create_multi(Request $request){
        $level_department=LevelDepartment::where('department_id',$request['department'])->where('level',$request['level'])->first();
        $user =\JWTAuth::parseToken()->authenticate();
        $post = new Post();
        $post->text = $request['text'];
        $post->user_id = $user->id;
        $post->approved = $user->type == 1 ? 1 : 0;
        $post->level_department_id=$level_department->id;
        $post->save();
        if ($request->file('image'))
        {
            $file = $request->file('image');
            $post->addMedia($file)->toMediaCollection('post');
        }


        if ($user->type == 1)
        {
            //   Send Notifications To Students
            //$levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');

            $userss = UserLevel::where('level_id' , $level_department->id)->where('user_id' , '!=' , $user->id)->pluck('user_id');
            $users = User::whereIn('id' , $userss)->get();
            foreach ($users as $usr)
            {
                $notification_patient = new Notification();
                $notification_patient->title = "تم رفع منشور جديد بواسطة ".$post->user->full_name;
                $notification_patient->user_id = $usr->id;
                $notification_patient->sender_id = $post->user->id ;
                $notification_patient->action_type = 'post' ;
                $notification_patient->save();
                userNotification($notification_patient);

            }
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Get(
     *      path="/posts/index",
     *      operationId="Show posts",
     *      tags={"Posts"},
     *      summary="Show posts",
     *      description="get postss",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $posts = Post::where('approved' , 1)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), PostsResource::collection($posts));

    }

    /**
     * @SWG\Get(
     *      path="/posts/admin-posts",
     *      operationId="Show admin posts",
     *      tags={"Posts"},
     *      summary="Show admin posts",
     *      description="get admin postss",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function adminPosts(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $posts = Post::whereNull('user_id')->where('level_department_id',null)->orWhere('level_department_id',)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), PostsResource::collection($posts));
    }

    /**
     * @SWG\Post(
     *      path="/posts/like",
     *      operationId="like posts",
     *      tags={"Posts"},
     *      summary="like posts",
     *      description="like posts",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function like(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $post = Post::find($request->post_id);
        if ($post->liked_by !== null && in_array(json_encode($user->id) , json_decode($post->liked_by)) == 1)
        {
            return apiResponse(400, trans('like_exist' , [] , $request->header('lang')));
        }
        $post->likes +=1;
        $liked_by = [];

        if ($post->liked_by == null)
            $liked_by[] = json_encode($user->id);

        if ($post->liked_by !== null)
        {
            $liked_by = json_decode($post->liked_by);

            array_push($liked_by , json_encode($user->id));
        }

        $post->liked_by =$liked_by ;
        $post->save();

        /*
        //   Send Notifications To Student

        if ($post->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الاعجاب بالمنشور التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $post->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'post' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }*/

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/posts/dislike",
     *      operationId="dislike posts",
     *      tags={"Posts"},
     *      summary="dislike posts",
     *      description="dislike posts",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function dislike(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $post = Post::find($request->post_id);
        if (in_array(json_encode($user->id) , json_decode($post->liked_by)) == 1)
        {
            $liked_by = [];
            for($i = 0 ; $i < count(json_decode($post->liked_by)) ; $i++)
            {
                if (json_decode($post->liked_by)[$i] == $user->id)
                    continue;

                if (json_decode($post->liked_by)[$i] !== $user->id)
                {
                    $liked_by []=json_decode($post->liked_by)[$i];
                }
            }

            $post->liked_by = $liked_by;
            $post->likes -=1;
            $post->save();
            return apiResponse(200, trans('Success' , [] , $request->header('lang')));

        }

    }

    /**
     * @SWG\Post(
     *      path="/posts/report",
     *      operationId="report posts",
     *      tags={"Posts"},
     *      summary="report posts",
     *      description="report posts",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $post = Post::find($request->post_id);
        $post->report +=1;
        $post->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/posts/edit",
     *      operationId="edit posts",
     *      tags={"Posts"},
     *      summary="edit posts",
     *      description="edit posts",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="title",
     *          description="post text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function update(Request $request)
    {
        $post = Post::find($request->post_id);
        $post->text =$request->title;
        $post->save();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }


    /**
     * @SWG\Post(
     *      path="/posts/delete",
     *      operationId="delete posts",
     *      tags={"Posts"},
     *      summary="delete posts",
     *      description="delete posts",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="post_id",
     *          description="post id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function delete(Request $request)
    {
        $post = Post::find($request->post_id);
        $post->delete();

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

}
