<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\HomeResource;
use App\Http\Resources\LevelsResource;
use App\Http\Resources\NewsResource;
use App\Http\Resources\PostsResource;
use App\Models\AppImage;
use App\Models\LevelDepartment;
use App\Models\News;
use App\Models\Notification;
use App\Models\Post;
use App\Models\Subject;
use App\Models\SubjectFile;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserSubject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use App\Traits\ApiResponser;

class HomeController extends Controller
{
    /**
     * @SWG\Post (
     *      path="/home/index",
     *      operationId="get all home data ",
     *      tags={"Home"},
     *      summary="get all home data",
     *      description="get home data ",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="filter",
     *          description="filter with all , materials , news , posts",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="level",
     *          description="filter with level",
     *          required=false,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        if(request()->header('authorization')&&request()->header('authorization')!=""){
            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(501, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
        else
            return apiResponse(415, trans('messages.login', [] , $request->header('lang')));


        if ($request->filter == 'all')
        {
            if ($request->level && $request->level !== '')
            {
                $level_department=LevelDepartment::where(['level'=>$request->level,'department_id'=>$user->department_id])->first();
                $data['materials']  = HomeResource::collection(Subject::where('department_id' , $level_department->id)->get());
                //$data['levels']  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get(['id' ,'level','department_id']);
                $data['levels'] = LevelsResource::collection(LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get());
                $users_same_level=UserLevel::where('level_id',$level_department->id)->pluck('user_id');
                $data['news']  = NewsResource::collection(News::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
                $data['posts']  = PostsResource::collection(Post::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
                return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);
            }
            $level=LevelDepartment::findOrFail(UserLevel::where('user_id',$user->id)->first()->level_id)->level;
            $level_department=LevelDepartment::where(['level'=>$level,'department_id'=>$user->department_id])->first();
            $data['materials']  = HomeResource::collection(Subject::where('department_id' , UserLevel::where('user_id' , $user->id)->first()->level_id)->get());
            //$data['levels']  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get(['id' ,'level','department_id']);
            $data['levels']=LevelsResource::collection(LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get());
            $users_same_level=UserLevel::where('level_id',$level_department->id)->pluck('user_id');
            $data['news']  = NewsResource::collection(News::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
            $data['posts']  = PostsResource::collection(Post::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);
        }

        if ($request->filter == 'materials')
        {
            //$data['levels']  = LevelDepartment::whereIn('department_id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->pluck('id' ,'level','department_id');
            $data['levels']=LevelsResource::collection(LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get());

            if ($request->level && $request->level !== '')
            {
                $level_department=LevelDepartment::where(['level'=>$request->level,'department_id'=>$user->department_id])->first();
                $data['materials']  = HomeResource::collection(Subject::where('department_id' , $level_department->id)->get());
                return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);

            }
            $data['materials']  = HomeResource::collection(Subject::where('department_id' , UserLevel::where('user_id' , $user->id)->first()->level_id)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);

        }

        if ($request->filter == 'news')
        {
            $level=LevelDepartment::findOrFail(UserLevel::where('user_id',$user->id)->first()->level_id)->level;
            $level_department=LevelDepartment::where(['level'=>$level,'department_id'=>$user->department_id])->first();
            //$data['levels']  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get(['id' ,'level','department_id']);
            $data['levels']=LevelsResource::collection(LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get());
            $users_same_level=UserLevel::where('level_id',$level_department->id)->pluck('user_id');
            $data['news']  = NewsResource::collection(News::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);
        }

        if ($request->filter == 'posts')
        {
            $level=LevelDepartment::findOrFail(UserLevel::where('user_id',$user->id)->first()->level_id)->level;
            $level_department=LevelDepartment::where(['level'=>$level,'department_id'=>$user->department_id])->first();
            $data['levels']=LevelsResource::collection(LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get());
            //$data['levels']  = LevelDepartment::whereIn('id' , UserLevel::where('user_id' , $user->id)->pluck('level_id'))->get(['id' ,'level','department_id']);
            $users_same_level=UserLevel::where('level_id',$level_department->id)->pluck('user_id');
            $data['posts']  = PostsResource::collection(Post::whereIn('user_id',$users_same_level)->orWhere('level_department_id',$level_department->id)->where('approved' , 1)->get());
            return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);
        }

    }

    /**
     * @SWG\Post(
     *      path="/home/search",
     *      operationId="search on materials",
     *      tags={"Home"},
     *      summary="search on materials",
     *      description="search on materials",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="search",
     *          description="search",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function search(Request $request)
    {
        if(request()->header('authorization')){

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        }
        else
            return apiResponse(401, trans('messages.login', [] , $request->header('lang')));


        $data['materials']  = HomeResource::collection(Subject::where('name_'.$request->header('lang') , 'like' , '%' . $request['search'] . '%')->get());

        return apiResponse(200, trans('Success', [] , $request->header('lang')), $data);
    }

    /**
     * @SWG\Post(
     *      path="/home/upload-file",
     *      operationId="upload file on materials",
     *      tags={"Home"},
     *      summary="upload file on materials",
     *      description="upload file on materials",
     *
     *          @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="name",
     *          description="file name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="subject_id",
     *          description="subject_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="department_id",
     *          description="department_id",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="level",
     *          description="level",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="file_type",
     *          description="0 if video , 1 if sheet , 2 summary",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="file",
     *          description="file",
     *          required=true,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:notifications", "read:notifications"}
     *         }
     *     },
     * )
     *
     */
    public function upload(Request $request)
    {
        if (request()->header('authorization')) {

            try {
                $user = \JWTAuth::parseToken()->authenticate();

            } catch (JWTException $e) {

                return apiResponse(500, 'كود التسجيل خطا ، من فضلك ادخل الكود الصحيح');
            }
        } else
            return apiResponse(401, trans('messages.login', [], $request->header('lang')));

        if (strlen($request->department_id) == 1) {
            $subject = SubjectFile::create([
                'name' => $request->name,
                'subject_id' => $request->subject_id,
                'file_type' => $request->file_type,
                'department_id' => $request->department_id,
                'level' => $request->level,
                'user_id'=>$user->id,
            ]);

            $file = $request->file('file');
            $subject->addMedia($file)->toMediaCollection('subject');
            $subject->save();
            //   Send Notifications To Students

            // $levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');
            $userss = UserLevel::where('level_id', $request->level)->where('user_id', '!=', $user->id)->pluck('user_id');
            $users = User::whereIn('id', $userss)->where('university_id', $user->university_id)->where('faculty_id', $user->faculty_id)->
            where('department_id', $request->department_id)->get();
            $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject_id)->pluck('user_id'))->get();

            foreach ($users as $use) {
                $notification_patient = new Notification();
                $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
                $notification_patient->user_id = $use->id;
                $notification_patient->sender_id = $subject->user_id;
                $notification_patient->action_type = 'file';
                $notification_patient->level_id = $request->level;
                $notification_patient->save();
                userNotification($notification_patient);
            }

            foreach ($users_subjects as $usr) {
                $notification_patient = new Notification();
                $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name . " بواسطة " . $user->full_name;
                $notification_patient->user_id = $usr->id;
                $notification_patient->sender_id = $subject->user_id;
                $notification_patient->action_type = 'file';
                $notification_patient->level_id = $request->level;
                $notification_patient->save();
                userNotification($notification_patient);
            }
        }
        if (strlen($request->department_id) > 1)
        {
            $deps = array_map('intval', explode(',', $request['department_id']));
            foreach ($deps as $dep)
            {
                $subject = SubjectFile::create([
                    'name' => $request->name,
                    'subject_id' => $request->subject_id,
                    'file_type' => $request->file_type,
                    'department_id' => $dep,
                    'level' => $request->level,
                    'user_id'=>$user->id,
                ]);

                $file = $request->file('file');
                $subject->addMedia($file)->toMediaCollection('subject');
                $subject->save();

                //   Send Notifications To Students

                // $levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');
                $userss = UserLevel::where('level_id' , $request->level)->where('user_id' , '!=' , $user->id)->pluck('user_id');
                $users = User::whereIn('id' , $userss)->where('university_id' , $user->university_id)->where('faculty_id' , $user->faculty_id)->
                where('department_id' , $dep)->get();

                $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject_id)->pluck('user_id'))->get();

                foreach ($users as $use) {
                    $notification_patient = new Notification();
                    $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name." بواسطة ".$user->full_name;
                    $notification_patient->user_id = $use->id;
                    $notification_patient->sender_id = $use->id ;
                    $notification_patient->action_type = 'file' ;
                    $notification_patient->level_id = $request->level ;
                    $notification_patient->save();
                    userNotification($notification_patient);
                }


                foreach ($users_subjects as $usr) {
                    $notification_patient = new Notification();
                    $notification_patient->title = "تم رفع ملف شرح جديد لمادة " . $subject->name." بواسطة ".$user->full_name;
                    $notification_patient->user_id = $usr->id;
                    $notification_patient->sender_id = $usr->id ;
                    $notification_patient->action_type = 'file' ;
                    $notification_patient->level_id = $request->level ;
                    $notification_patient->save();
                    userNotification($notification_patient);
                }


            }
        }

        return apiResponse(200, trans('Success', [] , $request->header('lang')));
    }

}
