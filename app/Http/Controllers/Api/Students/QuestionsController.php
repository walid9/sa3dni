<?php

namespace App\Http\Controllers\Api\Students;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Http\Resources\PostsResource;
use App\Http\Resources\QuestionsResource;
use App\Models\News;
use App\Models\Notification;
use App\Models\Post;
use App\Models\Question;
use App\Models\User;
use App\Models\UserLevel;
use App\Models\UserSubject;
use App\Traits\ApiResponser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }

    /**
     * @SWG\Post(
     *      path="/questions/create",
     *      operationId="Create Question",
     *      tags={"Questions"},
     *      summary="Create Question",
     *      description="Create Questions",
     *      @SWG\Parameter(
     *          name="text",
     *          description="Question Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *     @SWG\Parameter(
     *          name="subject_id",
     *          description="subject_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     *
     */
    public function create(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $question= new Question();
        $question->text = $request['text'];
        $question->user_id = $user->id;
        $question->subject_id=$request->subject_id;
        $question->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $question->addMedia($file)->toMediaCollection('question');
        }

        /*
        //   Send Notifications To Students

        $levels = UserLevel::where('user_id' , $user->id)->pluck('level_id');
        $userss = UserLevel::whereIn('level_id' , $levels)->where('user_id' , '!=' , $user->id)->pluck('user_id');
        $users = User::whereIn('id' , $userss)->where('university_id' , $user->university_id)->where('faculty_id' , $user->faculty_id)->
        where('department_id' , $user->department_id)->get();

        $users_subjects = User::whereIn('id', UserSubject::where('subject_id' , $request->subject_id)->pluck('user_id'))->get();

//        foreach ($users as $usr)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع سؤال جديد بواسطة ".$user->full_name;
//            $notification_patient->user_id = $usr->id;
//            $notification_patient->sender_id = $user->id ;
//            $notification_patient->action_type = 'question' ;
//            $notification_patient->save();
//            userNotification($notification_patient);
//
//        }

//        foreach ($users_subjects as $use)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع سؤال جديد بواسطة ".$user->full_name;
//            $notification_patient->user_id = $use->id;
//            $notification_patient->sender_id = $user->id ;
//            $notification_patient->action_type = 'question' ;
//            $notification_patient->save();
//            userNotification($notification_patient);
//
//        }

//        foreach ($levels as $level)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم رفع سؤال جديد بواسطة ".$user->full_name;
//            $notification_patient->sender_id = $user->id ;
//            $notification_patient->action_type = 'question' ;
//            $notification_patient->level_id = $level ;
//            $notification_patient->save();
//
//        }
*/
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/questions/edit",
     *      operationId="Edit Question",
     *      tags={"Questions"},
     *      summary="Edit Question",
     *      description="Edit Questions",
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="Question Id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          description="Question Text",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="image",
     *          required=false,
     *          type="file",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     *
     */
    public function update(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $question= Question::find($request->question_id);
        $question->text = $request['text'];
        $question->save();

        if ($request->file('image'))
        {
            $file = $request->file('image');
            $question->addMedia($file)->toMediaCollection('question');
        }

        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/questions/delete",
     *      operationId="delete Question",
     *      tags={"Questions"},
     *      summary="delete Question",
     *      description="delete Questions",
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="Question Id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     *
     */
    public function delete(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
         Question::find($request->question_id)->delete();
        return apiResponse(200, trans('messages.success' , [] , $request->header('lang')));

    }


    /**
     * @SWG\Get(
     *      path="/questions/index",
     *      operationId="Show questions",
     *      tags={"Questions"},
     *      summary="Show questions",
     *      description="get questions",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *     @SWG\Parameter(
     *          name="subject_id",
     *          description="subject_id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function index(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $questions = Question::where('subject_id',$request->subject_id)->where('approved' , 1)->get();
        return apiResponse(200, trans('Success' , [] , $request->header('lang')), QuestionsResource::collection($questions));
    }

    /**
     * @SWG\Post(
     *      path="/questions/like",
     *      operationId="like questions",
     *      tags={"Questions"},
     *      summary="like questions",
     *      description="like questions",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="question id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function like(Request $request)
    {
        $user =\JWTAuth::parseToken()->authenticate();
        $question = Question::find($request->question_id);
        if ($question->liked_by !== null && in_array(json_encode($user->id) , json_decode($question->liked_by)) == 1)
        {
            return apiResponse(400, trans('like_exist' , [] , $request->header('lang')));

        }
        $question->likes +=1;
        $liked_by = [];

        if ($question->liked_by == null)
            $liked_by[] = json_encode($user->id);

        if ($question->liked_by !== null)
        {
            $liked_by = json_decode($question->liked_by);

            array_push($liked_by , json_encode($user->id));
        }

        $question->liked_by =$liked_by ;
        $question->save();

        //   Send Notifications To Student

        if ($question->user_id !== $user->id)
        {
            $notification_patient = new Notification();
            $notification_patient->title = "تم الاعجاب بالسؤال التابع لك بواسطة ".$user->full_name;
            $notification_patient->user_id = $question->user_id ;
            $notification_patient->sender_id = $user->id ;
            $notification_patient->action_type = 'question' ;
            $notification_patient->save();
            userNotification($notification_patient);

        }

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

    /**
     * @SWG\Post(
     *      path="/questions/dislike",
     *      operationId="dislike questions",
     *      tags={"Questions"},
     *      summary="dislike questions",
     *      description="dislike questions",
     *       @SWG\Parameter(
     *          name="authorization",
     *          description="authorization token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="question id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function dislike(Request $request)
    {
        $user = \JWTAuth::parseToken()->authenticate();
        $question = Question::find($request->question_id);
        if (in_array(json_encode($user->id), json_decode($question->liked_by)) == 1) {
            $liked_by = [];
            for ($i = 0; $i < count(json_decode($question->liked_by)); $i++) {
                if (json_decode($question->liked_by)[$i] == $user->id)
                    continue;

                if (json_decode($question->liked_by)[$i] !== $user->id) {
                    $liked_by [] = json_decode($question->liked_by)[$i];
                }
            }

            $question->liked_by = $liked_by;
            $question->likes -= 1;
            $question->save();
            return apiResponse(200, trans('Success', [], $request->header('lang')));

        }
    }

    /**
     * @SWG\Post(
     *      path="/questions/report",
     *      operationId="report questions",
     *      tags={"Questions"},
     *      summary="report questions",
     *      description="report questions",
     *      @SWG\Parameter(
     *          name="lang",
     *          description="lang",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="question_id",
     *          description="question id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function report(Request $request)
    {
        $question = Question::find($request->question_id);
        $question->report +=1;
        $question->save();

        //   Send Notifications To Student

//        if ($post->user_id !== $user->id)
//        {
//            $notification_patient = new Notification();
//            $notification_patient->title = "تم الاعجاب بالمنشور التابع لك بواسطة ".$user->full_name;
//            $notification_patient->user_id = $post->user_id ;
//            $notification_patient->save();
//            userNotification($notification_patient);
//
//        }

        return apiResponse(200, trans('Success' , [] , $request->header('lang')));

    }

}
