<?php

namespace App\Http\Resources;

use App\Http\Resources\LocationsResource;
use App\Models\LevelDepartment;
use App\Models\Notification;
use App\Models\Subject;
use App\Models\UserBalance;
use App\Models\UserLevel;
use App\Models\UserPromoCode;
use App\Models\UserSubject;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this) {
            if ($this->type == 0)
                $type = 'normal student';

            if ($this->type == 1)
                $type = 'excellent student';

            if ($this->type == 2)
                $type = 'teaching assistant';

            if ($this->type == 3)
                $type = 'doctor';
        }
        else{
            $type = 'normal student';
        }
        if($this) {
            return [
                'id' => $this->id,
                'full_name' => $this->full_name ?? "",
                'user_name' => $this->user_name ?? "",
                'email' => $this->email ?? "",
                'phone' => $this->phone,
                'gender' => $this->gender,
                'deadline' => $this->deadline,
                'device_id' => $this->device_id,
                'status' => $this->status == 0 ? 'not_paid' : 'paid',
                'image' => $this->img ?? 'Null',
                'type' => $type,
                'university_id' => $this->university_id,
                'university_name' => $this->university->name,
                'faculty_id' => $this->faculty_id,
                'faculty_name' => $this->faculty->name,
                'department_id' => $this->department_id,
                'department_name' => $this->department->name,
                'has_subjects' => UserSubject::where('user_id', $this->id)->count() > 0 ? 'true' : 'false',
                'level' => LevelsResource::collection(LevelDepartment::whereIn('id', UserLevel::where('user_id', $this->id)->pluck('level_id'))->get()),
                'subjects' => HomeResource::collection(Subject::whereIn('id', UserSubject::where('user_id', $this->id)->pluck('subject_id'))->get()),
                'promo_code' => UserPromoCode::where('user_id',$this->id)->pluck('promo_code')->first(),
                'promo_status' => UserPromoCode::where('user_id',$this->id)->pluck('status')->first(),
                'balance_cache' => UserBalance::where('user_id',$this->id)->pluck('cache')->first(),
                'balance_service' => UserBalance::where('user_id',$this->id)->pluck('service')->first(),
            ];
        }
        else{
            return [
                'id' => null,
                'full_name' => "User Sa3dni",
                'user_name' => "user_sa3dni",
                'email' => "",
                'phone' => "",
                'gender' => "male",
                'deadline' => null,
                'device_id' => null,
                'status' => 'not_paid' ,
                'image' => 'Null',
                'type' => $type,
                'university_id' => null,
                'university_name' => null,
                'faculty_id' => null,
                'faculty_name' => null,
                'department_id' => null,
                'department_name' => null,
                'has_subjects' =>  'false',
                'level' => null,
                'subjects' => null,
                'promo_code'=>null,
            ];
        }
    }
}
