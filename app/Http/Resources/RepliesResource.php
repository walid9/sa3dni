<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class RepliesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->user) {
            if ($this->user->type == 0)
                $type = 'normal student';

            if ($this->user->type == 1)
                $type = 'excellent student';

            if ($this->user->type == 2)
                $type = 'teaching assistant';

            if ($this->user->type == 3)
                $type = 'doctor';
        }
        else {
            $type = 'normal student';
        }
        if($this->user) {
            return [
                'id' => $this->id,
                'user_id' => $this->user_id,
                'full_name' => $this->user->full_name ?? "",
                'user_image' => $this->user->img ?? 'Null',
                'user_gender' => $this->user->gender,
                'user_type' => $type,
                'user_verified' => $this->user->verified,
                'user_helper' => $this->user->rank == User::whereNotNull('email_verified_at')->where('faculty_id',$this->user->faculty_id)->max('rank') && $this->user->rank>0 ? 1 : 0,
                'text' => $this->text ?? "",
                'comment_img' => $this->img ? $this->img : "",
                'likes' => $this->likes,
                'has_like' => $this->liked_by !== null && in_array(json_encode(auth()->user()->id), json_decode($this->liked_by)) == 1 ? 'true' : 'false',
                'report' => $this->report,
                'created_id' => $this->created_at ?? "",
            ];
        }
        else{
            return [
                'id' => $this->id,
                'full_name' => "User Sa3dni",
                'user_image' => 'Null',
                'user_gender' => 'male',
                'user_type' => $type,
                'user_verified' =>0,
                'user_helper' => 0,
                'text' => $this->text ?? "",
                'comment_img' => $this->img ? $this->img : "",
                'likes' => $this->likes,
                'has_like' => $this->liked_by !== null && in_array(json_encode(auth()->user()->id), json_decode($this->liked_by)) == 1 ? 'true' : 'false',
                'report' => $this->report,
                'created_id' => $this->created_at ?? "",
            ];
        }
    }
}
