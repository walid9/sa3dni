<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $type = '';

        if ($this->sender && $this->sender->type == 0)
            $type = 'normal student';

        if ($this->sender && $this->sender->type == 1)
            $type = 'excellent student';

        if ($this->sender && $this->sender->type == 2)
            $type = 'teaching assistant';

        if ($this->sender && $this->sender->type == 3)
            $type = 'doctor';
        if($this->sender==null)
            $type = 'admin';

        if($this->sender) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'read' => $this->read == 1 ? true : false,
                'action_type' => $this->action_type,
                'user_name' => $type? "Sa3dni Team":optional($this->sender)->full_name,
                'user_image' => optional($this->sender)->img ?? 'Null',
                'user_gender' => optional($this->sender)->gender,
                'user_type' => $type,
                'user_verified' => optional($this->sender)->verified,
                'user_helper' => $this->sender->rank == User::whereNotNull('email_verified_at')->where('faculty_id',$this->sender->faculty_id)->max('rank') && $this->sender->rank>0 ? 1 : 0,
                'created_at' => $this->created_at,
            ];
        }
        elseif ($type=='admin'){
            return [
                'id' => $this->id,
                'title' => $this->title,
                'read' => $this->read == 1 ? true : false,
                'action_type' => $this->action_type,
                'user_name' => "Sa3dni Admin",
                'user_image' => optional($this->sender)->img ?? 'Null',
                'user_gender' => optional($this->sender)->gender,
                'user_type' => $type,
                'user_verified' => 1,
                'user_helper' => 0,
                'created_at' => $this->created_at,
            ];
        }
        else{
            return [
                'id' => $this->id,
                'title' => $this->title,
                'body' => $this->body,
                'read' => $this->read == 1 ? true : false,
                'action_type' => $this->action_type,
                'user_name' => "User Sa3dni",
                'user_image' => 'Null',
                'user_gender' => 'male',
                'user_type' => $type,
                'user_verified' => 0,
                'user_helper' =>  0,
                'created_at' => $this->created_at,
            ];
        }
    }
}
