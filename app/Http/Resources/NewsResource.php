<?php

namespace App\Http\Resources;

use App\Http\Resources\LocationsResource;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->user) {
            if ($this->user_id !== null && $this->user->type == 0)
                $type = 'normal student';

            if ($this->user_id !== null && $this->user->type == 1)
                $type = 'excellent student';

            if ($this->user_id !== null && $this->user->type == 2)
                $type = 'teaching assistant';

            if ($this->user_id !== null && $this->user->type == 3)
                $type = 'doctor';

            if ($this->user_id == null)
                $type = 'admin';
        }
        else{
            $type='normal student';
        }
        if($this->user) {
            return [
                'id' => $this->id,
                'full_name' => $this->user->type ? "Sa3dni Team":$this->user->full_name,
                'user_gender' => optional($this->user)->gender,
                'user_type' => $type,
                'user_verified' => $this->user->verified ?? 1,
                'user_helper' => $this->user->rank == User::whereNotNull('email_verified_at')->where('faculty_id',$this->user->faculty_id)->max('rank') && $this->user->rank>0 ? 1 : 0,
                'image' => $this->user->img ?? 'Null',
                'NewsText' => $this->newsText ?? "",
                'report' => $this->report,
                'created_id' => $this->created_at ?? "",
                'user_id' => $this->user_id,
            ];
        }
        else{
            return [
                'id' => $this->id,
                'full_name' => "User Sa3dni",
                'user_gender' => 'male',
                'user_type' => $type,
                'user_verified' => 0,
                'user_helper' =>  0,
                'image' => 'Null',
                'NewsText' => $this->newsText ?? "",
                'report' => $this->report,
                'created_id' => $this->created_at ?? "",
                'user_id' => $this->user_id,
            ];
        }
    }
}
