<?php

namespace App\Http\Resources;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class PostsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        if ($this->user_id !== null && $this->user->type == 0)
            $type = 'normal student';

        if ($this->user_id !== null && $this->user->type == 1)
            $type = 'excellent student';

        if ($this->user_id !== null && $this->user->type == 2)
            $type = 'teaching assistant';

        if ($this->user_id !== null && $this->user->type == 3)
            $type = 'doctor';

        if ($this->user_id == null)
            $type = 'admin';

        if ($this->user_id != null)
        {
            return [
                'id' => $this->id,
                'full_name' => $this->user->type ? "Sa3dni Team":$this->user->full_name,
                'user_image' => $this->user->img ?? 'Null',
                'user_type' => $type,
                'user_verified' => $this->user->verified ?? 1,
                'user_helper' => $this->user->rank == User::whereNotNull('email_verified_at')->where('faculty_id', $this->user->faculty_id)->max('rank') && $this->user->rank > 0 ? 1 : 0,
                'text' => $this->text ?? "",
                'post_img' => $this->img ? $this->img : "",
                'likes' => $this->likes,
                'has_like' => $this->liked_by !== null && in_array(json_encode(auth()->user()->id), json_decode($this->liked_by)) == 1 ? 'true' : 'false',
                'report' => $this->report,
                'created_at' => $this->created_at ?? "",
                'comments' => Comment::where('post_id', $this->id)->count(),
                'user_id' => $this->user_id,
                'user_gender' => optional($this->user)->gender,
            ];
    }
        else{
            return [
                'id' => $this->id,
                'full_name' => "Sa3dni",
                'user_image' => 'Null',
                'user_type' => $type,
                'user_verified' =>  1,
                'user_helper' => 0 ,
                'text' => $this->text ?? "",
                'post_img' => $this->img ? $this->img : "",
                'likes' => $this->likes,
                'has_like' => $this->liked_by !== null && in_array(json_encode(auth()->user()->id), json_decode($this->liked_by)) == 1 ? 'true' : 'false',
                'report' => $this->report,
                'created_at' => $this->created_at ?? "",
                'comments' => Comment::where('post_id', $this->id)->count(),
                'user_id' => $this->user_id,
            ];
        }

    }
}
