<?php

namespace App\Http\Resources;
use App\Models\LevelDepartment;
use App\Models\User;
use App\Models\UserLevel;
use Illuminate\Http\Resources\Json\JsonResource;
use  App\Http\Resources\LevelsResource;
class RankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'full_name' => $this->full_name ?? "",
            'user_gender' => $this->gender,
            'user_type' => $this->type,
            'image' => $this->img ?? 'Null',
             'rank'=>$this->rank,
            'user_verified' => $this->verified,
            'user_helper' => $this->rank == User::whereNotNull('email_verified_at')->where('faculty_id',$this->faculty_id)->max('rank') && $this->rank>0 ? 1 : 0,
        ];
    }
}
