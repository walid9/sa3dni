<?php

namespace App\Http\Resources;

use App\Models\AppImage;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'name' => $this->name,
            'image' => AppImage::where('type','subject')->inRandomOrder()->first()->img??"",
        ];
    }
}
