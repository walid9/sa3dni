<?php

namespace App\Http\Requests\Api\profile;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required|string|max:150',
            'full_name' => 'required|string|max:150',
            'email' => 'required|email',
            'phone' => 'required|string',
            'university_id' => 'required|integer',
            'faculty_id' => 'required|integer',
            'department_id' => 'required|integer',
            'level' => 'required',

        ];
    }
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(apiResponse(405, $validator->errors()->first()));
    }
}
