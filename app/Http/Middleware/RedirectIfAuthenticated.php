<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
//        if (Auth::guard($guard)->check()) {
//            return redirect(RouteServiceProvider::HOME);
//        }

        if (Auth::guard($guard)->check() && $guard != 'doctor') {


            return redirect('/')->with(['message' => 'you already login ' . auth()->user()->name,
                'alert-type' => 'info']);
        }

        if (Auth::guard($guard)->check() && $guard == 'doctor') {

            return redirect('/')->with(['message' => 'you already login ' . auth('doctor')->user()->name,
                'alert-type' => 'info']);
        }


        return $next($request);
    }
}
