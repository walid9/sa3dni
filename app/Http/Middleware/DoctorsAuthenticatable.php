<?php

namespace App\Http\Middleware;

use Closure;

class DoctorsAuthenticatable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = auth('api_doctors')->user();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response(apiResponse(405, 'please enter a valid jwt'));
        }

        return $next($request);
    }
}
