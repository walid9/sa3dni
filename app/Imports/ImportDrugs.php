<?php

namespace App\Imports;


use App\Models\Drug;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportDrugs implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Drug([
            'name' => $row[0] ?? "",
            //'dose' => $row[1] ?? "",
            'unit' => $row[2] ?? "",
            'route'  => $row[3] ?? "",
            //'frequency' => $row[4] ?? "",
           // 'duration'  => $row[5] ?? "",
            'duration_unit' => $row[6] ?? "",
            'prn'    => $row[7] ?? "",
            'indications'    => $row[8] ?? "",
            //'order_instructions'  => $row[9] ?? "",
        ]);
    }
}
