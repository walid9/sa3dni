<?php


namespace App\Services;


use App\Models\Time;
use Carbon\Carbon;

trait DateServices
{
    public function makeDate()
    {
        $time = Time::get()->first();

        $value = explode(' ',$time->value);

        $type = $this->getType($value);
        $hours = $this->getHours($value);

    }

    public function getType($value)
    {
        $type = '';

        if(is_array($value))
            $type = $value[1];

        return $type;
    }

    public function getHours($value)
    {
        $hours = [];

        if(is_array($value)){

            $data = explode(':',$value[0]);

            $hours = $data[0];
        }

        return $hours;
    }

    public function onlyCreateDate($date){

        $selected_date = explode('-',$date);

        $month =  $selected_date[1];
        $day   = $selected_date[2];
        $year  = $selected_date[0];

        $creation_date = Carbon::create($year,$month,$day,0,0,0);

        return $creation_date;
    }
}