<?php


namespace App\Services;


use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

trait ValidationService
{

    public function rules(){

        $rules = [

            'doctor_id' => 'required|integer|exists:doctors,id',
            'book_time' => 'required|string',
            'days_value' => 'required|integer|min:0|max:6',
        ];

        return $rules;
    }

    public function validationTest($request){

        $rules = $this->rules();

        $customValidationMessages = $this->customValidationMessages();

        $validator = Validator::make($request->all(), $rules,$customValidationMessages);

        if ($validator->fails())
            return $validator;
        else
            return false;
    }

    public function checkAuth(){

        $message = '';

        if(!getAuth())
            $message = __('sorry please sign in');


        if(auth('doctor')->check())
           $message = __('sorry please sign up as patient');

        return $message;
    }

    public function customValidationMessages(){

        $customMessagesEn = [
            'doctor_id.required' => __('sorry please select doctor'),
            'doctor_id.integer' => __('sorry this doctor not valid'),
            'doctor_id.exists' =>  __('sorry this doctor not valid'),

            'book_time.required' =>  __('sorry please select time'),
            'book_time.string' =>  __('sorry this time not valid'),

            'days_value.required' =>  __('sorry day value is required'),
            'days_value.integer' =>  __('sorry day value must be integer'),
            'days_value.min' =>  __('sorry day value must be equal zero or more'),
            'days_value.max' =>  __('sorry day value must be equal five or less'),
        ];

        return $customMessagesEn;
    }
}
