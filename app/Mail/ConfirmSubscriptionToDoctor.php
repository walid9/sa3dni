<?php

namespace App\Mail;

use App\Models\Reservation;
use App\Models\SubscribeOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmSubscriptionToDoctor extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $reservation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, Reservation $reservation)
    {
        $this->subject = $subject;
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $reservation = $this->reservation;

        return $this->view('mail.reservation_in_plan_to_doctor',
            compact('reservation'))->subject($subject);
    }
}
