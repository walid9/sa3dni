<?php

namespace App\Mail;

use App\Models\Reservation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaidReservationToDoctor extends Mailable
{
    use Queueable, SerializesModels;

    public  $reservation;
    public  $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,  Reservation $reservation)
    {
        $this->subject = $subject;
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $reservation = $this->reservation;

        return $this->view('mail.reservation.paid_reservation_doctor',compact('reservation'))->subject($subject);
    }
}
