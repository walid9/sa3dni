<?php

namespace App\Mail;

use App\Models\SubscribeOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmSubscription extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $subscribeOrder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, SubscribeOrder $subscribeOrder)
    {
        $this->subject = $subject;
        $this->subscribeOrder = $subscribeOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->subject;
        $subscribeOrder = $this->subscribeOrder;

        return $this->view('mail.confirm_Subscription', compact('subscribeOrder'))->subject($subject);
    }
}
