<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable , SerializesModels;
    
    public $sub;
    public $msg;
    
    public function __construct($subject , $message)
    {
       $this->sub=$subject;
       $this->msg=$message;
    }
    
    public function build()
    {
       $e_subject=$this->sub;
       $e_message=$this->msg;
       return $this->view('mail.sendMail' , compact('e_message'))->subject($e_subject);
    }
}