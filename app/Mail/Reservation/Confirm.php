<?php

namespace App\Mail\Reservation;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Confirm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $reservation;
    public $date;

    public function __construct($subject, $reservation,$date)
    {
        $this->subject = $subject;
        $this->reservation = $reservation;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $reservation = $this->reservation;
        $date = $this->date;
        return $this->view('mail.reservation.confirm', compact('reservation','date'))
            ->subject($this->subject);
    }
}
