<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Prescription extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $reservation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$reservation)
    {
        $this->subject = $subject;
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $reservation = $this->reservation;
        return $this->view('web.home.prescription_pdf', compact('reservation'))
            ->subject($this->subject);
    }
}
