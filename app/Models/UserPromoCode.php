<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPromoCode extends Model
{
    protected $table = 'users_promocode';
    protected $fillable = [
        'user_id' , 'promo_code' , 'count' , 'status'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
