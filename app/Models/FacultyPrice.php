<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacultyPrice extends Model
{
    protected $table = 'faculties_prices';
    protected $fillable = [
        'university_id' , 'faculty_id' , 'price' , 'period'
    ];

    public function university(){
        return $this->belongsTo(University::class, 'university_id');
    }

    public function faculty(){
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }
}
