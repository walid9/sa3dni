<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Subject extends Model  implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'subjects';
    protected $fillable = [
        'name_en' , 'name_ar' , 'department_id'
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('subject')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('subject')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $url;
    }

        public function getNameAttribute(){

        return App::getLocale() == 'ar' ? $this->name_ar : $this->name_en ;
    }

    public function department(){
        return $this->belongsTo(LevelDepartment::class, 'department_id');
    }

    public function videos(){
        return $this->hasMany(SubjectFile::class)->where('file_type',0);
    }
    public function files(){
        return $this->hasMany(SubjectFile::class)->where('file_type','<>',0);
    }
}
