<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSubject extends Model
{
    protected $table = 'users_subjects';
    protected $fillable = [
        'user_id' , 'subject_id'
    ];

    public function subject(){
        return $this->belongsTo(Subject::class, 'subject_id');
    }
}
