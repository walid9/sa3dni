<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdLevel extends Model
{

    public $table = 'ads_levels';

    protected $fillable = [
        'ad_id',
        'level_department_id',
    ];

    public function ad(){
        return $this->belongsTo(Ad::class,'ad_id');
    }
    public function level_department(){
        return $this->belongsTo(LevelDepartment::class,'level_department_id');
    }
}
