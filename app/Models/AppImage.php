<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class AppImage extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'app_images';
    protected $fillable= ['type'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('app_images')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('app_images')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $url;
    }
}
