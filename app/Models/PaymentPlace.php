<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentPlace extends Model
{
    public $table = 'payments_places';

    protected $fillable = [
        'name',
        'status',
    ];
}
