<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    protected $table = 'users_levels';
    protected $fillable = [
        'user_id' , 'level_id'
    ];
    public function level(){
        return $this->belongsTo(LevelDepartment::class,'level_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

}
