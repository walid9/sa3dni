<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentRequest extends Model
{
    protected $table = 'payment_requests';
    protected $fillable = [
        'user_id' , 'transaction_number' , 'status'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
