<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class SubjectFile extends Model  implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'subject_files';
    protected $fillable = [
        'subject_id' , 'file_type' , 'department_id' , 'level' , 'name','user_id',
    ];

    public function subject(){
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function department(){
        return $this->belongsTo(Department::class, 'department_id');
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('subject')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('subject')->last();
        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $url;
    }

}
