<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Question extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table ='questions';

    protected $fillable=['text' ,'user_id' , 'approved' , 'likes' , 'report','subject_id' , 'liked_by'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('question')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('question')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $url;
    }


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function comments(){
        return $this->hasMany(QComment::class)->with('user');
    }
}
