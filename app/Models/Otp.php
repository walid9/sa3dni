<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    public $table = 'otps';

    protected $dates = [
        'updated_at',
        'created_at',
        'code_verified_at'
    ];

    protected $fillable = [
        'code',
        'is_valid',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

}
