<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    protected $table = 'users_balance';
    protected $fillable = [
        'user_id' , 'cache' , 'service'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
