<?php

namespace App\Models;



use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Thomasjohnkane\Snooze\Traits\SnoozeNotifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject ,  HasMedia
{
    use Notifiable;
    //use HasRoles;
    use HasMediaTrait;
    use SnoozeNotifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name','full_name', 'email', 'password', 'phone','type', 'firebase_token' , 'device_id' , 'code' , 'university_id' , 'faculty_id' , 'department_id' ,
        'verified' , 'rank' , 'status' , 'deadline' , 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('user')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('user')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }

        return $url;
    }

    public function staffImg()
    {
        $file = $this->getMedia('staffImg')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function faculty(){
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }

    public function university(){
        return $this->belongsTo(University::class, 'university_id');
    }

    public function department(){
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function userlevel(){
        return $this->hasMany(UserLevel::class)->with('level');
    }
    public function promo_code(){
        return $this->hasMany(UserPromoCode::class);
    }
    public function balance(){
        return $this->hasMany(UserBalance::class);
    }

}
