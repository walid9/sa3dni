<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Ad extends Model implements HasMedia
{

    use HasMediaTrait;

    public $table = 'ads';

    protected $dates = [
        'updated_at',
        'created_at',
    ];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['img'];

    protected $fillable = [
        'faculty_id',
        'link',
        'start_date',
        'end_date',
        'active',
        'type',
    ];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('ad')
            ->singleFile();
    }
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }
    public function getImgAttribute()
    {
        $file = $this->getMedia('ad')->last();

        if ($file) {
            $file->url = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
            $file->localUrl = asset('storage/'.$file->id.'/'.$file->file_name);
        }

        return $file;
    }

    public function faculty(){
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }

    public function levels() {
        return $this->belongsToMany(AdLevel::class, 'ads_levels' , 'ad_id' ,'level_department_id')->withTimestamps();
    }
}
