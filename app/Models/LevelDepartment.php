<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LevelDepartment extends Model
{
    protected $table = 'level_departments';
    protected $fillable = [
        'level' , 'department_id'
    ];

    public function department(){
        return $this->belongsTo(Department::class, 'department_id');
    }

}
