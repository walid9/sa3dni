<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Managament extends Model
{
    protected $table = 'management';
    protected $fillable = [
        'title' , 'status' ,'info'
    ];
}
