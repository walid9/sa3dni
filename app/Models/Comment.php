<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Comment extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'comments';
    protected $fillable= ['text' ,'user_id' , 'approved' , 'likes' , 'post_id' , 'report' , 'liked_by'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('comment')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('comment')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }
        return $url;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class,'post_id');
    }
    public function reply(){
        return $this->hasMany(CommentReply::class)->with('user');
    }
}
