<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'faculties';
    protected $fillable = [
        'name' , 'university_id'
    ];

    public function university(){
        return $this->belongsTo(University::class, 'university_id');
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }
}
