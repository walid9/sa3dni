<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable= ['newsText' ,'user_id' , 'approved'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
