<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class CommentReply extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'comments_replies';
    protected $fillable= ['text' ,'user_id' , 'likes' , 'comment_id' , 'report' , 'liked_by'];

    public function registerMediaCollections()
    {
        $this->addMediaCollection('reply')
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getImgAttribute()
    {
        $file = $this->getMedia('reply')->last();

        $url = '';
        if ($file) {
            $url = asset('storage/' . $file->id . '/' . $file->file_name);
        }
        return $url;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class,'comment_id');
    }
}
