<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cros'], 'namespace' => 'Api'], function () {

    ////////////////////////////////// Students ////////////////////////////

    include('apis/students/auth.php');
    include('apis/students/app_status.php');
    include('apis/students/home.php');
    include('apis/students/notification.php');
    include('apis/students/posts.php');
    include('apis/students/questions.php');
    include('apis/students/comments.php');
    include('apis/students/Qcomments.php');
    include('apis/students/otp.php');
    include('apis/students/news.php');
    include('apis/students/ranks.php');
    include('apis/students/subjects.php');
    include('apis/students/profile.php');
    include('apis/students/payment_requests.php');
    include('apis/students/contact_us.php');
    include('apis/students/ad.php');
    include('apis/students/replies.php');


});
