<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::get('/','HomeController@index');

Route::get('dashboard/login', 'Dashboard\AuthController@login')->name('login');
Route::post('dashboard/login', 'Dashboard\AuthController@authenticate');
Route::post('dashboard/logout', 'Dashboard\AuthController@logout')->name('logout');

Route::get('/student/login', 'Dashboard\AuthController@login_student')->name('login_student');
Route::post('/student/login', 'Dashboard\AuthController@authenticate_student');
Route::post('/student/logout', 'Dashboard\AuthController@logout_student')->name('logout_student');

Route::group(['middleware'=>'auth:web'],function () {
    Route::get('/student', 'Dashboard\StudentController@index')->name('student');
    Route::get('/student/subjects/video', 'Dashboard\StudentController@video')->name('video_student');
    Route::post('student/subjects/video/add','Dashboard\StudentController@video_add')->name('add_video_student');

    Route::get('/student/subjects/sheet', 'Dashboard\StudentController@sheet')->name('sheet_student');
    Route::post('student/subjects/sheet/add','Dashboard\StudentController@sheet_add')->name('add_sheet_student');

    Route::get('/student/subjects/summary', 'Dashboard\StudentController@summary')->name('summary_student');
    Route::post('student/subjects/summary/add','Dashboard\StudentController@summary_add')->name('add_summary_student');

    Route::get('student/getSubject', 'Dashboard\StudentController@get_subject')->name('get_subject_student');


});
Route::group(['middleware'=>'auth:admin'],function () {
    Route::get('dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::get('dashboard/user/all','Dashboard\UserController@all')->name('users');
    Route::get('dashboard/user/normal','Dashboard\UserController@normal')->name('user_normal');
    Route::get('dashboard/user/excellent','Dashboard\UserController@excellent')->name('user_excellent');
    Route::get('dashboard/user/balance','Dashboard\UserController@balance')->name('user_balance');

    Route::get('dashboard/user/normal/{id}','Dashboard\UserController@to_normal')->name('to_normal');
    Route::get('dashboard/user/excellent/{id}','Dashboard\UserController@to_excellent')->name('to_excellent');
    Route::post('dashboard/user/level/add/','Dashboard\UserController@add_level')->name('add_level_user');
    Route::get('dashboard/user/level/del/{id}','Dashboard\UserController@del_level')->name('del_level_user');
    Route::post('dashboard/user/department/change/','Dashboard\UserController@change_department')->name('change_department');
    Route::get('dashboard/user/del/{id}','Dashboard\UserController@del_user')->name('del_user');
    Route::post('dashboard/user/data','Dashboard\UserController@user_data')->name('user_data');

    Route::get('dashboard/user/payment','Dashboard\UserController@Request_Payment')->name('request_payment');
    Route::post('dashboard/user/payment/approve','Dashboard\UserController@Approve_Request')->name('approve_request');
    Route::get('dashboard/user/payment/del/{id}','Dashboard\UserController@del_Request')->name('del_request');

    Route::post('dashboard/user/verified','Dashboard\UserController@verified')->name('verified');
    Route::post('dashboard/user/rank','Dashboard\UserController@rank')->name('rank');
    Route::get('dashboard/user/deadline','Dashboard\UserController@deadline')->name('check_deadline');
    Route::post('dashboard/user/status','Dashboard\UserController@status')->name('update_status');
    Route::get('dashboard/user/statics','Dashboard\UserController@statics')->name('user_statics');
    Route::post('dashboard/user/reset','Dashboard\UserController@reset')->name('user_reset');
    Route::post('dashboard/user/promo','Dashboard\UserController@promo')->name('user_promo');
    Route::get('dashboard/getUser', 'Dashboard\UserController@get_user')->name('get_user');

    Route::post('dashboard/university/add','Dashboard\UniversityController@add')->name('add_university');
    Route::post('dashboard/university/edit/{id}','Dashboard\UniversityController@edit')->name('edit_university');
    Route::get('dashboard/university/del/{id}','Dashboard\UniversityController@delete')->name('del_university');

    Route::get('dashboard/getFaculty', 'Dashboard\FacultyController@get_faculty')->name('get_faculty');
    Route::get('dashboard/faculty','Dashboard\FacultyController@index')->name('faculty');
    Route::post('dashboard/faculty/add','Dashboard\FacultyController@add')->name('add_faculty');
    Route::post('dashboard/faculty/edit/{id}','Dashboard\FacultyController@edit')->name('edit_faculty');
    Route::get('dashboard/faculty/del/{id}','Dashboard\FacultyController@delete')->name('del_faculty');

    Route::get('dashboard/getDepartment', 'Dashboard\DepartmentController@get_department')->name('get_department');
    Route::get('dashboard/getLevel', 'Dashboard\DepartmentController@get_level')->name('get_level');
    Route::get('dashboard/department','Dashboard\DepartmentController@index')->name('department');
    Route::post('dashboard/department/add','Dashboard\DepartmentController@add')->name('add_department');
    Route::post('dashboard/department/edit/{id}','Dashboard\DepartmentController@edit')->name('edit_department');
    Route::get('dashboard/department/del/{id}','Dashboard\DepartmentController@delete')->name('del_department');
    Route::post('dashboard/department/level/add','Dashboard\DepartmentController@add_level')->name('add_level');
    Route::get('dashboard/department/level/del/{id}','Dashboard\DepartmentController@delete_level')->name('del_level');

    Route::get('dashboard/subject','Dashboard\SubjectController@index')->name('subject');
    Route::post('dashboard/subject/add','Dashboard\SubjectController@add')->name('add_subject');
    Route::post('dashboard/subject/edit/{id}','Dashboard\SubjectController@edit')->name('edit_subject');
    Route::get('dashboard/subject/del/{id}','Dashboard\SubjectController@delete')->name('del_subject');
    Route::get('dashboard/getSubject','Dashboard\SubjectController@get_subject')->name('get_subject');


    Route::get('dashboard/posts/approved','Dashboard\PostController@approved')->name('approved_posts');
    Route::get('dashboard/posts/waiting','Dashboard\PostController@waiting')->name('waiting_posts');
    Route::get('dashboard/posts/rejected','Dashboard\PostController@rejected')->name('rejected_posts');
    Route::get('dashboard/posts/reports','Dashboard\PostController@reports')->name('reports_posts');
    Route::get('dashboard/posts/admin','Dashboard\PostController@admin')->name('admin_posts');
    Route::post('dashboard/posts/add','Dashboard\PostController@add_post')->name('add_post');
    Route::post('dashboard/posts/edit','Dashboard\PostController@edit_post')->name('edit_post');
    Route::get('dashboard/posts/del/{id}','Dashboard\PostController@del_post')->name('del_post');
    Route::get('dashboard/posts/to_approve/{id}','Dashboard\PostController@to_approve')->name('to_approve_post');
    Route::get('dashboard/posts/to_reject/{id}','Dashboard\PostController@to_reject')->name('to_reject_post');

    Route::get('dashboard/posts/comments','Dashboard\PostController@comments')->name('posts_comment');
    Route::post('dashboard/posts/comments/add','Dashboard\PostController@add_comment')->name('add_post_comment');
    Route::post('dashboard/posts/comments/edit','Dashboard\PostController@edit_comment')->name('edit_post_comment');
    Route::get('dashboard/posts/comments/del/{id}','Dashboard\PostController@del_comment')->name('del_post_comment');


    Route::get('dashboard/news/approved','Dashboard\NewsController@approved')->name('approved_news');
    Route::get('dashboard/news/waiting','Dashboard\NewsController@waiting')->name('waiting_news');
    Route::get('dashboard/news/rejected','Dashboard\NewsController@rejected')->name('rejected_news');
    Route::get('dashboard/news/reports','Dashboard\NewsController@reports')->name('reports_news');
    Route::get('dashboard/news/admin','Dashboard\NewsController@admin')->name('admin_news');
    Route::post('dashboard/news/add','Dashboard\NewsController@add_news')->name('add_news');
    Route::post('dashboard/news/edit','Dashboard\NewsController@edit_news')->name('edit_news');
    Route::get('dashboard/news/del/{id}','Dashboard\NewsController@del_news')->name('del_news');
    Route::get('dashboard/news/to_approve/{id}','Dashboard\NewsController@to_approve')->name('to_approve_news');
    Route::get('dashboard/news/to_reject/{id}','Dashboard\NewsController@to_reject')->name('to_reject_news');


    Route::get('dashboard/questions/approved','Dashboard\QuestionController@approved')->name('approved_questions');
    Route::get('dashboard/questions/waiting','Dashboard\QuestionController@waiting')->name('waiting_questions');
    Route::get('dashboard/questions/rejected','Dashboard\QuestionController@rejected')->name('rejected_questions');
    Route::get('dashboard/questions/reports','Dashboard\QuestionController@reports')->name('reports_questions');
    Route::get('dashboard/questions/to_approve/{id}','Dashboard\QuestionController@to_approve')->name('to_approve_question');
    Route::get('dashboard/questions/to_reject/{id}','Dashboard\QuestionController@to_reject')->name('to_reject_question');
    Route::get('dashboard/questions/del/{id}','Dashboard\QuestionController@delete')->name('del_question');

    Route::get('dashboard/questions/comments','Dashboard\QuestionController@comments')->name('questions_comment');
    Route::get('dashboard/questions/comments/del/{id}','Dashboard\QuestionController@del_comment')->name('del_question_comment');

    Route::get('dashboard/subjects/videos','Dashboard\SubjectController@videos')->name('videos');
    Route::post('dashboard/subjects/videos/add','Dashboard\SubjectController@video_add')->name('add_video');
    Route::get('dashboard/subjects/videos/del','Dashboard\SubjectController@video_del')->name('del_video');
    Route::get('dashboard/subjects/sheets','Dashboard\SubjectController@sheets')->name('sheets');
    Route::post('dashboard/subjects/sheets/add','Dashboard\SubjectController@sheet_add')->name('add_sheet');
    Route::get('dashboard/subjects/sheets/del','Dashboard\SubjectController@sheet_del')->name('del_sheet');
    Route::get('dashboard/subjects/summary','Dashboard\SubjectController@summary')->name('summary');
    Route::post('dashboard/subjects/summary/add','Dashboard\SubjectController@summary_add')->name('add_summary');
    Route::get('dashboard/subjects/summary/del','Dashboard\SubjectController@summary_del')->name('del_summary');

    Route::get('dashboard/contact','Dashboard\ContactUsController@index')->name('contact_us');
    Route::get('dashboard/contact/delete/{id}','Dashboard\ContactUsController@delete')->name('del_contact_us');


    //Management System
        //Qr
    Route::get('dashboard/management/qr','Dashboard\QrCodeController@index')->name('qrcode');
    Route::post('dashboard/management/qr/add','Dashboard\QrCodeController@add')->name('add_qr');
    Route::post('dashboard/management/qr/edit','Dashboard\QrCodeController@edit')->name('edit_qr');
    Route::get('dashboard/management/qr/del/{id}','Dashboard\QrCodeController@delete')->name('del_qr');
        //Price Faculty
    Route::get('dashboard/management/faculty/price','Dashboard\FacultyController@price')->name('price_faculty');
    Route::post('dashboard/management/faculty/price/add','Dashboard\FacultyController@add_price')->name('add_faculty_price');
    Route::post('dashboard/management/faculty/price/edit','Dashboard\FacultyController@edit_price')->name('edit_faculty_price');
    Route::get('dashboard/management/faculty/price/del/{id}','Dashboard\FacultyController@delete_price')->name('del_faculty_price');
        //Ads
    Route::get('dashboard/management/ads','Dashboard\AdsController@index')->name('ads');
    Route::post('dashboard/management/ads/add','Dashboard\AdsController@create')->name('add_ad');
    Route::get('dashboard/management/ads/active/{id}','Dashboard\AdsController@active')->name('active_ad');
    Route::post('dashboard/management/ads/edit','Dashboard\AdsController@edit')->name('edit_ad');
    Route::get('dashboard/management/ads/delete/{id}','Dashboard\AdsController@delete')->name('del_ad');
    Route::post('dashboard/management/ads/level/add','Dashboard\AdsController@add_level')->name('add_ad_level');
    Route::get('dashboard/management/ads/level/delete/{id}','Dashboard\AdsController@del_level')->name('del_ad_level');

        //Notifications
    Route::get('dashboard/management/notifications','Dashboard\NotificationController@index')->name('notifications');
    Route::post('dashboard/management/notifications/send','Dashboard\NotificationController@send')->name('send_notification');
        //Places
    Route::get('dashboard/management/places','Dashboard\PlaceController@index')->name('places');
    Route::post('dashboard/management/places/add','Dashboard\PlaceController@add')->name('add_place');
    Route::post('dashboard/management/places/edit','Dashboard\PlaceController@edit')->name('edit_place');
    Route::get('dashboard/management/places/del/{id}','Dashboard\PlaceController@delete')->name('del_place');
    Route::get('dashboard/management/places/active/{id}','Dashboard\PlaceController@active')->name('active_place');

        //Trial
    Route::get('dashboard/management/trials','Dashboard\TrialController@index')->name('trials');
    Route::post('dashboard/management/trials/edit','Dashboard\TrialController@edit')->name('edit_trial');

        //Android App
    Route::get('dashboard/management/android','Dashboard\ManagementController@android')->name('android');
    Route::get('dashboard/management/android/tool/{id}','Dashboard\ManagementController@android_tool')->name('android_tool');

        //Images App
    Route::get('dashboard/management/AppImages','Dashboard\ManagementController@img_app')->name('img_app');
    Route::post('dashboard/management/AppImages/add','Dashboard\ManagementController@add_img_app')->name('add_img_app');
    Route::get('dashboard/management/AppImages/del/{id}','Dashboard\ManagementController@del_img_app')->name('del_img_app');


});

Route::get('/test','HomeController@test');
