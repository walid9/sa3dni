<?php

Route::group(['prefix' => 'app-status'], function(){

    Route::get('universities','Students\AppStatusController@universities');
    Route::post('faculties','Students\AppStatusController@faculties');
    Route::post('departments','Students\AppStatusController@departments');
    Route::post('levels','Students\AppStatusController@levels');
    Route::post('subjects','Students\AppStatusController@subjects');
    Route::post('shared-subjects','Students\AppStatusController@sharedSubjects');
    Route::post('faculty-subjects','Students\AppStatusController@mySubjects');
    Route::post('payment-places','Students\AppStatusController@places');
    Route::post('faculty-cost','Students\AppStatusController@facultyCost');
    Route::post('promo-code','Students\AppStatusController@check_promocode');
    Route::get('subjects-images','Students\AppStatusController@subjects_images');

});





