<?php
Route::group(['prefix' => 'comments'], function(){

    Route::post('index','Students\CommentsController@index');
    Route::post('create','Students\CommentsController@create');
    Route::post('like','Students\CommentsController@like');
    Route::post('dislike','Students\CommentsController@dislike');
    Route::post('report','Students\CommentsController@report');
    Route::post('edit','Students\CommentsController@update');
    Route::post('delete','Students\CommentsController@delete');
});
