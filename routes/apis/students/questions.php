<?php
Route::group(['prefix' => 'questions'], function(){

    Route::get('index','Students\QuestionsController@index');
    Route::post('create','Students\QuestionsController@create');
    Route::post('like','Students\QuestionsController@like');
    Route::post('dislike','Students\QuestionsController@dislike');
    Route::post('report','Students\QuestionsController@report');
    Route::post('edit','Students\QuestionsController@update');
    Route::post('delete','Students\QuestionsController@delete');
});
