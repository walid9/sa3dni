<?php
Route::group(['prefix' => 'posts'], function(){

    Route::get('index','Students\PostsController@index');
    Route::post('create','Students\PostsController@create');
    Route::post('create-multi','Students\PostsController@create_multi');
    Route::post('like','Students\PostsController@like');
    Route::post('dislike','Students\PostsController@dislike');
    Route::post('report','Students\PostsController@report');
    Route::post('edit','Students\PostsController@update');
    Route::post('delete','Students\PostsController@delete');
    Route::post('admin-posts','Students\PostsController@adminPosts');
});
