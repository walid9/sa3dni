<?php
Route::group(['prefix' => 'replies'], function(){

    Route::post('index','Students\CommentsRepliesController@index');
    Route::post('create','Students\CommentsRepliesController@create');
    Route::post('like','Students\CommentsRepliesController@like');
    Route::post('dislike','Students\CommentsRepliesController@dislike');
    Route::post('report','Students\CommentsRepliesController@report');
    Route::post('edit','Students\CommentsRepliesController@update');
    Route::post('delete','Students\CommentsRepliesController@delete');
});
