
<?php

/*
Route::post('update-firebase-token', 'AuthController@updateFirebase');
Route::post('update-language', 'AuthController@updateLang');
*/

Route::post('/auth/register', 'Students\AuthController@register');
Route::post('/auth/login', 'Students\AuthController@login');
Route::post('/auth/logout', 'Students\AuthController@logout')->middleware('jwt.auth');
Route::post('/auth/skip-login', 'Students\AuthController@skipLogin');
Route::post('/auth/forget-password', 'Students\AuthController@forgetPassword');
Route::post('/auth/change-password', 'Students\AuthController@changePassword');
Route::post('/auth/check-login', 'Students\AuthController@checkLogin');
Route::post('/auth/check-phone', 'Students\AuthController@checkphone');
Route::post('/auth/check-username', 'Students\AuthController@checkusername');
Route::post('/auth/update-firebase-token', 'Students\AuthController@updateFirebase');
Route::post('/auth/get-names', 'Students\AuthController@getNames');
Route::post('/auth/promo-code', 'Students\AuthController@verify_promocode');



