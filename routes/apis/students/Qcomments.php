<?php
Route::group(['prefix'=>'Qcomments'],function ()
{
    Route::post('index','Students\QCommentsController@index');
    Route::post('create','Students\QCommentsController@create');
    Route::post('like','Students\QCommentsController@like');
    Route::post('dislike','Students\QCommentsController@dislike');
    Route::post('report','Students\QCommentsController@report');
    Route::post('edit','Students\QCommentsController@update');
    Route::post('delete','Students\QCommentsController@delete');
});
