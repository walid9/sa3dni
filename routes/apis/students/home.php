<?php
Route::group(['prefix' => 'home'], function(){

    Route::post('index','Students\HomeController@index');
    Route::post('search','Students\HomeController@search');
    Route::post('upload-file','Students\HomeController@upload');
});
