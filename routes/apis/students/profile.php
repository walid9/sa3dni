<?php

Route::get('/profile', 'Students\ProfileController@index');
Route::post('/profile/update', 'Students\ProfileController@update');
