<?php

return [
    'success'=>'تم بنجاح',
    'like_exist'=>'عفوا انت قمت بالفعل بالاعجاب ',
    'error_data'=>'خطا فى البيانات',
    'change_password'=>'من فضلك ادخل كلمة مرور غير متطابقة لكلمة المرور السابقة',
    'amount_error'=>'عفوا المبلغ المطلوب اكبر من رصيدك الحالى',
    'wallet_invalid' => 'خطا المبلغ الكلى للمحفظة اقل من تكلفة الحجز',
    'error' => 'حدث خطا ما',
    'no_dates'=>'عفوا تاريخ بداية ونهاية العرض الجديد غير مناسب',
    'invalid_item'=>'عفوا هذا العنصر غير موجود',
    'invalid_code'=>'كود غير صحيح',
    'coupon_invalid'=>'الكود غير صالح للاستخدام',
    'user_exsist' => 'الحساب موجود من قبل لهذا القسم',
    'user_blocked' => 'الحساب معلق',
    'activation_required' => 'يجب تفعيل الحساب اولاً',
    'invalid_credentials' => 'برجاء التأكد من البيانات',
    'code_activated' => 'تم تفعيل الحساب بنجاح',
    'code_expired' => ' مدة تفعيل الكود انتهت',
    'no_data' => 'لا توجد بيانات',
    'email_exist' => 'البريد الالكترونى موجود من قبل',
    'phone_exist' => 'رقم الهاتف موجود من قبل',
    'username_exist'=> 'اسم المستخدم موجود من قبل',
    'device_error'=> 'الحساب الذي تحاول الدخول به غير مفعل علي هذا الجهاز',
    'old_password_invalid'=> 'خطا فى كلمة المرور السابقة',
    'error_email'=> 'خطا فى البريد الالكترونى من فضلك ادخل الصحيح',
    'error_code'=> 'خطا فى كود التفعيل من فضلك ادخل الصحيح',
    'confirm_password' => 'من فضلك ادخل تاكيد كلمة المرور صحيحة',
    'coupon_used' => 'عفوا انت بالفعل تستخدم هذا الكوبون.',
    'completed' => 'عفوا لقد اكتمل العدد المحدد من استخدام هذا الكوبون.',
    'rated' => 'تم تقييم هذا الدكتور من قبل المستخدم من قبل',
    'invalid_reservation' => 'هذا الحجز لم ينتمى للمستخدم ',
    'invalid_phone'=>'خطا فى رقم الهاتف',
    'login'=>'من فضلك قم بتسجيل الدخول اولا',
    'invalid_time'=>'خطا فى اختيار الوقت',
    'time_reserved'=>'عفوا هذا الوقت تم حجزه من قبل مستخدم اخر',
    'time_correct'=>'من فضلك اختر التوقيت بشكل صحيح',
    'date_exist'=>'عفوا هذه المواعيد موجودة بالفعل',
    'not_start'=>'الجلسة لم تبدا بعد',
    'reserve_twice'=>'عفوا لا تسطيع حجز اكتر من دكتور فى نفس الوقت',
];

