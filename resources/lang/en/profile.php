<?php
return [

    'account' => 'Profile',
    'bookings' => 'My Bookings',
    'appoint' => 'Appointments',
    'archive' => 'Archive',
    'clinic' => 'My Clinic',
    'name' => 'Name',
    'email' => 'Email Address',
    'phone' => 'Phone',
    'pass' => 'if you want to update your password , please click here',
    'update' => 'Update Your Password',
    'old' => 'Enter Your Old Password',
    'new' => 'Enter Your New Password',
    'confirm' => 'Confirm New Password',
    'btn' => 'Update Profile',
    'more' => 'More',
    'diag' =>'Diagnose',
    'pres' => 'Prescription' ,
    'med' => 'Medicine' ,
    'dose' => 'Dose' ,
    'frequency' => 'Frequency' ,
    'duration_unit' => 'Duration' ,
    'route' => 'Route' ,
    'order_instructions' => 'order instruction' ,
    'add' => 'Add' ,
    'join' => 'Join' ,
    'fees' =>'Fees',
    'show_files' =>'Show Files',
    'add_files' =>'Add Files',


];
