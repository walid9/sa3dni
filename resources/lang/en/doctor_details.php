<?php
return [

    'about' => 'About Doctor',
    'tags' => 'Tags',
    'opa' => 'Online Payment Available',
    'rate' => 'Overall Rating',
    'pick' => 'Pick a Time',
    'today' => 'Today',
    'tom' => 'Tomorrow',
    'please_login' => 'Please Login To Ask a Question',
    'sorry' => 'sorry no appointments available in this day',
    'info' => 'Info',
    'feedback' => 'Feedback',
    'q' => 'Q&A',
    'patient' => 'Patients’ Reviews',
    'ask' => 'Ask a Question',
    'answer' => 'Answer',
    'fees' =>'Fees',
    'egp' => 'EGP' ,
    'show' => 'Show more' ,
    'bok' => 'Book Now' ,
    'not' => 'Not Answered' ,
    'as' => 'Ask' ,
    'name' => 'Name' ,
    'subscribe_details' => 'Subscription Details' ,

];
