@extends('Student.app')

@section('content')

    <!-- Main statics -->
    <div class="row justify-content-center">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" dir="rtl">
            <div class="container pt-2 position-relative animated">
                    <h2 class="text-center">
                        {{ Auth::user()->full_name }}
                    </h2>
            </div>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /Main statics -->

@endsection
