<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta data-n-head="true" name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>Sa3dni | Dashboard</title>
    <!--===============================================================================================-->
    <link rel="shortcut icon" href="{{ asset('icon.png') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!--===============================================================================================-->
</head>
<body>
<div class="limiter">
    <div class="container-login100 " style="background-image: url({{asset('/images/poster.jpg')}});">
        <div class="wrap-login100 trans-1-0 op-0-9">
            <div class="login100-form-title" style="background-image: url({{asset('/images/backgroundwhite.jpg')}});"> </div>
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                    {{session('error')}} <i class="fa fa-times-circle"></i>
                </div>
            @endif
            <form class="login100-form validate-form" method="POST" action="{{ route('login_student') }}">
                @csrf
                <div class="wrap-input100 m-b-26 " >
                    <input class="input100" type="text"  name="email" placeholder="Enter Email" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100  m-b-18">
                    <input class="input100" type="password" name="password" placeholder="Enter password" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        Login
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
        document.addEventListener("contextmenu", function(e){
            e.preventDefault();
        }, false);
        document.addEventListener("keydown", function(e) {
            // "I" key
            if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
                disabledEvent(e);
            }
            // "J" key
            if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
                disabledEvent(e);
            }
            // "S" key + macOS
            if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                disabledEvent(e);
            }
            // "U" key
            if (e.ctrlKey && e.keyCode == 85) {
                disabledEvent(e);
            }
            // "F12" key
            if (event.keyCode == 123) {
                disabledEvent(e);
            }
        }, false);
        function disabledEvent(e){
            if (e.stopPropagation){
                e.stopPropagation();
            } else if (window.event){
                window.event.cancelBubble = true;
            }
            e.preventDefault();
            return false;
        }
    });
</script>
