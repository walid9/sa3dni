@extends('Student.app')
@section('title')
    Summary
@endsection

@section('content')
    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_file"><i class="nav-icon fas fa-plus"></i> Add Summary </a>
        </div>
        <h2 class="text-center col-md-7"> <i class="nav-icon fas fa-file-pdf"> </i> Summary  </h2>
        <input class="col-md-3 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">

    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-file-pdf"></i> Summary </th>
                    <th width="25%" class="align-middle"><i class="fas fa-file"></i>  Name </th>
                    <th width="25%" class="align-middle"><i class="fas fa-book"></i>  Subject </th>
                    <th width="25%" class="align-middle"><i class="fas fa-info-circle"></i>  F D L </th>
                    <th width="15%" class="align-middle"><i class="fas fa-calendar-day"></i>  Date </th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                    <tr id="data">
                        <td class="align-middle">
                            @if($file->img)
                                <i class="fa fa-file-pdf fa-2x"></i>
                            @else
                                <i class="fa fa-file-pdf text-danger fa-2x"></i>
                            @endif
                        </td>
                        <td class="align-middle">{{$file->name}}</td>
                        <td class="align-middle">{{$file->subject->name}}</td>
                        <td class="align-middle">{{$file->department->faculty->name}}<br>{{$file->department->name}} <br> Level : {{$file->level}}</td>
                        <td class="align-middle">{{$file->created_at}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{--Start Add Modal--}}
    <div class="modal fade" id="add_file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" id="add_file_form" action="{{route('add_summary_student')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add File</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 row" >
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" ><i class="fas fa-building prefix grey-text"></i> Department & Level</label>
                                    <select id="select_department"  name='department' class="form-control validate text-center" required >
                                        <option disabled selected value="" > Choose .. </option>
                                        @foreach($departments as $department)
                                            <option value="{{$department->level->id}}">Department : {{$department->level->department->name}} Level :  {{$department->level->level}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="form-group">
                                    <label data-error="wrong" data-success="right" ><i class="fas fa-book prefix grey-text"></i> Subject </label>
                                    <select id="select_subject"  name='subject' class="form-control validate text-center" required >
                                        <option disabled selected value="" > Choose Subject</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name </label>
                                <input  type="text"  name='name' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-12 col-12 mb-5">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-file-pdf prefix grey-text"></i> Summary </label>
                                <div class="custom-file">
                                    <input type="file" name="file" class="custom-file-input" required>
                                    <label class="custom-file-label" >Choose Summary</label>
                                </div>
                            </div>
                            <div class="col-md-12 col-12">
                                <div class="progress border">
                                    <div class="badge badge-info position-absolute d-none" id="wait" style="bottom: 20px;font-size: 14px"> Please Wait <i class="fa fa-spinner"></i></div>
                                    <div class="progress-bar progress-bar-striped progress-bar-animated text-center" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#add_file_form').on('submit', function(event) {
            $('#wait').removeClass('d-none');
            postFile();
            //$('.progress-bar').attr('aria-valuenow', '100').css('width', '100' + '%').text('Please Wait ... ');
        });
        function postFile() {
            var formdata = new FormData();

            formdata.append('file1', $('.custom-file-input')[0].files[0]);

            var request = new XMLHttpRequest();

            request.upload.addEventListener('progress', function (e) {
                var file1Size = $('.custom-file-input')[0].files[0].size;
                if (e.loaded <= file1Size) {
                    var percent = Math.round(e.loaded / file1Size * 100);
                    $('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent+" %");
                }

                if(e.loaded == e.total){
                    $('.progress-bar').attr('aria-valuenow', 100).css('width', 100 + '%').text("Please Wait ....");
                }

            });

            request.open('post', '/echo/html/');
            //request.timeout = 4500000;
            request.send(formdata);
        }

        $('#select_department').on('change',function () {
            var _token = $('input[name="_token"]').val();
            $('#level_department').val(this.value);
            $.ajax({
                url:"{{ route('get_subject_student') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name_en;
                            var option=  "<option class='select_subject' value='"+id+"'>"+name+"</option>";
                            $("#select_subject").append(option);
                        }
                    }
                },
            });

        });

        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
