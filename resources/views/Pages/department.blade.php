@extends('layouts.app')
@section('title')
    Department
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-building"> </i> Departments  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
    </div>

    <section class="content">
        <div class="row">
            <div class="col-12" id="accordion">
                @foreach($faculties as $faculty)
                <div class="card card-primary card-outline">
                    <a class="d-block w-100 text-center" data-toggle="collapse" href="#collapse{{$faculty->id}}">
                        <div class="card-header">
                            <h4 class="card-title w-100">
                                {{$faculty->university->name}} <i class="fa fa-arrow-left"></i> {{$faculty->name}}
                            </h4>
                        </div>
                    </a>
                    <div id="collapse{{$faculty->id}}" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <a  class="btn btn-success w-100 m-2 add_department @if(!\Auth::user()->role) disabled @endif" data-toggle="modal" data-target="#add_department" data-faculty="{{$faculty->id}}"><i class="nav-icon fas fa-plus"></i> Add Department </a>
                            <div class="row">
                                <!-- /.col -->
                                @foreach($faculty->departments as $department)
                                <div class="col-md-4 col-sm-6 col-12">
                                    <div class="info-box">
                                        <span class="info-box-icon">
                                             <a  class="btn btn-success add_level @if(!\Auth::user()->role) disabled @endif" data-toggle="modal" data-target="#add_level" data-department="{{$department->id}}"><i class="nav-icon fas fa-plus"></i> Level </a>
                                        </span>
                                        <div class="info-box-content text-center">
                                            <span class="info-box-text">{{$department->name}}</span>
                                            <span class="info-box-more">
                                                <a class="btn btn-primary edit_department @if(!\Auth::user()->role) disabled @endif" data-target="#edit_department" data-id="{{$department->id}}"  data-name="{{$department->name}}" data-faculty="{{$faculty->id}}" data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                                <a  data-toggle="modal" data-target="#del_department" class="btn btn-danger del_department @if(!\Auth::user()->role) disabled @endif" data-id="{{$department->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </span>
                                            <span class="info-box-number">
                                                @foreach($levels as $level)
                                                    @if($level->department_id==$department->id)
                                                        <a data-toggle="modal" data-target="#del_level" class="btn btn-danger del_level @if(!\Auth::user()->role) disabled @endif" data-id="{{$level->id}}"> {{$level->level}} </a>
                                                    @endif
                                                 @endforeach
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                @endforeach
                                <!-- /.col -->
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_department" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_add_department" role="form" method="POST" action="{{route('add_department')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Department </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name </label>
                            <input  type="text"  name='name' class="form-control validate text-center" required>
                            <input type="hidden" name="faculty_id" id="faculty_id_add" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_department" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_department" role="form" method="POST" action="" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Department</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i>  Name </label>
                            <input type="text"  name='name' id='name_department' class="form-control validate text-center" required>
                            <input type="hidden" name="faculty_id" id="faculty_id_edit">
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_department" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Department </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_department" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_level" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_add_level" role="form" method="POST" action="{{route('add_level')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Level </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Level </label>
                            <input  type="text"  name='level' class="form-control validate text-center" required>
                            <input type="hidden" name="department_id" id="department_id" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_level" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Level </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_level" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}


@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click','.add_department',function () {
        $('#faculty_id_add').val($(this).data('faculty'));
        $('#add_department').modal('show');
    });
    $(document).on('click', '.edit_department', function() {
        $('#name_department').val($(this).data('name'));
        $('#faculty_id_edit').val($(this).data('faculty'));
        var id=$(this).data('id');
        var action="{{route('edit_department',"id")}}";
        action=action.replace('id',id);
        $('#form_edit_department').attr('action',action);
        $('#edit_department').modal('show');
    });
    $(document).on('click','.del_department',function () {
        var id=$(this).data('id');
        var action="{{route('del_department',"id")}}";
        action=action.replace('id',id);
        $('#form_del_department').attr('action',action);
        $('#del_department').modal('show');
    });

    $(document).on('click','.add_level',function () {
        $('#department_id').val($(this).data('department'));
        $('#add_level').modal('show');
    });

    $(document).on('click','.del_level',function () {
        var id=$(this).data('id');
        var action="{{route('del_level',"id")}}";
        action=action.replace('id',id);
        $('#form_del_level').attr('action',action);
        $('#del_level').modal('show');
    });

    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".card").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



