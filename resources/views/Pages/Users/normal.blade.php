@extends('layouts.app')
@section('title')
    Normal User
@endsection

@section('content')
    <section class="content" style="width: 99%">
    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="col-md-2 text-center border-right"> <i class="nav-icon fas fa-users"> </i> Users  </h2>
        <div class="col-md-4 row">
            <div class="btn-group col-md-5">
                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" >Faculty
                    <span class="caret"></span></button>
                <ul class="dropdown-menu faculty text-center m-5" >
                    <input class="form-control " id="search_faculty" type="search" placeholder="Search..">
                    @foreach($faculties as $faculty)
                        <li class="select_faculty" value="{{$faculty->id}}"><a href="#" class="dropdown-item"> {{$faculty->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <i class="col-md-1 m-1 fa fa-sync-alt text-center"></i>
            <div class="btn-group col-md-5 border-right">
                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" >Department
                    <span class="caret"></span></button>
                <ul class="dropdown-menu department text-center" >
                    <input class="form-control " id="search_department" type="search" placeholder="Search..">

                </ul>
            </div>
        </div>
        <div class="col-md-3 row mt-3 mb-3 text-center">
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-user-check" value="fa-user-check" name="radio">
                <label class="form-check-label" for="inlineRadio1"><i class="fa fa-user-check"></i></label>
            </div>
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-battery-full" value="fa-battery-full" name="radio">
                <label class="form-check-label" for="inlineRadio2"><i class="fas fa-battery-full text-success"></i></label>
            </div>
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-battery-empty" value="fa-battery-empty" name="radio">
                <label class="form-check-label" for="inlineRadio3"><i class="fas fa-battery-empty text-danger"></i> </label>
            </div>
        </div>
        <input class="col-md-3 form-control" type="search" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('success'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('success')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
                <div class="col-12">
                @foreach($users->groupBy('department_id') as  $group)
                            <div class=" main_search card card-primary card-outline">
                                <a class="d-block w-100 text-center" data-toggle="collapse" href="#collapse{{$group[0]->department_id}}">
                                    <div class="card-header">
                                        <h4 class="card-title w-100">
                                            {{$group[0]->university->name}} <i class="fa fa-arrow-left"></i> {{$group[0]->faculty->name}} <i class="fa fa-arrow-left"></i> {{$group[0]->department->name}}
                                        </h4>
                                    </div>
                                </a>
                                <div id="collapse{{$group[0]->department_id}}" class="collapse">
                                    <div class="card-body">
                                        <div class="row">
                                    @foreach($group as $user)
                                                <div class="user_search col-lg-4 col-md-12 col-12">
                                                    <div class="card card-widget widget-user-2">
                                                        <div class="widget-user-header bg-gray">
                                                            <div class="widget-user-image">
                                                                <img class="img-circle elevation-2" style="width: 70px;height: 70px" @if($user->img) src="{{asset($user->img)}}" @else @if($user->gender=='Female') src="{{asset('images/user_female.png')}}" @else src="{{asset('images/user_male.png')}}" @endif @endif>
                                                            </div>
                                                            @if($user->status)
                                                                <i class="fas fa-battery-full fa-1x text-success" style="float: right"></i>
                                                                <span class="d-none">fa-battery-full</span>
                                                            @else
                                                                <i class="fas fa-battery-empty fa-1x text-danger" style="float: right"></i>
                                                                <span class="d-none">fa-battery-empty</span>
                                                            @endif
                                                            <h3 class="widget-user-username" >{{$user->full_name}} @if($user->verified) <i class="fa fa-user-check"></i> <span class="d-none">fa-user-check</span>  @endif</h3>
                                                            <a href="#" class="profile_modal" data-toggle="modal" data-target="#OpenProfileModal"
                                                               data-id="{{$user->id}}" data-fullname="{{$user->full_name}}" data-username="{{$user->user_name}}" data-email="{{$user->email}}" data-phone="{{$user->phone}}"
                                                               data-type="{{$user->type}}" data-university="{{$user->university->name}}" data-faculty="{{$user->faculty->name}}" data-promo="{{$user->promo_code[0]->promo_code}}" data-promo_status="{{$user->promo_code[0]->status}}"
                                                               data-department="{{$user->department->name}}"  data-status="{{$user->status}}"  data-gender="{{$user->gender}}" data-device="{{$user->device_id}}" data-service="{{$user->balance[0]->service}}" data-cache="{{$user->balance[0]->cache}}"
                                                               @if($user->img) data-image="{{$user->img}}" @else @if($user->gender=='Female') data-image="{{asset('images/user_female.png')}}" @else data-image="{{asset('images/user_male.png')}}" @endif  @endif
                                                               data-deadline="{{$user->deadline}}" data-verified="{{$user->verified}}" data-rank="{{$user->rank}}" data-levels="{{$user->levels}}"
                                                               data-post="{{$user->post}}" data-news="{{$user->news}}" data-question="{{$user->question}}" data-comment="{{$user->comment}}"
                                                            >
                                                                <h5 class="widget-user-desc"><i class="fa fa-at"></i> {{$user->user_name}}</h5>
                                                            </a>
                                                            <h5 class="widget-user-desc"><i class="fa fa-mobile-alt"></i> {{$user->phone}}</h5>
                                                            <span class="widget-user-desc text-center" style="margin-left: 5px"><i class="fa fa-envelope"></i> {{$user->email}}</span>
                                                        </div>
                                                        <div class="card-footer p-0">
                                                            <div class="row p-2">
                                                                <div class="col-md-8 col-8 text-center">
                                                                    <a class="btn btn-success" href="{{route('to_excellent',$user->id)}}"> <i class="fas fa-user-check"></i> To Excellent Student </a>
                                                                </div>
                                                                <div class="col-md-4 col-4 text-center">
                                                                    <a  class="btn btn-info change_department" data-toggle="modal" data-target="#change_department" data-user_id="{{$user->id}}" data-faculty_id="{{$user->faculty_id}}" data-department_id="{{$user->department_id}}" data-level="{{$user->levels}}"><i class="nav-icon fas fa-edit"></i> Edit </a>
                                                                </div>
                                                            </div>
                                                            <ul class="nav flex-column">
                                                                <div class="row">
                                                                    <li class="col-6 nav-item">
                                                                        <a class="nav-link">
                                                                            Post <i class="fa fa-chalkboard-teacher"></i>
                                                                            <span class="float-right badge bg-primary">{{$user->post}}</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="col-6 nav-item">
                                                                        <a  class="nav-link">
                                                                            News <i class="fa fa-newspaper"></i>
                                                                            <span class="float-right badge bg-info">{{$user->news}}</span>
                                                                        </a>
                                                                    </li>
                                                                </div>
                                                                <div class="row">
                                                                    <li class="col-6 nav-item">
                                                                        <a  class="nav-link">
                                                                            Question <i class="fa fa-question"></i>
                                                                            <span class="float-right badge bg-success">{{$user->question}}</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="col-6 nav-item">
                                                                        <a  class="nav-link">
                                                                            Comment <i class="fa fa-comment"></i>
                                                                            <span class="float-right badge bg-danger">{{$user->comment}}</span>
                                                                        </a>
                                                                    </li>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                    @endforeach
                                        </div>
                                    </div>
                                </div>
                        </div>
                    @endforeach
                </div>
    </div>
    </section>
    {{--Start Edit Modal--}}
    <div class="modal fade" id="change_department" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('change_department')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold"> Change Department&Level </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" class="form-control" id="user_id_change" name="user_id" required >
                        <div class="md-form mb-2 form-group row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-building prefix grey-text"></i> Department </label>
                                <select name='department_id' id="department_change" class="form-control validate text-center department_change" required>
                                    <option disabled selected value=""> Choose Department</option>

                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-id-badge prefix grey-text"></i> Level </label>
                                <select name='level' id="level_change" class="form-control validate text-center level_change" required>
                                    <option disabled selected value=""> Choose Level</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Save <i class="fas fa-save ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}
@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>

<script>
    $faculty_text="";
    $(document).on('click', '.change_department', function() {
        $('#user_id_change').val($(this).data('user_id'));
        $department_id=$(this).data('department_id');
        $level=$(this).data('level');
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('get_department') }}",
            method:"GET",
            data:{id:$(this).data('faculty_id'), _token:_token},
            success: function(response){
                $('#department_change').find('option').not(':first').remove();
                var len = 0;
                if(response['data'] != null){
                    len = response['data'].length;
                }
                if(len > 0){
                    // Read data and create <option >
                    for(var i=0; i<len; i++){
                        var id = response['data'][i].id;
                        var name = response['data'][i].name;
                        var option = "<option  value='" + id + "'>" + name + "</option>";
                        $("#department_change").append(option);
                    }
                }

            },
        });
        //get_level($department_id);
        $('#change_department').modal('show');
    });

    function get_level($department_id){
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('get_level') }}",
            method:"GET",
            data:{id:$department_id, _token:_token},
            success: function(response){
                var len = 0;
                if(response['data'] != null){
                    len = response['data'].length;
                }
                $('#level_change').find('option').not(':first').remove();
                if(len > 0){
                    // Read data and create <option >
                    for(var i=0; i<len; i++){
                        var id = response['data'][i].id;
                        var name = response['data'][i].level;
                        var option=  "<option value='"+name+"'>"+name+"</option>";
                        $("#level_change").append(option);
                    }
                }
            },
        });
    }
    $(document).ready(function(){
        // Faculty Change
        $('.select_faculty').click(function(){
            // Faculty id
            var id = $(this).val();
            var _token = $('input[name="_token"]').val();
            var value = $(this).text();
            $faculty_text=value;
            $(".main_search").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            // Empty the dropdown
            $('.department').find('li').remove();

            // AJAX request
            $.ajax({
                url:"{{ route('get_department') }}",
                method:"GET",
                data:{id:id, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){

                            var id = response['data'][i].id;
                            var name = response['data'][i].name;

                            var li=  "<li class='select_department' value='"+id+"'><a href='#' class='dropdown-item'>"+name+"</a></li>";

                            $(".department").append(li);
                        }
                    }

                },
            });
        });

        //Search Faculty
        $("#search_faculty").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".faculty li").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        //Search Department
        $("#search_department").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".department li").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        //Select Department
        $('.department').on('click', '.select_department',function(){
            var value =$(this).text();
            $(".main_search").filter(function() {
                $(this).toggle($(this).text().indexOf(value) > -1)
            });
        });

        //Get Level Change
        $('#department_change').on('change',function(){
            get_level(this.value);
        });

        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".user_search").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $(".fa-sync-alt").on("click", function() {
            $(".main_search").filter(function() {
                var value="";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $(".user_search").filter(function() {
                var value="";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
            $('#fa-user-check').prop( "checked", false );
            $('#fa-battery-full').prop( "checked", false );
            $('#fa-battery-empty').prop( "checked", false );
        });
        $("#fa-user-check").on("click", function() {
            $(".user_search").filter(function() {
                var value="fa-user-check";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $("#fa-battery-full").on("click", function() {
            $(".user_search").filter(function() {
                var value="fa-battery-full";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $("#fa-battery-empty").on("click", function() {
            $(".user_search").filter(function() {
                var value="fa-battery-empty";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
    });
</script>



