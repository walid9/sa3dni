@extends('layouts.app')
@section('title')
    Balance User
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-8"> <i class="nav-icon fas fa-users"> </i> Balance User  </h2>
        <input class="col-md-4 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('success'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('success')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        @if(session('failed'))
                <div class="alert alert-default-danger alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                    {{session('failed')}} <i class="fa fa-times-circle"></i>
                </div>
            @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="20%" class="align-middle"><i class="fas fa-user-tag"></i> Data </th>
                    <th width="10%" class="align-middle"><i class="fas fa-university"></i> University </th>
                    <th width="15%" class="align-middle"><i class="fas fa-school"></i> Faculty </th>
                    <th width="25%" class="align-middle"><i class="fas fa-building"></i> Department Level </th>
                    <th width="10%" class="align-middle"><i class="fas fa-money-bill-alt"></i> Cache </th>
                    <th width="10%" class="align-middle"><i class="fas fa-money-check-alt"></i> Service </th>
                    <th width="10%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="data">
                        <td class="align-middle">
                            <a href="#" class="profile_modal" data-toggle="modal" data-target="#OpenProfileModal"
                               data-id="{{$user->id}}" data-fullname="{{$user->full_name}}" data-username="{{$user->user_name}}" data-email="{{$user->email}}" data-phone="{{$user->phone}}"
                               data-type="{{$user->type}}" data-university="{{$user->university->name}}" data-faculty="{{$user->faculty->name}}" data-promo="{{$user->promo_code[0]->promo_code}}" data-promo_status="{{$user->promo_code[0]->status}}"
                               data-department="{{$user->department->name}}"  data-status="{{$user->status}}"  data-gender="{{$user->gender}}" data-device="{{$user->device_id}}" data-service="{{$user->balance[0]->service}}" data-cache="{{$user->balance[0]->cache}}"
                               @if($user->img) data-image="{{$user->img}}" @else @if($user->gender=='Female') data-image="{{asset('images/user_female.png')}}" @else data-image="{{asset('images/user_male.png')}}" @endif  @endif
                               data-deadline="{{$user->deadline}}" data-verified="{{$user->verified}}" data-rank="{{$user->rank}}" data-levels="{{$user->levels}}"
                               data-post="{{$user->post}}" data-news="{{$user->news}}" data-question="{{$user->question}}" data-comment="{{$user->comment}}"
                            >
                            {{$user->user_name}} <br>
                                {{$user->phone}} <br>
                                {{$user->email}}
                            </a>
                        </td>
                        <td class="align-middle">{{$user->university->name}}</td>
                        <td class="align-middle">{{$user->faculty->name}}</td>
                        <td class="align-middle">
                            @foreach($user->userlevel as $level)
                                <a  class="btn btn-info m-2" > {{$level->level->department->name}} Level : {{$level->level->level}} </a>
                            @endforeach
                        </td>
                        <td class="align-middle"><input class="form-control validate text-center" type="number" value="{{$user->balance[0]->cache}}" readonly></td>
                        <td class="align-middle"><input class="form-control validate text-center" type="number" value="{{$user->balance[0]->service}}" readonly></td>
                        <td class="align-middle">
                            <a class="btn btn-success m-2 disabled"> <i class="fas fa-save"></i> Save </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>

</script>



