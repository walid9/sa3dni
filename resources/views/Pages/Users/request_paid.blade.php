@extends('layouts.app')
@section('title')
    User Request Payment
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-warning" href="{{route('check_deadline')}}"><i class="nav-icon fas fa-calendar-day"></i> Check Deadline </a>
        </div>
        <h2 class="text-center col-md-7"> <i class="nav-icon fas fa-users"> </i> User Request Payment  </h2>
        <input class="col-md-3 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('success'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('success')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="30%" class="align-middle"><i class="fas fa-user"></i> Full Name </th>
                    <th width="20%" class="align-middle"><i class="fas fa-user-tag"></i> Username </th>
                    <th width="30%" class="align-middle"><i class="fas fa-receipt"></i> Transaction </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr id="data">
                        <td class="align-middle">{{$transaction->user->full_name}}</td>
                        <td class="align-middle">
                            {{$transaction->user->user_name}}
                        </td>
                        <td class="align-middle">{{$transaction->transaction_number}}</td>
                        <td>
                            <a  data-toggle="modal" data-target="#approve_request" class="btn btn-info approve_request m-1"
                                data-id="{{$transaction->id}}" data-full_name="{{$transaction->user->full_name}}"
                                data-phone="{{$transaction->user->phone}}" data-transaction="{{$transaction->transaction_number}}"
                                data-university="{{$transaction->user->university->name}}" data-faculty="{{$transaction->user->faculty->name}}">
                                <i class="fas fa-check-circle"></i> Approve </a>
                            <a data-toggle="modal" data-target="#del_request" class="btn btn-danger del_request m-1" data-id="{{$transaction->id}}"> <i class="fa fa-minus-circle"></i> </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    {{--Start Approve Modal--}}
    <div class="modal fade" id="approve_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="approve_request" role="form" method="POST" action="{{route('approve_request')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Approve Request</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3 row">
                        <input type="hidden" readonly name="id" id='id' class="form-control validate text-center" required>
                        <div class="md-form mb-2 col-md-6 col-6" >
                            <label for="full_name" data-error="wrong" data-success="right" ><i class="fas fa-user prefix grey-text"></i>  Full Name </label>
                            <input type="text" readonly id='full_name' class="form-control validate text-center" required>
                        </div>
                        <div class="md-form mb-2 col-md-6 col-6" >
                            <label for="phone" data-error="wrong" data-success="right" ><i class="fas fa-mobile-alt prefix grey-text"></i>  Phone </label>
                            <input type="text" readonly  id='phone' class="form-control validate text-center" required>
                        </div>
                        <div class="md-form mb-2 col-md-6 col-6" >
                            <label for="university" data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i>  University </label>
                            <input type="text" readonly  id='university' class="form-control validate text-center" required>
                        </div>
                        <div class="md-form mb-2 col-md-6 col-6" >
                            <label for="faculty" data-error="wrong" data-success="right" ><i class="fas fa-school prefix grey-text"></i>  Faculty </label>
                            <input type="text" readonly  id='faculty' class="form-control validate text-center" required>
                        </div>
                        <div class="md-form mb-2 col-md-8 col-8" >
                            <label for="transaction" data-error="wrong" data-success="right" ><i class="fas fa-receipt prefix grey-text"></i>  Transaction Number </label>
                            <input type="text" readonly  id='transaction' class="form-control validate text-center" required>
                        </div>
                        <div class="md-form mb-2 col-md-4 col-4" >
                            <label for="count" data-error="wrong" data-success="right" ><i class="fas fa-calendar-alt prefix grey-text"></i>  Month Count </label>
                            <input type="number" value="1" min="1" name='count' id='count' class="form-control validate text-center" required>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Approve <i class="fas fa-check-circle ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Approve Modal--}}
    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_request" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Request </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_request" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.approve_request', function() {
        $('#id').val($(this).data('id'));
        $('#full_name').val($(this).data('full_name'));
        $('#phone').val($(this).data('phone'));
        $('#transaction').val($(this).data('transaction'));
        $('#university').val($(this).data('university'));
        $('#faculty').val($(this).data('faculty'));
        $('#approve_request').modal('show');
    });
    $(document).on('click','.del_request',function () {
        var id=$(this).data('id');
        var action="{{route('del_request',"id")}}";
        action=action.replace('id',id);
        $('#form_del_request').attr('action',action);
        $('#del_request').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



