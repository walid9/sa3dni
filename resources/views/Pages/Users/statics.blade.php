@extends('layouts.app')
@section('title')
    Statics
@endsection
@section('content')
    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fas fa-percent"> </i> Statics  </h2>
    </div>
    <!-- Main statics -->
    <div class="justify-content-center p-2 m-2">
        <section class="content">
            <div class="row">
                <div class="col-12" id="accordion">
                    @foreach($faculties as $faculty)
                        <span class="d-none">{{$count_faculty=0}}</span>
                        <div class="card card-primary card-outline">
                            <a class="d-block w-100 text-center" data-toggle="collapse" href="#collapse{{$faculty->id}}">
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        {{$faculty->university->name}} <i class="fa fa-arrow-left"></i> {{$faculty->name}}
                                    </h4>
                                </div>
                            </a>
                            <div id="collapse{{$faculty->id}}" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        @foreach($faculty->departments as $department)
                                            <span class="d-none">{{$count_department=0}}</span>
                                        <div class="col-md-3 col-12">
                                            <div class="card card-primary collapsed-card">
                                                <div class="card-header">
                                                    <h3 class="card-title">{{$department->name}}</h3>
                                                    <div class="card-tools">
                                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                                                    </div>
                                                    <!-- /.card-tools -->
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    @foreach($levels as $level)
                                                        <ul class="nav flex-column">
                                                            @if($level->department_id==$department->id)
                                                            <li class="nav-item">
                                                                    <a class="nav-link">
                                                                        <i class="fa fa-id-card"></i> {{$level->level}}
                                                                        <span class="float-right badge bg-info"> {{$level->count}} <i class="fa fa-users"></i></span>
                                                                        <span class="d-none">{{$count_department+=$level->count}}</span>
                                                                    </a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- /.card-body -->
                                                <div class="card-footer text-center">
                                                    <span class="btn btn-outline-success"> {{$count_department}} <i class="fa fa-users"></i></span>
                                                    <span class="d-none">{{$count_faculty+=$count_department}}</span>
                                                </div>
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <span class="btn btn-outline-success"> {{$count_faculty}} <i class="fa fa-users"></i></span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

    </div>
    <!-- /Main statics -->

@endsection

