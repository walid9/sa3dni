@extends('layouts.app')
@section('title')
    Users
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-6"> <i class="nav-icon fas fa-users"> </i> Users  </h2>
        <div class="col-md-3 row mt-3 mb-3 text-center">
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-users" value="fa-users" name="radio">
                <label class="form-check-label" for="inlineRadio1"><i class="fa fa-users"></i></label>
            </div>
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-user-lock" value="fa-user-lock" name="radio">
                <label class="form-check-label" for="inlineRadio2"><i class="fas fa-user-lock"></i></label>
            </div>
            <div class="form-check form-check-inline col-md-3 col-3">
                <input class="form-check-input" type="radio"  id="fa-battery-empty" value="fa-battery-empty" name="radio">
                <label class="form-check-label" for="inlineRadio3"><i class="fas fa-battery-empty text-danger"></i></label>
            </div>
        </div>
        <input class="col-md-3 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('success'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('success')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        @if(session('failed'))
                <div class="alert alert-default-danger alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                    {{session('failed')}} <i class="fa fa-times-circle"></i>
                </div>
            @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="20%" class="align-middle"><i class="fas fa-user-tag"></i> Data </th>
                    <th width="10%" class="align-middle"><i class="fas fa-university"></i> University </th>
                    <th width="20%" class="align-middle"><i class="fas fa-school"></i> Faculty </th>
                    <th width="35%" class="align-middle"><i class="fas fa-building"></i> Department Level </th>
                    <th width="15%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="data">
                        <td class="align-middle">
                            <a href="#" class="profile_modal" data-toggle="modal" data-target="#OpenProfileModal"
                               data-id="{{$user->id}}" data-fullname="{{$user->full_name}}" data-username="{{$user->user_name}}" data-email="{{$user->email}}" data-phone="{{$user->phone}}"
                               data-type="{{$user->type}}" data-university="{{$user->university->name}}" data-faculty="{{$user->faculty->name}}" data-promo="{{$user->promo_code[0]->promo_code}}" data-promo_status="{{$user->promo_code[0]->status}}"
                               data-department="{{$user->department->name}}"  data-status="{{$user->status}}"  data-gender="{{$user->gender}}" data-device="{{$user->device_id}}" data-service="{{$user->balance[0]->service}}" data-cache="{{$user->balance[0]->cache}}"
                               @if($user->img) data-image="{{$user->img}}" @else @if($user->gender=='Female') data-image="{{asset('images/user_female.png')}}" @else data-image="{{asset('images/user_male.png')}}" @endif  @endif
                               data-deadline="{{$user->deadline}}" data-verified="{{$user->verified}}" data-rank="{{$user->rank}}" data-levels="{{$user->levels}}"
                               data-post="{{$user->post}}" data-news="{{$user->news}}" data-question="{{$user->question}}" data-comment="{{$user->comment}}"
                            >
                            {{$user->user_name}} <br>
                                {{$user->phone}} <br>
                                {{$user->email}}
                            </a>
                        </td>
                        <td class="align-middle">{{$user->university->name}}</td>
                        <td class="align-middle">{{$user->faculty->name}}</td>
                        <td class="align-middle">
                            @foreach($user->userlevel as $level)
                                <a  class="btn btn-info m-2" > {{$level->level->department->name}} Level : {{$level->level->level}} </a>
                            @endforeach
                        </td>
                        <td class="align-middle">
                            <a data-toggle="modal" data-target="#del_user" class="btn btn-danger del_user m-2 @if(!\Auth::user()->role) disabled @endif" data-id="{{$user->id}}"> <i class="fa fa-minus-circle"></i> Delete </a> <br>
                            @if($user->email_verified_at==null)
                                <i class="fa fa-user-lock m-2"></i>
                                <span class="d-none">fa-user-lock</span>
                            @endif
                            @if($user->status==0)
                                <i class="fa fa-battery-empty m-2 text-danger"></i>
                                <span class="d-none">fa-battery-empty</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete User </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_user" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}
@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>

    $(document).on('click','.del_user',function () {
        var id=$(this).data('id');
        var action="{{route('del_user',"id")}}";
        action=action.replace('id',id);
        $('#form_del_user').attr('action',action);
        $('#del_user').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        $("#fa-users").on("click", function() {
            $(".table #data").filter(function() {
                var value="";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $("#fa-user-lock").on("click", function() {
            $(".table #data").filter(function() {
                var value="fa-user-lock";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        $("#fa-battery-empty").on("click", function() {
            $(".table #data").filter(function() {
                var value="fa-battery-empty";
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
    });
</script>



