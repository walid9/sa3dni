@extends('layouts.app')
@section('title')
    Excellent User
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-8"> <i class="nav-icon fas fa-users"> </i> Excellent User  </h2>
        <input class="col-md-4 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('success'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('success')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        @if(session('failed'))
                <div class="alert alert-default-danger alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                    {{session('failed')}} <i class="fa fa-times-circle"></i>
                </div>
            @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="15%" class="align-middle"><i class="fas fa-user-tag"></i> Data </th>
                    <th width="10%" class="align-middle"><i class="fas fa-university"></i> University </th>
                    <th width="15%" class="align-middle"><i class="fas fa-school"></i> Faculty </th>
                    <th width="30%" class="align-middle"><i class="fas fa-building"></i> Department Level </th>
                    <th width="10%" class="align-middle"><i class="fas fa-file"></i> Files </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr id="data">
                        <td class="align-middle">
                            <a href="#" class="profile_modal" data-toggle="modal" data-target="#OpenProfileModal"
                               data-id="{{$user->id}}" data-fullname="{{$user->full_name}}" data-username="{{$user->user_name}}" data-email="{{$user->email}}" data-phone="{{$user->phone}}"
                               data-type="{{$user->type}}" data-university="{{$user->university->name}}" data-faculty="{{$user->faculty->name}}" data-promo="{{$user->promo_code[0]->promo_code}}" data-promo_status="{{$user->promo_code[0]->status}}"
                               data-department="{{$user->department->name}}"  data-status="{{$user->status}}"  data-gender="{{$user->gender}}" data-device="{{$user->device_id}}" data-service="{{$user->balance[0]->service}}" data-cache="{{$user->balance[0]->cache}}"
                               @if($user->img) data-image="{{$user->img}}" @else @if($user->gender=='Female') data-image="{{asset('images/user_female.png')}}" @else data-image="{{asset('images/user_male.png')}}" @endif  @endif
                               data-deadline="{{$user->deadline}}" data-verified="{{$user->verified}}" data-rank="{{$user->rank}}" data-levels="{{$user->levels}}"
                               data-post="{{$user->post}}" data-news="{{$user->news}}" data-question="{{$user->question}}" data-comment="{{$user->comment}}"
                            >
                            {{$user->user_name}} <br>
                                {{$user->phone}} <br>
                                {{$user->email}}
                            </a>
                        </td>
                        <td class="align-middle">{{$user->university->name}}</td>
                        <td class="align-middle">{{$user->faculty->name}}</td>
                        <td class="align-middle">
                            @foreach($user->userlevel as $level)
                                <a data-toggle="modal" data-target="#del_level" class="btn btn-danger del_level m-2" data-id="{{$level->id}}"> {{$level->level->department->name}} Level : {{$level->level->level}} </a>
                            @endforeach
                        </td>
                        <td class="align-middle">
                            <i class="fa fa-file-video"></i> {{$user->videos}} <br>
                            <i class="fa fa-file-pdf"></i> {{$user->pdf}}
                        </td>
                        <td class="align-middle">
                            <a class="btn btn-warning m-2" href="{{route('to_normal',$user->id)}}"> <i class="fas fa-user-alt"></i> To Normal Student </a>
                            <a data-toggle="modal" data-target="#add_level" class="btn btn-info add_level m-2" data-user_id="{{$user->id}}" data-faculty_id="{{$user->faculty_id}}"><i class="fas fa-plus-circle"></i> Add Level </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_level" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_add_level" role="form" method="POST" action="{{route('add_level_user')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Level </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="user_id" id="user_id" required>
                        <div class="md-form mb-2 row">
                            <div class="col-md-6">
                                <label>Department</label>
                                <select id="select_department"  name='department' class="form-control validate text-center" required>
                                    <option disabled selected value="" > Choose Department</option>

                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Level</label>
                                <select id="select_level"  name='level' class="form-control validate text-center" required>
                                    <option disabled selected value="" > Choose Level</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_level" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Level </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_level" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click','.add_level',function () {
        $('#user_id').val($(this).data('user_id'));
        get_department($(this).data('faculty_id'));
        $('#add_level').modal('show');
    });
    function get_department($faculty_id){
        var _token = $('input[name="_token"]').val();
        // Empty the dropdown
        $('#select_department').find('option').not(':first').remove();
        $.ajax({
            url:"{{ route('get_department') }}",
            method:"GET",
            data:{id:$faculty_id, _token:_token},
            success: function(response){
                var len = 0;
                if(response['data'] != null){
                    len = response['data'].length;
                }
                if(len > 0){
                    // Read data and create <option >
                    for(var i=0; i<len; i++){
                        var id = response['data'][i].id;
                        var name = response['data'][i].name;
                        var option=  "<option class='select_department' value='"+id+"'>"+name+"</option>";
                        $("#select_department").append(option);
                    }
                }
            },
        });
    };

    $(document).on('click','.del_level',function () {
        var id=$(this).data('id');
        var action="{{route('del_level_user',"id")}}";
        action=action.replace('id',id);
        $('#form_del_level').attr('action',action);
        $('#del_level').modal('show');
    });
    $(document).ready(function(){
        $('#select_department').on('change',function(){
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_level').find('option').not(':first').remove();

            $.ajax({
                url:"{{ route('get_level') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].level;
                            var option=  "<option class='select_level' value='"+name+"'>"+name+"</option>";
                            $("#select_level").append(option);
                        }
                    }
                },
            });

        });

        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



