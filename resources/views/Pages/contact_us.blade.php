@extends('layouts.app')
@section('title')
    Contact Us
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-info-circle"> </i> Contact Us  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                    <th width="20%" class="align-middle"><i class="fas fa-file"></i> Name </th>
                    <th width="10%" class="align-middle"><i class="fas fa-phone-alt"></i> Phone </th>
                    <th width="30%" class="align-middle"><i class="fas fa-newspaper"></i> Message </th>
                    <th width="10%" class="align-middle"><i class="fas fa-calendar-day"></i> Date </th>
                    <th width="10%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($contacts as $key=>$contact)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$contact->name}}</td>
                        <td class="align-middle">{{$contact->phone}}</td>
                        <td class="align-middle">{{$contact->message}}</td>
                        <td class="align-middle">{{$contact->created_at}}</td>
                        <td>
                            <a href="{{route('del_contact_us',$contact->id)}}" class="btn btn-danger m-2"><i class="fas fa-minus-circle"></i>  </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



