@extends('layouts.app')
@section('title')
    Subject
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-book"> </i> Subjects  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
    </div>

    <section class="content">
        <div class="row">
            <div class="col-12" id="accordion">
                @foreach($faculties as $faculty)
                    <div class="card card-primary card-outline">
                        <a class="d-block w-100 text-center" data-toggle="collapse" href="#collapse{{$faculty->id}}">
                            <div class="card-header">
                                <h4 class="card-title w-100">
                                    {{$faculty->university->name}} <i class="fa fa-arrow-left"></i> {{$faculty->name}}
                                </h4>
                            </div>
                        </a>
                        <div id="collapse{{$faculty->id}}" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <!-- /.col -->
                                    @foreach($faculty->departments as $department)
                                        <div class="col-12 row">
                                            <div class="col-md-3 col-sm-6 col-12">
                                                <div class="info-box bg-gradient-gray">
                                                    <div class="info-box-content text-center">
                                                        <span class="info-box-text">{{$department->name}}</span>
                                                        <span class="info-box-number">
                                                            @foreach($levels as $level)
                                                                @if($level->department_id==$department->id)
                                                                    <a data-toggle="modal" data-target="#add_subject" class="btn btn-success add_subject @if(!\Auth::user()->role) disabled @endif" data-id="{{$level->id}}"> {{$level->level}} </a>
                                                                @endif
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                </div>
                                                <!-- /.info-box -->
                                            </div>
                                            @foreach($subjects as $subject)
                                                @if($subject->department->department_id==$department->id)
                                                <div class="col-md-2 col-sm-6 col-12">
                                                    <div class="info-box bg-gray-light">
                                                        <div class="info-box-content text-center">
                                                            <span class="info-box-number btn-info">
                                                                {{$subject->department->level}}
                                                            </span>
                                                            <span class="info-box-more">
                                                                {{$subject->name_ar}} - {{$subject->name_en}} <br>
                                                                <i class="fa fa-video"></i> {{$subject->videos->count()}} <i class="fa fa-file-pdf"></i> {{$subject->files->count()}}
                                                            </span>
                                                            <span class="info-box-more">
                                                                <a class="btn btn-primary edit_subject @if(!\Auth::user()->role) disabled @endif" data-target="#edit_subject" data-id="{{$subject->id}}"  data-name_ar="{{$subject->name_ar}}" data-name_en="{{$subject->name_en}}" data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                                                <a  data-toggle="modal" data-target="#del_subject" class="btn btn-danger del_subject @if(!\Auth::user()->role) disabled @endif" data-id="{{$subject->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                                            </span>
                                                        </div>
                                                        <!-- /.info-box-content -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                        </div>
                                    @endforeach
                                <!-- /.col -->
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_subject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_add_subject" role="form" method="POST" action="{{route('add_subject')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Subject </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name_ar" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name (Arabic) </label>
                            <input  type="text"  name='name_ar' class="form-control validate text-center" required>
                            <label for="name_en" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name (English) </label>
                            <input  type="text"  name='name_en' class="form-control validate text-center" required>
                            <input type="hidden" name="level_id" id="level_id" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_subject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_subject" role="form" method="POST" action="" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Subject</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name_ar" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name (Arabic) </label>
                            <input  type="text"  name='name_ar' id='name_ar' class="form-control validate text-center" required>
                            <label for="name_en" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name (English) </label>
                            <input  type="text"  name='name_en' id='name_en' class="form-control validate text-center" required>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_subject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Subject </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_subject" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click','.add_subject',function () {
        $('#level_id').val($(this).data('id'));
        $('#add_subject').modal('show');
    });
    $(document).on('click', '.edit_subject', function() {
        $('#name_ar').val($(this).data('name_ar'));
        $('#name_en').val($(this).data('name_en'));
        var id=$(this).data('id');
        var action="{{route('edit_subject',"id")}}";
        action=action.replace('id',id);
        $('#form_edit_subject').attr('action',action);
        $('#edit_subject').modal('show');
    });
    $(document).on('click','.del_subject',function () {
        var id=$(this).data('id');
        var action="{{route('del_subject',"id")}}";
        action=action.replace('id',id);
        $('#form_del_subject').attr('action',action);
        $('#del_subject').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".card").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



