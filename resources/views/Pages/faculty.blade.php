@extends('layouts.app')
@section('title')
    Faculty
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  style="direction: rtl" class="btn btn-success @if(!\Auth::user()->role) disabled @endif"  data-toggle="modal" data-target="#add_university"><i class="nav-icon fas fa-plus"></i> Add University </a>
        </div>
        <h2 class="text-center col-md-8"> <i class="nav-icon fas fa-university"> </i> Faculties  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search University ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="40%" class="align-middle"><i class="fas fa-university"></i> University </th>
                        <th width="30%" class="align-middle"><i class="fas fa-tags"></i>  # of Faculty </th>
                        <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($universities as $university)
                        <tr id="data">
                            <td class="align-middle accordion-toggle collapsed " data-toggle="collapse"  href="#id{{$university->id}}"> <i class="fas fa-bars"></i></td>
                            <td class="align-middle">{{$university->name}}</td>
                            <td class="align-middle">{{$university->facultys->count()}}</td>
                            <td>
                                <a  style="direction: rtl" class="btn btn-primary edit_university @if(!\Auth::user()->role) disabled @endif" data-target="#edit_university" data-id="{{$university->id}}" data-name="{{$university->name}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> Edit </a>
                                <a  data-toggle="modal" data-target="#del_university" class="btn btn-danger del_university @if(!\Auth::user()->role) disabled @endif" data-id="{{$university->id}}"><i class="fas fa-minus-circle"></i> Delete </a>
                            </td>
                        </tr>
                        <tr class="hide-table-padding">
                            <td id="id{{$university->id}}" class="collapse in py-2 ">
                                <a href="" class="btn btn-success add_faculty mt-5 @if(!\Auth::user()->role) disabled @endif" data-toggle="modal" data-target="#add_faculty" data-id="{{$university->id}}"><i class="nav-icon fas fa-plus-circle"></i>  </a>
                            </td>
                            <td colspan="4">
                                <div id="id{{$university->id}}" class="collapse in py-2" style="width: 100%">
                                    @foreach($university->facultys as $faculty)
                                        <div class="row py-1">
                                            <div class="col-8">{{$faculty->name}} <i class="fas fa-building"></i> </div>
                                            <div class="col-4">
                                                <a class="btn btn-primary edit_faculty @if(!\Auth::user()->role) disabled @endif" data-target="#edit_faculty" data-id="{{$faculty->id}}"  data-name="{{$faculty->name}}" data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                                <a data-toggle="modal" data-target="#del_faculty" class="btn btn-danger del_faculty @if(!\Auth::user()->role) disabled @endif" data-id="{{$faculty->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_university" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_university')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add University </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i> Name </label>
                            <input  type="text"  name='name' class="form-control validate text-center" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_university" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_university" role="form" method="POST" action="" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit University</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i>  Name </label>
                            <input type="text"  name='name' id='name_univeristy' class="form-control validate text-center" required>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_university" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete University </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_university" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_faculty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_add_faculty" role="form" method="POST" action="{{route('add_faculty')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Faculty</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">

                        <div class="md-form mb-2" >
                            <label  data-error="wrong" data-success="right" ><i class="fas fa-file prefix grey-text"></i>   Name  </label>
                            <input type="text"  name='name' class="form-control validate text-center" required>
                        </div>

                        <input type="hidden" name="university_id" id="university_id" >

                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_faculty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <form id="form_edit_faculty" role="form" method="POST" action="" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body mx-3">

                        <div class="md-form mb-2">
                            <label  data-error="wrong" data-success="right"><i class="fas fa-file prefix grey-text"></i>   Name </label>
                            <input type="text"  name="name" id='name_faculty' class="form-control validate text-center" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_faculty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete  </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_faculty" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_university', function() {
        $('#name_univeristy').val($(this).data('name'));
        var id=$(this).data('id');
        var action="{{route('edit_university',"id")}}";
        action=action.replace('id',id);
        $('#form_edit_university').attr('action',action);
        $('#edit_university').modal('show');
    });
    $(document).on('click','.del_university',function () {
        var id=$(this).data('id');
        var action="{{route('del_university',"id")}}";
        action=action.replace('id',id);
        $('#form_del_university').attr('action',action);
        $('#del_university').modal('show');
    });

    $(document).on('click','.add_faculty',function () {
        $('#university_id').val($(this).data('id'));
        $('#add_faculty').modal('show');
    });
    $(document).on('click', '.edit_faculty', function() {
        $('#name_faculty').val($(this).data('name'));
        var id=$(this).data('id');
        var action="{{route('edit_faculty',"id")}}";
        action=action.replace('id',id);
        $('#form_edit_faculty').attr('action',action);
        $('#edit_faculty').modal('show');
    });
    $(document).on('click','.del_faculty',function () {
        var id=$(this).data('id');
        var action="{{route('del_faculty',"id")}}";
        action=action.replace('id',id);
        $('#form_del_faculty').attr('action',action);
        $('#del_faculty').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



