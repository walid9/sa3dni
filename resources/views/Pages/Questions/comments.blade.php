@extends('layouts.app')
@section('title')
    Question Comments
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fas fa-comments"> </i> Question Comments  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="5%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="50%" class="align-middle"><i class="fas fa-file-alt"></i> Question </th>
                        <th width="15%" class="align-middle"><i class="fas fa-thumbs-up"></i>  Likes </th>
                        <th width="15%" class="align-middle"><i class="fas fa-comments"></i>  Comments </th>
                        <th width="15%" class="align-middle"><i class="fas fa-ban"></i>  Report </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($questions as $question)
                        <tr id="data">
                            <td class="align-middle accordion-toggle collapsed " data-toggle="collapse"  href="#id{{$question->id}}"> <i class="fas fa-bars"></i></td>
                            <td class="align-middle">{{$question->text}}</td>
                            <td class="align-middle">{{$question->likes}}</td>
                            <td class="align-middle">{{$question->comments->count()}}</td>
                            <td class="align-middle">{{$question->report}}</td>
                        </tr>
                        <tr class="hide-table-padding">
                            <td colspan="5">
                                <div id="id{{$question->id}}" class="collapse in py-2" style="width: 100%">
                                    @foreach($question->comments as $comment)
                                        <div class="row py-1">
                                            <div class="col-4"><i class="fas fa-comment"></i> {{$comment->text}}  </div>
                                            <div class="col-2"><i class="fas fa-thumbs-up"></i> {{$comment->likes}}  </div>
                                            <div class="col-2"><i class="fas fa-ban"></i> {{$comment->report}}  </div>
                                            <div class="col-2"><i class="fas fa-user"></i> {{$comment->user->user_name}}  </div>
                                            <div class="col-2">
                                                <a  data-toggle="modal" data-target="#del_question_comment" class="btn btn-danger del_question_comment" data-id="{{$comment->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Comment </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_comment" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click','.del_question_comment',function () {
        var id=$(this).data('id');
        var action="{{route('del_question_comment',"id")}}";
        action=action.replace('id',id);
        $('#form_del_comment').attr('action',action);
        $('#del_comment').modal('show');
    });
</script>



