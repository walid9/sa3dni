@extends('layouts.app')
@section('title')
    Waiting Questions
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-spinner"> </i> Waiting Questions  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                    <th width="50%" class="align-middle"><i class="fas fa-newspaper"></i> Text </th>
                    <th width="20%" class="align-middle"><i class="fas fa-user"></i> User </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions as $key=>$question)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$question->text}}</td>
                        <td class="align-middle">{{$question->user->user_name}}</td>
                        <td>
                            <a href="{{route('to_approve_question',$question->id)}}" class="btn btn-info m-2"><i class="fas fa-check-circle"></i>  </a>
                            <a href="{{route('to_reject_question',$question->id)}}" class="btn btn-warning"><i class="fas fa-ban"></i>  </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>



@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



