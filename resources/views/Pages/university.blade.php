@extends('layouts.app')
@section('title')
    University
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a href="" style="direction: rtl" class="btn btn-success" data-toggle="modal" data-target="#add_university"><i class="nav-icon fas fa-plus"></i> Add University </a>
        </div>
        <h2 class="text-center col-md-8"> <i class="nav-icon fas fa-university"> </i> Universities  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}}
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"> # </th>
                    <th width="70%" class="align-middle"><i class="fas fa-university"></i> Name </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($universities as $university)
                    <tr id="data">
                        <td class="align-middle">{{$university->id}}</td>
                        <td class="align-middle">{{$university->name}}</td>
                        <td>
                            <a  style="direction: rtl" class="btn btn-primary edit_university" data-target="#edit_university" data-id="{{$university->id}}" data-name="{{$university->name}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> Edit </a>
                            <a  data-toggle="modal" data-target="#del_university" class="btn btn-danger  btn-mini deleteRecord del_university" data-id="{{$university->id}}"><i class="fas fa-minus-circle"></i> Delete </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_university" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_university')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add University </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-user prefix grey-text"></i> Name </label>
                            <input  type="text"  name='name' class="form-control validate text-center" required>
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_university" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="edit" role="form" method="POST" action="" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit University</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label for="name" data-error="wrong" data-success="right" ><i class="fas fa-user prefix grey-text"></i>  Name </label>
                            <input type="text"  name='name' id='name' class="form-control validate text-center" required>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_university" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete University </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_university', function() {
        $('#name').val($(this).data('name'));
        var id=$(this).data('id');
        var action="{{route('edit_university',"id")}}";
        action=action.replace('id',id);
        $('#edit').attr('action',action);
        $('#edit_university').modal('show');
    });
    $(document).on('click','.del_university',function () {
        var id=$(this).data('id');
        var action="{{route('del_university',"id")}}";
        action=action.replace('id',id);
        $('#del').attr('action',action);
        $('#del_university').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



