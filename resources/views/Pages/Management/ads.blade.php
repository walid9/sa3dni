@extends('layouts.app')
@section('title')
    Ads
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_ad"><i class="nav-icon fas fa-plus"></i> Add Ad </a>
        </div>
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-ad"> </i> Ads  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="5%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="20%" class="align-middle"><i class="fas fa-image"></i> Photo </th>
                        <th width="5%" class="align-middle"><i class="fas fa-info-circle"></i>  Type </th>
                        <th width="15%" class="align-middle"><i class="fas fa-link"></i>  Link </th>
                        <th width="10%" class="align-middle"><i class="fas fa-calendar-day"></i>  Start </th>
                        <th width="10%" class="align-middle"><i class="fas fa-calendar-day"></i>  End </th>
                        <th width="15%" class="align-middle"><i class="fas fa-school"></i>  Faculty </th>
                        <th width="8%" class="align-middle"><i class="fas fa-circle"></i>  Active </th>
                        <th width="12%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ads as $ad)
                        <tr id="data">
                            <td class="align-middle accordion-toggle collapsed " data-toggle="collapse"  href="#id{{$ad->id}}"> <i class="fas fa-bars"></i></td>
                            <td class="align-middle">
                                <a href="{{asset($ad->img->localUrl)}}" target="_blank">
                                    <img src="{{asset($ad->img->localUrl)}}" width="100px">
                                </a>
                            </td>
                            <td class="align-middle">{{$ad->type}}</td>
                            <td class="align-middle">{{$ad->link}}</td>
                            <td class="align-middle">{{$ad->start_date}}</td>
                            <td class="align-middle">{{$ad->end_date}}</td>
                            <td class="align-middle">{{$ad->faculty->university->name}}<br>{{$ad->faculty->name}}</td>
                            <td class="align-middle">
                                @if($ad->active)
                                    <a  class="btn btn-info" href="{{route('active_ad',$ad->id)}}"><i class="nav-icon fas fa-check-circle"></i>  </a>
                                @else
                                    <a  class="btn btn-warning" href="{{route('active_ad',$ad->id)}}"><i class="nav-icon fas fa-times-circle"></i>  </a>
                                @endif
                            </td>
                            <td class="align-middle">
                                <a  class="btn btn-primary edit_ad m-1" data-target="#edit_ad" data-id="{{$ad->id}}" data-link="{{$ad->link}}" data-type="{{$ad->type}}" data-start_date="{{$ad->start_date}}" data-end_date="{{$ad->end_date}}" data-university="{{$ad->faculty->university->name}}" data-faculty="{{$ad->faculty->name}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                <a  data-toggle="modal" data-target="#del_ad" class="btn btn-danger del_ad m-1" data-id="{{$ad->id}}"><i class="fas fa-minus-circle"></i>  </a>
                            </td>
                        </tr>
                        <tr class="hide-table-padding">
                            <td id="id{{$ad->id}}" class="collapse in py-2">
                                <a class="btn btn-success add_ad_level mt-3" data-toggle="modal" data-target="#add_ad_level" data-id="{{$ad->id}}" data-faculty_id="{{$ad->faculty_id}}"><i class="nav-icon fas fa-plus-circle"></i> </a>
                            </td>
                            <td colspan="9">
                                <div id="id{{$ad->id}}" class="collapse in py-2" style="width: 100%">
                                    @foreach($ad->level as $level)
                                        <div class="row py-1">
                                            <div class="col-6"><i class="fas fa-building "></i> {{$level->level_department->department->name}}  </div>
                                            <div class="col-4"><i class="fas fa-id-badge"></i> {{$level->level_department->level}}  </div>
                                            <div class="col-2">
                                                <a data-toggle="modal" data-target="#del_ad_level" class="btn btn-danger del_ad_level" data-id="{{$level->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_ad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_ad')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Ads </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 form-group row" >
                            <div class="col-md-8 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-image prefix grey-text"></i> Photo </label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" required>
                                    <label class="custom-file-label" >Choose Image</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-info prefix grey-text"></i> Type </label>
                                <select name='type' class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Type</option>
                                    <option value="link"> Link </option>
                                    <option value="meeting"> Meeting </option>
                                    <option value="payment"> Payment </option>
                                    <option value="other"> Other </option>
                                </select>
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-12 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-link prefix grey-text"></i> Link </label>
                                <input type="text" class="form-control text-center" name="link">
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> Start </label>
                                <input  type="date"  name='start_date' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> End </label>
                                <input  type="date"  name='end_date' class="form-control validate text-center" required>
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i> University </label>
                                <select id="select_university" name='university' class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose University</option>
                                    @foreach($universities as $university )
                                        <option value="{{$university->id}}">{{$university->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-school prefix grey-text"></i> Faculty </label>
                                <select id="select_faculty"  name='faculty' class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Faculty</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_ad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('edit_ad')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold"> Edit Ads </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" class="form-control" id="id" name="id" >
                        <div class="md-form mb-2 form-group row" >
                            <div class="col-md-8 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-image prefix grey-text"></i> Photo </label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input">
                                    <label class="custom-file-label" >Choose Image</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-info prefix grey-text"></i> Type </label>
                                <select name='type' id="type" class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Type</option>
                                    <option value="link"> Link </option>
                                    <option value="meeting"> Meeting </option>
                                    <option value="payment"> Payment </option>
                                    <option value="other"> Other </option>
                                </select>
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-12 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-link prefix grey-text"></i> Link </label>
                                <input type="text" id="link" class="form-control text-center" name="link">
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> Start </label>
                                <input  type="date" id="start_date" name='start_date' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> End </label>
                                <input  type="date" id="end_date"  name='end_date' class="form-control validate text-center" required>
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i> University </label>
                                <input type="text" class="form-control text-center" id="university" readonly>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-school prefix grey-text"></i> Faculty </label>
                                <input type="text" class="form-control text-center" id="faculty" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_ad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Ads </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del_ad_form" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

    {{--Start Add Level Modal--}}
    <div class="modal fade" id="add_ad_level" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_ad_level')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Ads Level </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" id="ad_id" name="ad_id" required>
                        <input type="hidden" id="level_department" name="level_department" required>
                        <div class="md-form mb-2 form-group row" >
                            <div class="col-md-6 col-12">
                                <select id="select_department"  class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Department</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <select id="select_level"  class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Level</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Level Modal--}}

    {{-- Start Delete Level Modal--}}
    <div class="modal fade" id="del_ad_level" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Level </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del_ad_level_form" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Level Modal --}}
@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>

    $(document).on('click', '.edit_ad', function() {
        $('#id').val($(this).data('id'));
        $('#start_date').val($(this).data('start_date'));
        $('#end_date').val($(this).data('end_date'));
        $('#link').val($(this).data('link'));
        $('#type').val($(this).data('type'));
        $('#university').val($(this).data('university'));
        $('#faculty').val($(this).data('faculty'));
        $('#edit_ad').modal('show');
    });
    $(document).on('click','.del_ad',function () {
        var id=$(this).data('id');
        var action="{{route('del_ad',"id")}}";
        action=action.replace('id',id);
        $('#del_ad_form').attr('action',action);
        $('#del_ad').modal('show');
    });
    $(document).on('click','.del_ad_level',function () {
        var id=$(this).data('id');
        var action="{{route('del_ad_level',"id")}}";
        action=action.replace('id',id);
        $('#del_ad_level_form').attr('action',action);
        $('#del_ad_level').modal('show');
    });
    $(document).on('click','.add_ad_level',function () {
        $('#select_department').find('option').not(':first').remove();
        $('#select_level').find('option').not(':first').remove();
        $('#ad_id').val($(this).data('id'));
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('get_department') }}",
            method:"GET",
            data:{id:$(this).data('faculty_id'), _token:_token},
            success: function(response){
                var len = 0;
                if(response['data'] != null){
                    len = response['data'].length;
                }
                if(len > 0){
                    // Read data and create <option >
                    for(var i=0; i<len; i++){
                        var id = response['data'][i].id;
                        var name = response['data'][i].name;
                        var option=  "<option class='select_department' value='"+id+"'>"+name+"</option>";
                        $("#select_department").append(option);
                    }
                }

            },
        });

    });
    $(document).ready(function(){
        $('#select_university').on('change',function(){
            // University id
            var id = this.value;
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_faculty').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url:"{{ route('get_faculty') }}",
                method:"GET",
                data:{id:id, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option value='"+id+"'>"+name+"</option>";
                            $("#select_faculty").append(option);
                        }
                    }

                },
            });
        });

        $('#select_department').on('change',function(){
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url:"{{ route('get_level') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    $('#select_level').find('option').not(':first').remove();
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].level;
                            var option=  "<option class='select_level' value='"+id+"'>"+name+"</option>";
                            $("#select_level").append(option);
                        }
                    }
                },
            });
        });

        $('#select_level').on('change',function () {
            $('#level_department').val(this.value);
        });
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });


</script>



