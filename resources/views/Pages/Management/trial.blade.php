@extends('layouts.app')
@section('title')
    Free Trial
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fas fa-percentage"> </i> Free Trial  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i> </th>
                    <th width="60%" class="align-middle"><i class="fas fa-calendar-day"></i> Days </th>
                    <th width="30%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($trials as $key=>$trial)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$trial->days}}</td>
                        <td>
                            <a  class="btn btn-info edit_trial m-1" data-target="#edit_trial" data-id="{{$trial->id}}" data-days="{{$trial->days}}"   data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> Edit </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_trial" tabindex="-1" role="dialog" aria-hidden="true"  >
        <div class="modal-dialog" role="document" >
            <form id="edit" role="form" method="POST" action="{{route('edit_trial')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Trial</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="id" id="id">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> Days </label>
                            <input  type="number" id="days"  name='days' class="form-control validate text-center" min="0">
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_trial', function() {
        $('#id').val($(this).data('id'));
        $('#days').val($(this).data('days'));
        //$('#edit').modal('show');
    });
</script>



