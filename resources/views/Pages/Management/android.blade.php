@extends('layouts.app')
@section('title')
    Android App
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fab fa-android"> </i> Android App  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i> </th>
                    <th width="20%" class="align-middle"><i class="fas fa-file"></i> Title </th>
                    <th width="30%" class="align-middle"><i class="fas fa-info-circle"></i> Info </th>
                    <th width="20%" class="align-middle"><i class="fas fa-circle"></i>  Active </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($tools as $key=>$tool)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$tool->title}}</td>
                        <td class="align-middle">{{$tool->info}}</td>
                        @if($tool->status)
                            <td class="align-middle"><i class="fa fa-check-circle"></i></td>
                            <td>
                                <a  class="btn btn-danger m-1" href="{{route('android_tool',$tool->id)}}" ><i class="nav-icon fas fa-times-circle"></i> Un Active </a>
                            </td>
                        @else
                            <td class="align-middle"><i class="fa fa-times-circle"></i></td>
                            <td>
                                <a  class="btn btn-success m-1" href="{{route('android_tool',$tool->id)}}" ><i class="nav-icon fas fa-check-circle"></i> Active </a>
                            </td>
                        @endif

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection




