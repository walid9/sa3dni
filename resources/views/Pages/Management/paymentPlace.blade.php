@extends('layouts.app')
@section('title')
   Payment Places
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success @if(!\Auth::user()->role) disabled @endif" data-toggle="modal" data-target="#add_place"><i class="nav-icon fas fa-plus"></i> Add Place </a>
        </div>
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-map-marker-alt"> </i> Payment Places  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                    <th width="40%" class="align-middle"><i class="fas fa-location-arrow"></i> Name </th>
                    <th width="20%" class="align-middle"><i class="fas fa-circle"></i>  Active </th>
                    <th width="30%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($places as $key=>$place)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$place->name}}</td>
                        @if($place->status)
                            <td class="align-middle"><i class="fa fa-check-circle"></i></td>
                        @else
                            <td class="align-middle"><i class="fa fa-times-circle"></i></td>
                        @endif
                        <td>
                            @if($place->status)
                                <a class="btn btn-warning  m-2" href="{{route('active_place',$place->id)}}" ><i class="fas fa-times-circle"></i> Un Active </a>
                            @else
                                <a class="btn btn-info  m-2" href="{{route('active_place',$place->id)}}" ><i class="fas fa-check-circle"></i> Active </a>
                            @endif
                            <br>
                            <a  class="btn btn-primary edit_place m-1 @if(!\Auth::user()->role) disabled @endif" data-target="#edit_place" data-id="{{$place->id}}" data-name="{{$place->name}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> Edit </a>
                            <a  data-toggle="modal" data-target="#del_place" class="btn btn-danger del_place m-1 @if(!\Auth::user()->role) disabled @endif" data-id="{{$place->id}}"><i class="fas fa-minus-circle"></i> Delete </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_place" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_place')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Place </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-location-arrow prefix grey-text"></i> Place </label>
                            <input  type="text"  name='name' class="form-control validate text-center" required>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_place" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="edit" role="form" method="POST" action="{{route('edit_place')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Place</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input  type="hidden" id="id"  name='id' class="form-control validate text-center" required>
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-location-arrow prefix grey-text"></i> Place </label>
                            <input  type="text" id="name" name='name' class="form-control validate text-center" required>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_place" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Place </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_place', function() {
        $('#id').val($(this).data('id'));
        $('#name').val($(this).data('name'));
        $('#edit_place').modal('show');
    });
    $(document).on('click','.del_place',function () {
        var id=$(this).data('id');
        var action="{{route('del_place',"id")}}";
        action=action.replace('id',id);
        $('#del').attr('action',action);
        $('#del_place').modal('show');
    });
</script>



