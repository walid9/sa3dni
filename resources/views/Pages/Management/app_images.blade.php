@extends('layouts.app')
@section('title')
     App Images
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_img"><i class="nav-icon fas fa-plus"></i> Add Image </a>
        </div>
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-photo"> </i> App Images  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                    <th width="40%" class="align-middle"><i class="fas fa-image"></i> Photo </th>
                    <th width="25%" class="align-middle"><i class="fas fa-image"></i> Type </th>
                    <th width="25%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($images as $key=>$img)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">
                            <a href="{{asset($img->img)}}" target="_blank">
                                <img src="{{asset($img->img)}}" width="100px">
                            </a>
                        </td>
                        <td class="align-middle">{{$img->type}}</td>
                        <td class="align-middle">
                            <a  data-toggle="modal" data-target="#del_img" class="btn btn-danger del_img m-1" data-id="{{$img->id}}"><i class="fas fa-minus-circle"></i>  </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_img_app')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold"> Add Image </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 form-group row" >
                            <div class="col-md-8 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-image prefix grey-text"></i> Photo </label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input" required>
                                    <label class="custom-file-label" >Choose Image</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-info prefix grey-text"></i> Type </label>
                                <select name='type' class="form-control validate text-center" readonly="" required>
                                    <option disabled value=""> Choose Type</option>
                                    <option selected value="subject"> Subject </option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Image </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del_img_form" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>

    $(document).on('click','.del_img',function () {
        var id=$(this).data('id');
        var action="{{route('del_img_app',"id")}}";
        action=action.replace('id',id);
        $('#del_img_form').attr('action',action);
        $('#del_img').modal('show');
    });

</script>



