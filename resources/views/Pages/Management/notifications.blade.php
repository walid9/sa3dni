@extends('layouts.app')
@section('title')
    Notifications
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fas fa-sticky-note"> </i> Notifications  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="col-md-12 col-12 card card-info">
                <div class="card-body">
                    <form role="form" method="POST" action="{{route('send_notification')}}" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- textarea -->
                                <div class="form-group">
                                    <label>Title</label>
                                    <textarea class="form-control" name="title" rows="3" required placeholder="Enter ..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-6">
                                <div class="form-group">
                                    <label>University</label>
                                    <select id="select_university" name='university' class="form-control validate text-center" required>
                                        <option disabled selected value="" > Choose University</option>
                                        @foreach($universities as $university )
                                            <option value="{{$university->id}}">{{$university->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 col-6">
                                <div class="form-group">
                                    <label>Faculty</label>
                                    <select id="select_faculty"  name='faculty' class="form-control validate text-center" required>
                                        <option disabled selected value="" > Choose Faculty</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 col-6">
                                <div class="form-group">
                                    <label>Department</label>
                                    <select id="select_department"  name='department' class="form-control validate text-center" >
                                        <option disabled selected value="" > Choose Department</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 col-6">
                                <div class="form-group">
                                    <label>Level</label>
                                    <select id="select_level"  name='level' class="form-control validate text-center" >
                                        <option disabled selected value="" > Choose Level</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center mt-5">
                            <button class="btn btn-success"> Send <i class="fa fa-sticky-note ml-1"></i></button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
    </div>



@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#select_university').on('change',function(){
            // University id
            var id = this.value;
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_faculty').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url:"{{ route('get_faculty') }}",
                method:"GET",
                data:{id:id, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_faculty").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option value='"+id+"'>"+name+"</option>";
                            $("#select_faculty").append(option);
                        }
                    }

                },
            });
        });

        $('#select_faculty').on('change',function(){
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_department').find('option').not(':first').remove();
            $.ajax({
                url:"{{ route('get_department') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_department").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option class='select_department' value='"+id+"'>"+name+"</option>";
                            $("#select_department").append(option);
                        }
                    }
                },
            });

        });

        $('#select_department').on('change',function(){
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_level').find('option').not(':first').remove();

            $.ajax({
                url:"{{ route('get_level') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_level").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].level;
                            var option=  "<option class='select_level' value='"+name+"'>"+name+"</option>";
                            $("#select_level").append(option);
                        }
                    }
                },
            });

        });

        $('#select_level').on('change',function () {
            $('#level_department').val(this.value);
        });
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });


</script>



