@extends('layouts.app')
@section('title')
    Qr Code
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_qr"><i class="nav-icon fas fa-plus"></i> Add Qr </a>
        </div>
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-qrcode"> </i> Qr Code  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="20%" class="align-middle"><i class="fas fa-qrcode"></i> Code </th>
                    <th width="15%" class="align-middle"><i class="fas fa-money-bill-alt"></i> Price </th>
                    <th width="15%" class="align-middle"><i class="fas fa-calendar-day"></i> # Month </th>
                    <th width="30%" class="align-middle"><i class="fas fa-tags"></i> Description </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($codes as $code)
                    <tr id="data">
                        <td class="align-middle">{{$code->code}}</td>
                        <td class="align-middle">{{$code->price}}</td>
                        <td class="align-middle">{{$code->period}}</td>
                        <td class="align-middle">{{$code->description}}</td>
                        <td>
                            <a  class="btn btn-info view_qr m-1" data-target="#view_qr" data-id="{{$code->id}}" data-code="{{$code->code}}" data-description="{{$code->description}}" data-period="{{$code->period}}" data-price="{{$code->price}}" data-count="{{$code->count}}"  data-toggle="modal" ><i class="nav-icon fas fa-search"></i> View </a>
                            <a  data-toggle="modal" data-target="#del_qr" class="btn btn-danger del_qr m-1" data-id="{{$code->id}}"><i class="fas fa-minus-circle"></i> Delete </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_qr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_qr')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add QrCode </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-qrcode prefix grey-text"></i> Code </label>
                                <input  type="text"  name='code' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-3 col-6">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> # Month </label>
                                <input  type="number"  name='period' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-3 col-6">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i> Price </label>
                                <input  type="number"  name='price' class="form-control validate text-center" required>
                            </div>
                        </div>
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-tags prefix grey-text"></i> Description </label>
                            <input  type="text"  name='description' class="form-control validate text-center" >
                        </div>

                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start View Modal--}}
    <div class="modal fade" id="view_qr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">QrCode</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-qrcode prefix grey-text"></i> Code </label>
                                <input  type="text" id="code"  name='code' readonly class="form-control validate text-center" >
                            </div>
                            <div class="col-md-3 col-6">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> # Month </label>
                                <input  type="number" id="period" readonly name='period' class="form-control validate text-center" >
                            </div>
                            <div class="col-md-3 col-6">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i> Price </label>
                                <input  type="number" id="price" readonly name='price' class="form-control validate text-center" >
                            </div>
                        </div>

                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-tags prefix grey-text"></i> Description </label>
                            <input  type="text" id="description" readonly name='description' class="form-control validate text-center" >
                        </div>

                        <div class="modal-body mx-3">
                            <div class="md-form mb-2 row" >
                                <div class="col-md-6 col-6">
                                    <label data-error="wrong" data-success="right" ><i class="fas fa-users prefix grey-text"></i> Users </label>
                                    <input  type="text" id="count" readonly name='count' class="form-control validate text-center">
                                </div>
                                <div class="col-md-6 col-6">
                                    <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i> Total </label>
                                    <input  type="text" id="total" readonly name='total' class="form-control validate text-center">
                                </div>
                            </div>
                    </div>
                </div>
        </div>
        </div>
    </div>
    {{--End View Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_qr" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete QrCode </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.view_qr', function() {
        $('#code').val($(this).data('code'));
        $('#description').val($(this).data('description'));
        $('#period').val($(this).data('period'));
        $('#price').val($(this).data('price'));
        $('#count').val($(this).data('count'));
        $('#total').val($(this).data('count')*$(this).data('price'));
        $('#edit_university').modal('show');
    });
    $(document).on('click','.del_qr',function () {
        var id=$(this).data('id');
        var action="{{route('del_qr',"id")}}";
        action=action.replace('id',id);
        $('#del').attr('action',action);
        $('#del_qr').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



