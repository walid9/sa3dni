@extends('layouts.app')
@section('title')
   Faculty Price
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_qr"><i class="nav-icon fas fa-plus"></i> Add Price </a>
        </div>
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-money-bill-alt"> </i> Price Each Faculty  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="25%" class="align-middle"><i class="fas fa-university"></i> University </th>
                    <th width="25%" class="align-middle"><i class="fas fa-school"></i> Faculty </th>
                    <th width="15%" class="align-middle"><i class="fas fa-money-bill-alt"></i> Price </th>
                    <th width="15%" class="align-middle"><i class="fas fa-tags"></i> Month Count </th>
                    <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($objects as $object)
                    <tr id="data">
                        <td class="align-middle">{{$object->university->name}}</td>
                        <td class="align-middle">{{$object->faculty->name}}</td>
                        <td class="align-middle">{{$object->price}}</td>
                        <td class="align-middle">{{$object->period}}</td>
                        <td>
                            <a  class="btn btn-primary edit_faculty_price m-1" data-target="#edit_faculty_price" data-id="{{$object->id}}" data-university="{{$object->university->name}}" data-faculty="{{$object->faculty->name}}" data-price="{{$object->price}}" data-period="{{$object->period}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> Edit </a>
                            <a  data-toggle="modal" data-target="#del_faculty_price" class="btn btn-danger del_faculty_price m-1" data-id="{{$object->id}}"><i class="fas fa-minus-circle"></i> Delete </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_qr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_faculty_price')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Faculty Price </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i> University </label>
                                <select id="select_university" name='university' class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose University</option>
                                    @foreach($universities as $university )
                                        <option value="{{$university->id}}">{{$university->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-school prefix grey-text"></i> Faculty </label>
                                <select id="select_faculty"  name='faculty' class="form-control validate text-center" required>
                                    <option disabled selected value=""> Choose Faculty</option>
                                </select>
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i> Price </label>
                                <input  type="number"  name='price' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> Month Count </label>
                                <input  type="number"  name='period' class="form-control validate text-center" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}

    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_faculty_price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="edit" role="form" method="POST" action="{{route('edit_faculty_price')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Faculty Price</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input  type="hidden" id="id"  name='id' class="form-control validate text-center" required>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i> University </label>
                                <input type="text" readonly id="university" class="form-control validate text-center" >
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-school prefix grey-text"></i> Faculty </label>
                                <input type="text" readonly id="faculty" class="form-control validate text-center" >
                            </div>
                        </div>
                        <div class="md-form mb-2 row" >
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i> Price </label>
                                <input  type="number" id="price" name='price' class="form-control validate text-center" required>
                            </div>
                            <div class="col-md-6 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-calendar-day prefix grey-text"></i> Month Count </label>
                                <input  type="number" id="period"  name='period' class="form-control validate text-center" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_faculty_price" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Faculty Price </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="del" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_faculty_price', function() {
        $('#id').val($(this).data('id'));
        $('#university').val($(this).data('university'));
        $('#faculty').val($(this).data('faculty'));
        $('#price').val($(this).data('price'));
        $('#period').val($(this).data('period'));
        $('#edit_university').modal('show');
    });
    $(document).on('click','.del_faculty_price',function () {
        var id=$(this).data('id');
        var action="{{route('del_faculty_price',"id")}}";
        action=action.replace('id',id);
        $('#del').attr('action',action);
        $('#del_faculty_price').modal('show');
    });
    $(document).ready(function(){
        $('#select_university').on('change',function(){
            // University id
            var id = this.value;
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_faculty').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url:"{{ route('get_faculty') }}",
                method:"GET",
                data:{id:id, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option value='"+id+"'>"+name+"</option>";
                            $("#select_faculty").append(option);
                        }
                    }

                },
            });
        });

        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



