@extends('layouts.app')

@section('content')

    <!-- Main statics -->
    <div class="row justify-content-center">
        <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$users_all}}</h3>
                    <p>All Users</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users"></i>
                </div>
                <a href="{{route('users')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$users_verified}}</h3>
                    <p>Users Verified</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-shield"></i>
                </div>
                <a class="small-box-footer">
                    <i class="fas fa-user-shield"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$users_unverified}}</h3>
                    <p>Users UnVerified</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-lock"></i>
                </div>
                <a class="small-box-footer">
                    <i class="fas fa-user-lock"></i>
                </a>
            </div>
        </div>
        @if(\Auth::user()->role)
        <!-- ./col -->
        <div class="col-lg-5 col-6">
            <!-- small card -->
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>{{$users_excellent}}</h3>
                    <p>Excellent Users</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-check"></i>
                </div>
                <a href="{{route('user_excellent')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-5 col-6">
            <!-- small card -->
            <div class="small-box bg-gray">
                <div class="inner">
                    <h3>{{$users_normal}}</h3>
                    <p>Normal Users</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-circle"></i>
                </div>
                <a href="{{route('user_normal')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$users_paid}}</h3>
                    <p>Users Active</p>
                </div>
                <div class="icon">
                    <i class="fas fa-battery-full"></i>
                </div>
                <a class="small-box-footer">
                    <i class="fas fa-battery-full"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$users_verified-$users_paid-$users_excellent}}</h3>
                    <p>Users Deactivate</p>
                </div>
                <div class="icon">
                    <i class="fas fa-battery-empty"></i>
                </div>
                <a class="small-box-footer">
                    <i class="fas fa-battery-empty"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$admins}}</h3>
                    <p>Admins</p>
                </div>
                <div class="icon">
                    <i class="fas fa-users-cog"></i>
                </div>
                <a class="small-box-footer">
                     <i class="fas fa-users-cog"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        @endif
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-navy">
                <div class="inner">
                    <h3>{{$universities}}</h3>
                    <p>Universities</p>
                </div>
                <div class="icon">
                    <i class="fas fa-university text-gray"></i>
                </div>
                <a href="{{route('faculty')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-navy">
                <div class="inner">
                    <h3>{{$faculties}}</h3>
                    <p>Faculties</p>
                </div>
                <div class="icon">
                    <i class="fas fa-school text-gray"></i>
                </div>
                <a href="{{route('faculty')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-lightblue">
                <div class="inner">
                    <h3>{{$department}}</h3>
                    <p>Departments</p>
                </div>
                <div class="icon">
                    <i class="fas fa-building"></i>
                </div>
                <a href="{{route('department')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-olive">
                <div class="inner">
                    <h3>{{$subjects}}</h3>
                    <p>Subjects</p>
                </div>
                <div class="icon">
                    <i class="fas fa-book"></i>
                </div>
                <a href="{{route('subject')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-lightblue">
                <div class="inner">
                    <h3>{{$videos}}</h3>
                    <p>Videos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-video"></i>
                </div>
                @if(\Auth::user()->role)
                <a href="{{route('videos')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
                @else
                    <a class="small-box-footer">
                        <i class="fas fa-video"></i>
                    </a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-maroon">
                <div class="inner">
                    <h3>{{$sheets}}</h3>
                    <p>Sheets</p>
                </div>
                <div class="icon">
                    <i class="fas fa-file-pdf"></i>
                </div>
                @if(\Auth::user()->role)
                    <a href="{{route('sheets')}}" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                    </a>
                @else
                    <a class="small-box-footer">
                        <i class="fas fa-file-pdf"></i>
                    </a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-purple">
                <div class="inner">
                    <h3>{{$summary}}</h3>
                    <p>Summary</p>
                </div>
                <div class="icon">
                    <i class="fas fa-file-pdf"></i>
                </div>
                @if(\Auth::user()->role)
                    <a href="{{route('summary')}}" class="small-box-footer">
                        More info <i class="fas fa-arrow-circle-right"></i>
                    </a>
                @else
                    <a class="small-box-footer">
                        <i class="fas fa-file-pdf"></i>
                    </a>
                @endif
            </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gray-dark">
                <div class="inner">
                    <h3>{{$contact}}</h3>
                    <p>Contact</p>
                </div>
                <div class="icon">
                    <i class="fas fa-info-circle"></i>
                </div>
                <a href="{{route('contact_us')}}" class="small-box-footer">
                    More info <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /Main statics -->

    <!-- Posts statics -->
    <h3 class="mb-2 mt-4">Posts </h3>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fas fa-tags"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Posts</span>
                    <span class="info-box-number">{{$posts}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width:{{($posts?100:0)}}%"></div>
                    </div>
                    <span class="progress-description">
                        <i class="fa"></i>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
                <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Approved Posts</span>
                    <span class="info-box-number">{{$posts_approve}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($posts_approve*100/($posts?$posts:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('approved_posts')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
                <span class="info-box-icon"><i class="fas fa-spinner"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Waiting Posts</span>
                    <span class="info-box-number">{{$posts_wait}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($posts_wait*100/($posts?$posts:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('waiting_posts')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
                <span class="info-box-icon"><i class="fas fa-ban"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Reject Posts</span>
                    <span class="info-box-number">{{$posts_reject}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($posts_reject*100/($posts?$posts:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('rejected_posts')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>

                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /Posts statics -->

    <!-- News statics -->
    <h3 class="mb-2 mt-4">News </h3>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fas fa-newspaper"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All News</span>
                    <span class="info-box-number">{{$news}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($news?100:0)}}%"></div>
                    </div>
                    <span class="progress-description">
                        <i class="fa"></i>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
                <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Approved News</span>
                    <span class="info-box-number">{{$news_approve}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($news_approve*100/($news?$news:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('approved_news')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
                <span class="info-box-icon"><i class="fas fa-spinner"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Waiting News</span>
                    <span class="info-box-number">{{$news_wait}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($news_wait*100/($news?$news:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('waiting_news')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
                <span class="info-box-icon"><i class="fas fa-ban"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Reject News</span>
                    <span class="info-box-number">{{$news_reject}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($news_reject*100/($news?$news:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('rejected_news')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /News statics -->

    <!-- Questions statics -->
    <h3 class="mb-2 mt-4">Questions </h3>
    <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-info">
                <span class="info-box-icon"><i class="fas fa-question"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">All Questions</span>
                    <span class="info-box-number">{{$questions}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($questions?100:0)}}%"></div>
                    </div>
                    <span class="progress-description">
                        <i class="fa"></i>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-success">
                <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Approved Questions</span>
                    <span class="info-box-number">{{$questions_approve}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($questions_approve*100/($questions?$questions:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('approved_questions')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-warning">
                <span class="info-box-icon"><i class="fas fa-spinner"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Waiting Questions</span>
                    <span class="info-box-number">{{$questions_wait}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($questions_wait*100/($questions?$questions:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('waiting_questions')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box bg-danger">
                <span class="info-box-icon"><i class="fas fa-ban"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Reject Questions</span>
                    <span class="info-box-number">{{$questions_reject}}</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: {{($questions_reject*100/($questions?$questions:1))}}%"></div>
                    </div>
                    <span class="progress-description">
                        <a href="{{ route('rejected_questions')}}" class="link-muted">
                            More info <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </span>

                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /Questions statics -->

@endsection
