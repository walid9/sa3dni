@extends('layouts.app')
@section('title')
    Post Comments
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-12"> <i class="nav-icon fas fa-comments"> </i> Post Comments  </h2>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="5%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="50%" class="align-middle"><i class="fas fa-file-alt"></i> Post </th>
                        <th width="15%" class="align-middle"><i class="fas fa-user"></i> User </th>
                        <th width="10%" class="align-middle"><i class="fas fa-thumbs-up"></i>  Likes </th>
                        <th width="10%" class="align-middle"><i class="fas fa-comments"></i>  Comments </th>
                        <th width="10%" class="align-middle"><i class="fas fa-ban"></i>  Report </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr id="data">
                            <td class="align-middle accordion-toggle collapsed " data-toggle="collapse"  href="#id{{$post->id}}"> <i class="fas fa-bars"></i></td>
                            <td class="align-middle">{{$post->text}}
                                @if($post->img)
                                    <br>
                                    <a href="{{asset($post->img)}}" target="_blank">
                                        <img src="{{asset($post->img)}}" width="100px">
                                    </a>
                                @endif
                            </td>
                            <td class="align-middle">{{$post->user->user_name}}</td>
                            <td class="align-middle">{{$post->likes}}</td>
                            <td class="align-middle">{{$post->comments->count()}}</td>
                            <td class="align-middle">{{$post->report}}</td>
                        </tr>
                        <tr class="hide-table-padding">
                            <td id="id{{$post->id}}" class="collapse in py-2">
                                <a class="btn btn-success add_post_comment mt-2" data-toggle="modal" data-target="#add_post_comment" data-id="{{$post->id}}"><i class="nav-icon fas fa-plus-circle"></i>  </a>
                            </td>
                            <td colspan="5">
                                <div id="id{{$post->id}}" class="collapse in py-2" style="width: 100%">
                                    @foreach($post->comments as $comment)
                                        <div class="row py-1">
                                            <div class="col-4"><i class="fas fa-comment"></i> {{$comment->text}}  </div>
                                            <div class="col-2"><i class="fas fa-thumbs-up"></i> {{$comment->likes}}  </div>
                                            <div class="col-2"><i class="fas fa-ban"></i> {{$comment->report}}  </div>
                                            <div class="col-2"><i class="fas fa-user"></i> {{$comment->user->user_name}}  </div>
                                            <div class="col-2">
                                                <a  class="btn btn-primary edit_post_comment" data-target="#edit_post_comment" data-id="{{$comment->id}}"  data-text="{{$comment->text}}" data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                                <a  data-toggle="modal" data-target="#del_post_comment" class="btn btn-danger del_post_comment" data-id="{{$comment->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>
    {{--Start Add Modal--}}
    <div class="modal fade" id="add_post_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_post_comment')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Comment </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <input type="hidden" name="post_id" id="post_id">
                            <label data-error="wrong" data-success="right" ><i class="fas fa-comment-alt prefix grey-text"></i> Comment </label>
                            <textarea name='text' class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}
    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Comment </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_comment" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}
    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_post_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_post_comment" role="form" method="POST" action="{{route('edit_post_comment')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Comment </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" id="id_comment" name="id">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-file-alt prefix grey-text"></i> Post </label>
                            <textarea name='text' id="text_comment" class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.add_post_comment', function() {
        $('#post_id').val($(this).data('id'));
        $('#add_post_comment').modal('show');
    });
    $(document).on('click', '.edit_post_comment', function() {
        $('#id_comment').val($(this).data('id'));
        $('#text_comment').val($(this).data('text'));
        $('#edit_post_comment').modal('show');
    });
    $(document).on('click','.del_post_comment',function () {
        var id=$(this).data('id');
        var action="{{route('del_post_comment',"id")}}";
        action=action.replace('id',id);
        $('#form_del_comment').attr('action',action);
        $('#del_comment').modal('show');
    });

</script>



