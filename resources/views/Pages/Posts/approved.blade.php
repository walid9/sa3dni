@extends('layouts.app')
@section('title')
    Approved Posts
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-thumbs-up"> </i> Approved Posts  </h2>
        <input class="col-md-2 form-control input-lg" type="text" id="search" onkeyup="fun_search()" name="search" placeholder="Search ... " autocomplete="off">
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
        <div class="card table-responsive" style="width: 100%">
            <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                <thead class="thead-light">
                <tr>
                    <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                    <th width="40%" class="align-middle"><i class="fas fa-newspaper"></i> Text </th>
                    <th width="10%" class="align-middle"><i class="fas fa-thumbs-up"></i> Like </th>
                    <th width="10%" class="align-middle"><i class="fas fa-comment"></i> Comment </th>
                    <th width="15%" class="align-middle"><i class="fas fa-user"></i> User </th>
                    <th width="15%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $key=>$post)
                    <tr id="data">
                        <td class="align-middle">{{$key+1}}</td>
                        <td class="align-middle">{{$post->text}}
                            @if($post->img)
                                <br>
                                <a href="{{asset($post->img)}}" target="_blank">
                                    <img src="{{asset($post->img)}}" width="100px">
                                </a>
                            @endif
                        </td>
                        <td class="align-middle">{{$post->likes}}</td>
                        <td class="align-middle">{{$post->comments->count()}}</td>
                        <td class="align-middle">{{$post->user->user_name}}</td>
                        <td>
                            <a  data-toggle="modal" data-target="#del_post" class="btn btn-danger del_post" data-id="{{$post->id}}"><i class="fas fa-minus-circle"></i>  </a>
                            <a href="{{route('to_reject_post',$post->id)}}" class="btn btn-warning"><i class="fas fa-ban"></i>  </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Post </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_post" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click','.del_post',function () {
        var id=$(this).data('id');
        var action="{{route('del_post',"id")}}";
        action=action.replace('id',id);
        $('#form_del_post').attr('action',action);
        $('#del_post').modal('show');
    });
    $(document).ready(function(){
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>



