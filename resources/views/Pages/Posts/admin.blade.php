@extends('layouts.app')
@section('title')
    Admin Posts
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-user-cog"> </i> Admin Posts  </h2>
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_post"><i class="nav-icon fas fa-plus"></i> Add Post </a>
        </div>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="40%" class="align-middle"><i class="fas fa-file-alt"></i> Text </th>
                        <th width="15%" class="align-middle"><i class="fas fa-thumbs-up"></i>  Likes </th>
                        <th width="15%" class="align-middle"><i class="fas fa-comment-alt"></i>  Comments </th>
                        <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr id="data">
                            <td class="align-middle accordion-toggle collapsed" data-toggle="collapse"  href="#id{{$post->id}}"> <i class="fas fa-bars"></i></td>
                            <td class="align-middle">{{$post->text}} <br>
                                @if($post->img)
                                    <a href="{{asset($post->img)}}" target="_blank">
                                        <img src="{{asset($post->img)}}" width="100px">
                                    </a>
                                @endif
                            </td>
                            <td class="align-middle">{{$post->likes}}</td>
                            <td class="align-middle">{{$post->comments->count()}}</td>
                            <td>
                                <a  class="btn btn-primary edit_post m-2" data-target="#edit_post" data-id="{{$post->id}}" data-text="{{$post->text}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> </a>
                                <a  data-toggle="modal" data-target="#del_post" class="btn btn-danger del_post" data-id="{{$post->id}}"><i class="fas fa-minus-circle"></i>  </a>
                            </td>
                        </tr>
                        <tr class="hide-table-padding">
                            <td id="id{{$post->id}}" class="collapse in py-2">
                                <a class="btn btn-success add_post_comment mt-2" data-toggle="modal" data-target="#add_post_comment" data-id="{{$post->id}}"><i class="nav-icon fas fa-plus-circle"></i>  </a>
                            </td>
                            <td colspan="5">
                                <div id="id{{$post->id}}" class="collapse in py-2" style="width: 100%">
                                    @foreach($post->comments as $comment)
                                        <div class="row py-1">
                                            <div class="col-5"><i class="fas fa-comment-alt"></i> {{$comment->text}}  </div>
                                            <div class="col-2"><i class="fas fa-thumbs-up"></i> {{$comment->likes}}  </div>
                                            @if($comment->user_id==null)
                                                <div class="col-3"><i class="fas fa-user"></i> Admin </div>
                                            @else
                                                <div class="col-3"><i class="fas fa-user"></i>  {{$comment->user->user_name??'User'}} </div>
                                            @endif
                                            <div class="col-2">
                                                <a  class="btn btn-primary edit_post_comment" data-target="#edit_post_comment" data-id="{{$comment->id}}"  data-text="{{$comment->text}}" data-toggle="modal" ><i class="nav-icon fas fa-edit"></i>  </a>
                                                <a  data-toggle="modal" data-target="#del_post_comment" class="btn btn-danger del_post_comment" data-id="{{$comment->id}}"><i class="fas fa-minus-circle"></i>  </a>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>


    {{--Start Add Modal--}}
    <div class="modal fade" id="add_post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_post')}}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Post </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2 row" >
                            <div class="col-md-12 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-file-alt prefix grey-text"></i> Post <small class="fa fa-asterisk text-danger"></small> </label>
                                <textarea name='text' class="form-control validate text-center" required></textarea>
                            </div>
                            <div class="col-md-12 col-12">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-image prefix grey-text"></i> Photo <small class="fa fa-asterisk text-danger"></small> </label>
                                <div class="custom-file">
                                    <input type="file" name="image" class="custom-file-input">
                                    <label class="custom-file-label" >Choose Image</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <label data-error="wrong" data-success="right" ><i class="fas fa-university prefix grey-text"></i> University </label>
                                <select id="select_university" name='university' class="form-control validate text-center" disabled >
                                    <option disabled selected value="" > Choose University</option>
                                    @foreach($universities as $university )
                                        <option value="{{$university->id}}">{{$university->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label>Faculty</label>
                                    <select id="select_faculty"  name='faculty' class="form-control validate text-center" disabled>
                                        <option disabled selected value="" > Choose Faculty</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label>Department</label>
                                    <select id="select_department"  name='department' class="form-control validate text-center" disabled >
                                        <option disabled selected value="" > Choose Department</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-6">
                                <div class="form-group">
                                    <label>Level</label>
                                    <select id="select_level"  name='level' class="form-control validate text-center" disabled >
                                        <option disabled selected value="" > Choose Level</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}
    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_post" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_post" role="form" method="POST" action="{{route('edit_post')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Post </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" id="id_post" name="id">
                        <div class="col-md-12 col-12">
                            <label data-error="wrong" data-success="right" ><i class="fas fa-file-alt prefix grey-text"></i> Post </label>
                            <textarea name='text' id="text" class="form-control validate text-center" required></textarea>
                        </div>
                        <div class="col-md-12 col-12">
                            <label data-error="wrong" data-success="right" ><i class="fas fa-image prefix grey-text"></i> Photo </label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input">
                                <label class="custom-file-label" >Choose Image</label>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}
    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Post </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_post" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}
    {{--Start Add Modal--}}
    <div class="modal fade" id="add_post_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_post_comment')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add Comment </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <input type="hidden" name="post_id" id="post_id">
                            <label data-error="wrong" data-success="right" ><i class="fas fa-comment-alt prefix grey-text"></i> Comment </label>
                            <textarea name='text' class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}
    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_post_comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_post_comment" role="form" method="POST" action="{{route('edit_post_comment')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit Comment </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" id="id_comment" name="id">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-file-alt prefix grey-text"></i> Post </label>
                            <textarea name='text' id="text_comment" class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}
    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_post_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete Comment </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_post_comment" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}

@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_post', function() {
        $('#id_post').val($(this).data('id'));
        $('#text').val($(this).data('text'));
        $('#edit_post').modal('show');
    });
    $(document).on('click','.del_post',function () {
        var id=$(this).data('id');
        var action="{{route('del_post',"id")}}";
        action=action.replace('id',id);
        $('#form_del_post').attr('action',action);
        $('#del_post').modal('show');
    });
    $(document).on('click', '.add_post_comment', function() {
        $('#post_id').val($(this).data('id'));
        $('#add_post_comment').modal('show');
    });
    $(document).on('click', '.edit_post_comment', function() {
        $('#id_comment').val($(this).data('id'));
        $('#text_comment').val($(this).data('text'));
        $('#edit_post_comment').modal('show');
    });
    $(document).on('click','.del_post_comment',function () {
        var id=$(this).data('id');
        var action="{{route('del_post_comment',"id")}}";
        action=action.replace('id',id);
        $('#form_del_post_comment').attr('action',action);
        $('#del_post_comment').modal('show');
    });

    $(document).ready(function(){
        $('#select_university').on('change',function(){
            // University id
            var id = this.value;
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_faculty').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url:"{{ route('get_faculty') }}",
                method:"GET",
                data:{id:id, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_faculty").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option value='"+id+"'>"+name+"</option>";
                            $("#select_faculty").append(option);
                        }
                    }

                },
            });
        });

        $('#select_faculty').on('change',function(){
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_department').find('option').not(':first').remove();
            $.ajax({
                url:"{{ route('get_department') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_department").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].name;
                            var option=  "<option class='select_department' value='"+id+"'>"+name+"</option>";
                            $("#select_department").append(option);
                        }
                    }
                },
            });

        });

        $('#select_department').on('change',function(){
            var _token = $('input[name="_token"]').val();
            // Empty the dropdown
            $('#select_level').find('option').not(':first').remove();

            $.ajax({
                url:"{{ route('get_level') }}",
                method:"GET",
                data:{id:this.value, _token:_token},
                success: function(response){
                    var len = 0;
                    if(response['data'] != null){
                        len = response['data'].length;
                    }
                    if(len > 0){
                        // Read data and create <option >
                        $("#select_level").append("<option  value=\"all\" > All</option>");
                        for(var i=0; i<len; i++){
                            var id = response['data'][i].id;
                            var name = response['data'][i].level;
                            var option=  "<option class='select_level' value='"+name+"'>"+name+"</option>";
                            $("#select_level").append(option);
                        }
                    }
                },
            });

        });

        $('#select_level').on('change',function () {
            $('#level_department').val(this.value);
        });
        $("#search").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".table #data").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });

</script>



