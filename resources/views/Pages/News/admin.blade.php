@extends('layouts.app')
@section('title')
    Admin News
@endsection

@section('content')

    <div class="row" style="width: 100%;padding: 10px">
        <h2 class="text-center col-md-10"> <i class="nav-icon fas fa-user-cog"> </i> Admin News  </h2>
        <div class="text-center col-md-2" >
            <a  class="btn btn-success" data-toggle="modal" data-target="#add_news"><i class="nav-icon fas fa-plus"></i> Add News </a>
        </div>
    </div>
    <div class="row" style="padding: 10px">
        @if(session('message'))
            <div class="alert alert-default-success alert-dismissible fade show text-center font-weight-bold " role="alert" style="width: 100%">
                {{session('message')}} <i class="fa fa-check-circle"></i>
            </div>
        @endif
            <div class="card table-responsive" style="width: 100%">
                <table class="table table-bordered data-table table-hover" style="vertical-align: middle;text-align: center;">
                    <thead class="thead-light">
                    <tr>
                        <th width="10%" class="align-middle"><i class="fas fa-bars"></i>  </th>
                        <th width="70%" class="align-middle"><i class="fas fa-newspaper"></i> Text </th>
                        <th width="20%" class="align-middle"><i class="fas fa-tools"></i> Tools </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($news as $key=>$new)
                        <tr id="data">
                            <td class="align-middle">{{$key+1}}</td>
                            <td class="align-middle">{{$new->newsText}}</td>
                            <td>
                                <a  class="btn btn-primary edit_news m-2" data-target="#edit_news" data-id="{{$new->id}}" data-text="{{$new->newsText}}"  data-toggle="modal" ><i class="nav-icon fas fa-edit"></i> </a>
                                <a  data-toggle="modal" data-target="#del_news" class="btn btn-danger del_news" data-id="{{$new->id}}"><i class="fas fa-minus-circle"></i>  </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

    </div>

    {{--Start Add Modal--}}
    <div class="modal fade" id="add_news" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form role="form" method="POST" action="{{route('add_news')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Add News </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-newspaper prefix grey-text"></i> News </label>
                            <textarea name='text' class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Add <i class="fas fa-plus ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Add Modal--}}
    {{--Start Edit Modal--}}
    <div class="modal fade" id="edit_news" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document" >
            <form id="form_edit_news" role="form" method="POST" action="{{route('edit_news')}}" autocomplete="off">
                @csrf
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Edit News </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" id="id" name="id">
                        <div class="md-form mb-2" >
                            <label data-error="wrong" data-success="right" ><i class="fas fa-newspaper prefix grey-text"></i> News </label>
                            <textarea name='text' id="text" class="form-control validate text-center" required></textarea>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-success"> Edit <i class="fas fa-edit ml-1"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--End Edit Modal--}}
    {{-- Start Delete Modal--}}
    <div class="modal fade" id="del_news" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title w-100 font-weight-bold"> Delete News </h5>
                    <button type="button" style="float: left" class="close" data-dismiss="modal" aria-label="Close">
                        <span  aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    Are You Sure Delete ?
                </div>
                <div class="modal-footer">
                    <form id="form_del_news" role="form" method="GET" action="">
                        <button class="btn btn-danger"><i class="fas fa-minus-circle"></i> Delete </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Modal --}}
@endsection
<script src="{{asset('dist/js/jquery.min.js')}}"></script>
<script src="{{asset('dist/js/bootstrap.min.js')}}"></script>
<script>
    $(document).on('click', '.edit_news', function() {
        $('#id').val($(this).data('id'));
        $('#text').val($(this).data('text'));
        $('#edit_news').modal('show');
    });
    $(document).on('click','.del_news',function () {
        var id=$(this).data('id');
        var action="{{route('del_news',"id")}}";
        action=action.replace('id',id);
        $('#form_del_news').attr('action',action);
        $('#del_news').modal('show');
    });
</script>



