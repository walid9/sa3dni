<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta data-n-head="true" name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <link rel="shortcut icon" href="{{ asset('icon.png') }}">

    <title>Sa3dni | Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="{{asset('css/font.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-footer-fixed">

<div class="wrapper">

    <!-- Navbar -->
    @include('inc.navbar')
    <!-- /.navbar -->

    <!-- Sidebar -->
    @include('inc.sidebar')
    <!-- /.sidebar -->


    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">@yield('title') </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{asset('/')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active">@yield('title')</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- /.content-header -->
    <div id="loader" class="spinner-border text-info text-bold" style="width: 10rem; height: 10rem;position: absolute;left:50%;top: 40%">  </div>
    <div id="myDiv" class="pb-5" style="display: none">
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
    </div>
    <!-- /.content -->

</div>

    <!-- footer start -->
    @include('inc.footer')
    <!-- footer end -->

    <!-- Profile Modal -->
        @include('inc.ProfileModal')
    <!-- Profile Modal end -->


</div>
</body>

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>

<script>
    var timer;
    $(document).ready(function(){
        document.addEventListener("contextmenu", function(e){
            e.preventDefault();
        }, false);
        document.addEventListener("keydown", function(e) {
            // "I" key
            if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
                disabledEvent(e);
            }
            // "J" key
            if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
                disabledEvent(e);
            }
            // "S" key + macOS
            if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                disabledEvent(e);
            }
            // "U" key
            if (e.ctrlKey && e.keyCode == 85) {
                disabledEvent(e);
            }
            // "F12" key
            if (event.keyCode == 123) {
                disabledEvent(e);
            }
        }, false);
        function disabledEvent(e){
            if (e.stopPropagation){
                e.stopPropagation();
            } else if (window.event){
                window.event.cancelBubble = true;
            }
            e.preventDefault();
            return false;
        }
        timer = setTimeout(check, 2000);
        $(".alert-default-success").fadeTo(5000, 500).slideUp(500, function() {
            $(".alert-default-success").slideUp(500);
        });
    });

    function check() {
        if(document.readyState === 'complete')
            showPage();
        timer = setTimeout(check, 1000);
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }

    $(document).on('click', '.profile_modal', function() {
        $('#full_name').text($(this).data('fullname'));
        $('#phone').text($(this).data('phone'));
        $('#username').text($(this).data('username'));
        $('#email').text($(this).data('email'));
        $('#university').text($(this).data('university'));
        $('#faculty').text($(this).data('faculty'));
        $('#department').text($(this).data('department'));
        $('#image').attr('src',$(this).data('image'));
        $('#post').text($(this).data('post'));
        $('#news').text($(this).data('news'));
        $('#question').text($(this).data('question'));
        $('#comment').text($(this).data('comment'));
        $('#deadline').val($(this).data('deadline'));
        $('.user_id').val($(this).data('id'));
        $('#user_rank').val($(this).data('rank'));
        $('#user_device').val($(this).data('device'));
        $('#user_promo').val($(this).data('promo'));
        $('#balance_cache').val($(this).data('cache'));
        $('#balance_service').val($(this).data('service'));
        if($(this).data('promo_status')){
            $('#active_promo').show();
            $('#unactive_promo').hide();
        }
        else{
            $('#active_promo').hide();
            $('#unactive_promo').show();
        }
        if($(this).data('verified')){
            $('#user_check').show();
            $('#btn_unverified').show();
            $('#btn_verified').hide();
        }
        else{
            $('#user_check').hide();
            $('#btn_unverified').hide();
            $('#btn_verified').show();
        }
        if($(this).data('status')){
            $('#status').prop('checked',true);
        }
        else{
            $('#status').prop('checked',false);
        }
        $('#user_id').val($(this).data('id'));
        $levels=$(this).data('levels');
        $levels_text="";
        $levels.forEach(function (level) {
            $levels_text += level.level +"  ";
        });
        $('#level').text($levels_text);
        $('#OpenProfileModal').modal('show');
    });

</script>
</html>
