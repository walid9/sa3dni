<aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard')}}" class="brand-link m-auto">
        <img src="{{asset('images/logo.png')}}" class="brand-image ">
        <span class="brand-text font-weight-light hide-text fa fa-blank mb-5"></span>
    </a>
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('images/admin.png')}}" class="img-circle elevation-2 bg-white">
            </div>
            <div class="info">
                <a class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2 mb-5">
            <ul class="nav nav-pills nav-sidebar flex-column mb-5" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('dashboard')}}" class="nav-link @if(Request::is('dashboard')) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i> <p> Dashboard </p>
                    </a>
                </li>
                <li class="nav-item has-treeview @if(Request::is('dashboard/user/*')) menu-open @endif">
                    <a href="" class="nav-link @if(Request::is('dashboard/user/*')) active @endif">
                        <i class="fas fa-users nav-icon"></i> <p>User</p> <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview ml-3">
                        <li class="nav-item">
                            <a href="{{ route('users')}}" class="nav-link @if(Request::is('dashboard/user/all')) active @endif">
                                <i class="fa fa-users nav-icon"></i>
                                <p> All Users</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('user_normal')}}" class="nav-link @if(Request::is('dashboard/user/normal')) active @endif">
                                <i class="fa fa-users nav-icon"></i>
                                <p>Normal Users</p>
                            </a>
                        </li>
                        @if(\Auth::user()->role)
                        <li class="nav-item">
                            <a href="{{ route('user_excellent')}}" class="nav-link @if(Request::is('dashboard/user/excellent')) active @endif">
                                <i class="fa fa-user-friends nav-icon"></i>
                                <p>Excellent Users</p>
                            </a>
                        </li>
                            <li class="nav-item">
                                <a href="{{ route('user_balance')}}" class="nav-link @if(Request::is('dashboard/user/balance')) active @endif">
                                    <i class="fa fa-money-bill-alt nav-icon"></i>
                                    <p>Balance Users</p>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a href="{{ route('request_payment')}}" class="nav-link @if(Request::is('dashboard/user/payment')) active @endif">
                                <i class="fa fa-spinner nav-icon"></i>
                                <p>Request Payment</p>
                            </a>
                        </li>
                        @if(\Auth::user()->role)
                        <li class="nav-item">
                            <a href="{{ route('user_statics')}}" class="nav-link @if(Request::is('dashboard/user/statics')) active @endif">
                                <i class="fa fa-percent nav-icon"></i>
                                <p>Statics</p>
                            </a>
                        </li>
                        @endif
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('faculty')}}" class="nav-link @if(Request::is('dashboard/faculty')) active @endif">
                        <i class="fas fa-school nav-icon"></i>
                        <p>Faculty</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('department')}}" class="nav-link @if(Request::is('dashboard/department')) active @endif">
                        <i class="fas fa-building nav-icon"></i>
                        <p>Department</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('subject')}}" class="nav-link @if(Request::is('dashboard/subject')) active @endif">
                        <i class="fas fa-book nav-icon"></i>
                        <p>Subject</p>
                    </a>
                </li>


                <li class="nav-item has-treeview @if(Request::is('dashboard/posts/*')) menu-open @endif">
                    <a href="" class="nav-link @if(Request::is('dashboard/posts/*')) active @endif">
                        <i class="fas fa-chalkboard-teacher nav-icon"></i> <p>Posts</p> <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview ml-3">
                        <li class="nav-item">
                            <a href="{{ route('approved_posts')}}" class="nav-link @if(Request::is('dashboard/posts/approved')) active @endif">
                                <i class="fa fa-thumbs-up nav-icon"></i>
                                <p>Approved Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('waiting_posts')}}" class="nav-link @if(Request::is('dashboard/posts/waiting')) active @endif">
                                <i class="fa fa-spinner nav-icon"></i>
                                <p>Waiting Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('rejected_posts')}}" class="nav-link @if(Request::is('dashboard/posts/rejected')) active @endif">
                                <i class="fa fa-ban nav-icon"></i>
                                <p>Refused Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('reports_posts')}}" class="nav-link @if(Request::is('dashboard/posts/reports')) active @endif">
                                <i class="fa fa-comment-slash nav-icon"></i>
                                <p>Report Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin_posts')}}" class="nav-link @if(Request::is('dashboard/posts/admin')) active @endif">
                                <i class="fa fa-user-cog nav-icon"></i>
                                <p>Admin Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('posts_comment')}}" class="nav-link @if(Request::is('dashboard/posts/comments')) active @endif">
                                <i class="fa fa-comments nav-icon"></i>
                                <p>Posts Comments</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview @if(Request::is('dashboard/news/*')) menu-open @endif">
                    <a href="" class="nav-link @if(Request::is('dashboard/news/*')) active @endif">
                        <i class="fas fa-newspaper nav-icon"></i> <p>News</p> <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview ml-3">
                        <li class="nav-item">
                            <a href="{{ route('approved_news')}}" class="nav-link @if(Request::is('dashboard/news/approved')) active @endif">
                                <i class="fa fa-thumbs-up nav-icon"></i>
                                <p>Approved News</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('waiting_news')}}" class="nav-link @if(Request::is('dashboard/news/waiting')) active @endif">
                                <i class="fa fa-spinner nav-icon"></i>
                                <p>Waiting News</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('rejected_news')}}" class="nav-link @if(Request::is('dashboard/news/rejected')) active @endif">
                                <i class="fa fa-ban nav-icon"></i>
                                <p>Refused News</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('reports_news')}}" class="nav-link @if(Request::is('dashboard/news/reports')) active @endif">
                                <i class="fa fa-comment-slash nav-icon"></i>
                                <p>Report News</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin_news')}}" class="nav-link @if(Request::is('dashboard/news/admin')) active @endif">
                                <i class="fa fa-user-cog nav-icon"></i>
                                <p>Admin News</p>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item has-treeview @if(Request::is('dashboard/questions/*')) menu-open @endif">
                    <a href="" class="nav-link @if(Request::is('dashboard/questions/*')) active @endif">
                        <i class="fas fa-question nav-icon"></i> <p>Questions</p> <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview ml-3">
                        <li class="nav-item">
                            <a href="{{ route('approved_questions')}}" class="nav-link @if(Request::is('dashboard/questions/approved')) active @endif">
                                <i class="fa fa-thumbs-up nav-icon"></i>
                                <p>Approved Questions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('waiting_questions')}}" class="nav-link disabled @if(Request::is('dashboard/questions/waiting')) active @endif">
                                <i class="fa fa-spinner nav-icon"></i>
                                <p>Waiting Questions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('rejected_questions')}}" class="nav-link @if(Request::is('dashboard/questions/rejected')) active @endif">
                                <i class="fa fa-ban nav-icon"></i>
                                <p>Refused Questions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('reports_questions')}}" class="nav-link @if(Request::is('dashboard/questions/reports')) active @endif">
                                <i class="fa fa-comment-slash nav-icon"></i>
                                <p>Report Question</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('questions_comment')}}" class="nav-link @if(Request::is('dashboard/questions/comments')) active @endif">
                                <i class="fa fa-comments nav-icon"></i>
                                <p>Question Comments</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview @if(Request::is('dashboard/subjects/*')) menu-open @endif">
                    <a href="" class="nav-link @if(Request::is('dashboard/subjects/*')) active @endif">
                        <i class="fa fa-folder nav-icon"></i> <p>Subjects Files</p> <i class="right fas fa-angle-left"></i>
                    </a>
                    <ul class="nav nav-treeview ml-3">
                        <li class="nav-item">
                            <a href="{{ route('videos')}}" class="nav-link @if(Request::is('dashboard/subjects/videos')) active @endif">
                                <i class="fa fa-video nav-icon"></i>
                                <p>Videos</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('sheets')}}" class="nav-link @if(Request::is('dashboard/subjects/sheets')) active @endif">
                                <i class="fa fa-file-pdf nav-icon"></i>
                                <p>Sheets</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('summary')}}" class="nav-link @if(Request::is('dashboard/subjects/summary')) active @endif">
                                <i class="fa fa-file-pdf nav-icon"></i>
                                <p>Summary</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('contact_us')}}" class="nav-link @if(Request::is('dashboard/contact')) active @endif">
                        <i class="fas fa-info-circle nav-icon"></i>
                        <p>Contact Us</p>
                    </a>
                </li>

                <li class="nav-header"><i class="fa fa-cogs"></i>System Management</li>
                @if(\Auth::user()->role)
                    <li class="nav-item">
                        <a href="{{ route('android')}}" class="nav-link @if(Request::is('dashboard/management/android')) active @endif">
                            <i class="fab fa-android nav-icon"></i>
                            <p>Android App</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('img_app')}}" class="nav-link @if(Request::is('dashboard/management/AppImages')) active @endif">
                            <i class="fas fa-images nav-icon"></i>
                            <p>Images App</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('price_faculty')}}" class="nav-link @if(Request::is('dashboard/management/faculty/price')) active @endif">
                            <i class="fas fa-money-bill-alt nav-icon"></i>
                            <p>Faculty Price</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('qrcode')}}" class="nav-link @if(Request::is('dashboard/management/qr')) active @endif">
                            <i class="fas fa-qrcode nav-icon"></i>
                            <p>Qr Code</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('trials')}}" class="nav-link @if(Request::is('dashboard/management/trials')) active @endif">
                            <i class="fas fa-percentage nav-icon"></i>
                            <p>Free Trial</p>
                        </a>
                    </li>

                @endif
                <li class="nav-item">
                    <a href="{{ route('ads')}}" class="nav-link @if(Request::is('dashboard/management/ads')) active @endif">
                        <i class="fas fa-ad nav-icon"></i>
                        <p>Ads</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('notifications')}}" class="nav-link @if(Request::is('dashboard/management/notifications')) active @endif">
                        <i class="fas fa-sticky-note nav-icon"></i>
                        <p>Notifications</p>
                    </a>
                </li>
                <li class="nav-item mb-5">
                    <a href="{{ route('places')}}" class="nav-link @if(Request::is('dashboard/management/places')) active @endif">
                        <i class="fas fa-map-marker-alt nav-icon"></i>
                        <p>Payment Places</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
</aside>


