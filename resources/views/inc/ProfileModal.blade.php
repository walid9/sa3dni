<!-- Open Profile Modal -->
<div class="modal fade" id="OpenProfileModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-secondary">
                    <h3 class="widget-user-username" ><span id="full_name"  style="font-size:40px;"></span> <i class="fa fa-user-check" id="user_check"></i></h3>
                </div>
                <div class="widget-user-image" >
                    <img class="img-circle elevation-2" style="height: 90px;width: 90px" id="image" src="">
                </div>
                <!-- /.widget-user-image -->
                <div class="card-footer">
                    <p style="display: none" id="id"></p>
                    <div class="row">
                        <div class="col-sm-3 col-6 border-right">
                            <div class="description-block">
                                <h6><i class="fa fa-chalkboard-teacher"></i></h6>
                                <h5 class="description-header" id="post"></h5>
                                <span class="description-text">Posts</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-6 border-right">
                            <div class="description-block">
                                <h6><i class="fa fa-newspaper"></i></h6>
                                <h5 class="description-header" id="news"></h5>
                                <span class="description-text">News</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-6 border-right">
                            <div class="description-block">
                                <h6><i class="fa fa-question-circle"></i></h6>
                                <h5 class="description-header" id="question"></h5>
                                <span class="description-text">Questions</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <div class="col-sm-3 col-6">
                            <div class="description-block">
                                <h6><i class="fa fa-comments"></i></h6>
                                <h5 class="description-header" id="comment"></h5>
                                <span class="description-text">Comments</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <ul class="nav flex-column m-2" style="font-size:18px;">
                        <li class="nav-item m-1 row">
                        <div class="col-md-6 col-12 text-center">
                            <form role="form" method="POST" action="{{route('verified')}}">
                                @csrf
                                <input type="hidden" name="id" class="user_id">
                                    <button class="btn btn-danger rounded btn-block btn-flat mt-1" id="btn_unverified" >
                                        <i class="fa fa-minus-circle"></i> Un Verified
                                    </button>
                                    <button class="btn btn-success rounded btn-block btn-flat mt-1" id="btn_verified">
                                        <i class="fa fa-check-circle"></i> Verified
                                    </button>
                            </form>
                        </div>
                        <div class="col-md-6 col-12">
                            <form role="form" method="POST" class="row" action="{{route('rank')}}">
                                @csrf
                                <input type="number" name="rank" min="0" placeholder="rank" id="user_rank" class="col-6 form-control text-center m-1 mt-0">
                                <input type="hidden" name="id" class="user_id">
                                <button  class="col-5 btn btn-info btn-sm form-control m-1 mt-0"><i class="fa fa-save"></i> Rank</button>
                            </form>
                        </div>
                        <div class="col-md-7 col-12 border-top">
                                <form role="form" method="POST" class="row mt-1" action="{{route('user_reset')}}">
                                    @csrf
                                    <input type="text" placeholder="Device ID" id="user_device" readonly class="col-8 form-control text-center m-1">
                                    <input type="hidden" name="id" class="user_id">
                                    <button  class="col-3 btn btn-warning btn-sm form-control m-1"><i class="fa fa-trash"></i> Reset</button>
                                </form>
                        </div>
                         <div class="col-md-5 col-12 border-top">
                             <form role="form" method="POST" class="row mt-1" action="{{route('user_promo')}}">
                                 @csrf
                                 <input type="text"  id="user_promo" readonly class="col-7 form-control text-center m-1">
                                 <input type="hidden" name="id" class="user_id">
                                 <button  class="col-3 btn btn-danger btn-sm form-control m-1" id="unactive_promo"><i class="fa fa-times-circle"></i> </button>
                                 <button  class="col-3 btn btn-success btn-sm form-control m-1" id="active_promo"><i class="fa fa-check-circle"></i> </button>
                             </form>
                         </div>
                         <div class="col-md-12 col-12 row">
                            <div class="col-md-6 col-6">
                                <div class="md-form mb-2" >
                                    <label data-error="wrong" data-success="right" ><i class="fas fa-money-bill-alt prefix grey-text"></i>  Cache </label>
                                    <input type="text"  id='balance_cache' class="form-control text-center" readonly>
                                </div>
                            </div>
                             <div class="col-md-6 col-6">
                                 <div class="md-form mb-2" >
                                     <label  data-error="wrong" data-success="right" ><i class="fas fa-money-check-alt prefix grey-text"></i>  Service </label>
                                     <input type="text" id='balance_service' class="form-control text-center" readonly>
                                 </div>
                             </div>
                         </div>
                        </li>
                        <li class="nav-item m-1">
                            <p class="nav-link">
                                <i class="far fa-envelope"></i> Email:
                                <span class="float-right badge bg-info" id="email"></span>
                            </p>
                        </li>
                        <li class="nav-item m-1">
                            <p class="nav-link">
                                <i class="fas fa-at"></i> User Name:
                                <span class="float-right badge bg-info" id="username"></span>
                            </p>
                        </li>
                        <li class="nav-item  m-1">
                            <p class="nav-link ">
                                <i class="fas fa-mobile-alt"></i> Phone <span class="float-right badge bg-info" id="phone"></span>
                            </p>
                        </li>
                        <li class="nav-item m-1">
                            <p class="nav-link">
                                <i class="fas fa-university"></i> University <span class="float-right badge bg-info" id="university"></span>
                            </p>
                        </li>
                        <li class="nav-item m-2">
                            <p class="nav-link">
                                <i class="fas fa-school"></i> Faculty <span class="float-right badge bg-info" id="faculty"></span>
                            </p>
                        </li>
                        <li class="nav-item m-1">
                            <p class="nav-link">
                                <i class="fas fa-building"></i> Department <span class="float-right badge bg-info" id="department"></span>
                            </p>
                        </li>
                        <li class="nav-item m-1">
                            <p class="nav-link">
                                <i class="fas fa-id-badge"></i> Level <span class="float-right badge bg-info" id="level"></span>
                            </p>
                        </li>
                        <form role="form" method="POST" action="{{route('update_status')}}">
                            @csrf
                            <input type="hidden" name="id" id="user_id" class="user_id">
                            <div class="row border-top">
                                <li class="nav-item col-md-4 col-4">
                                    <p class="nav-link mt-2 ">
                                        <i class="fa fa-user-check"></i>  <span class="float-right" ><input class="form-check-input" id="status" name="status" type="checkbox"></span>
                                    </p>
                                </li>
                                <li class="nav-item col-md-8 col-8">
                                    <p class="nav-link row">
                                        <i class="fa fa-calendar-day mt-2 col-md-2 col-2"></i>
                                        <span class="float-right col-md-10 col-10"> <input type="date" class="form-control" name="deadline" id="deadline" ></span>
                                    </p>
                                </li>
                                <li class="nav-item col-md-12" style="margin-bottom: -50px">
                                    <p class="nav-link text-center">
                                        <button type="submit" class="btn btn-info"> <i class="fa fa-save"></i> Save </button>
                                    </p>
                                </li>
                            </div>

                        </form>

                    </ul>

                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>
</div>
<!-- Open Profile Modal end -->
