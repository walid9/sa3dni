<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta data-n-head="true" name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <title>Sa3dni</title>
    <!--===============================================================================================-->
    <link rel="shortcut icon" href="{{ asset('icon.png') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!--===============================================================================================-->
</head>
<body>
<div class="limiter wrapper">
    <div class="container-login100 container-fluid">
        <img src="{{asset('/images/poster.jpg')}}"  usemap="#image-map" readonly="true">
        <map name="image-map">
            <area target="" alt="android" title="android" href="" coords="181,568,343,614" shape="rect">
            <area target="" alt="ios" title="ios" href="" coords="532,566,369,620" shape="rect">
            <area target="_blank" alt="facebook" title="facebook" href="{{url('https://www.facebook.com/Sa3dniApp')}}" coords="455,789,28" shape="circle">
            <area target="" alt="instagram" title="instagram" href="" coords="511,789,25" shape="circle">
            <area target="" alt="youtube" title="youtube" href="" coords="563,788,27" shape="circle">
            <area target="" alt="twitter" title="twitter" href="" coords="621,786,26" shape="circle">
        </map>
    </div>
</div>
</body>
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("img").mousedown(function(e){
                e.preventDefault()
            });

            // this will disable right-click on all images
            $("body").on("contextmenu",function(e){
                return false;
            });
        });
</script>

</html>
